package gui;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 0.0
 */
public class Demo extends JFrame {
    /**
     * 正则表达式匹配整型数字
     */
    private static final Pattern P_INT = Pattern.compile("\\d+");
    private static final Pattern P_DOUBLE = Pattern.compile("\\d+.\\d+");
    /**
     * 产品信息面板
     */
    private JPanel productInformationPanel = new JPanel();
    /**
     * 操作面板
     */
    private JPanel operationPanel = new JPanel();
    /**
     * 网格布局
     */
    private GridLayout gridLayout = new GridLayout(3, 2);
    /**
     * 商品号的文本框
     */
    private JTextField articleNumberTextField = new JTextField(10);
    private JLabel articleNumberLabel = new JLabel("商品号");
    /**
     * 商品名的文本框
     */
    private JTextField productNameTextField = new JTextField(10);
    private JLabel productNameLabel = new JLabel("商品名");
    /**
     * 品牌的文本框
     */
    private JTextField brandTextField = new JTextField(10);
    private JLabel brandLabel = new JLabel("品牌");
    /**
     * 价格的文本框
     */
    private JTextField priceTextField = new JTextField(10);
    private JLabel priceLabel = new JLabel("价格");
    /**
     * 库存量的文本框
     */
    private JTextField inventoryTextField = new JTextField(10);
    private JLabel inventoryLabel = new JLabel("库存量");

    private JButton firstButton = new JButton("第一条");
    private JButton previousButton = new JButton("前一条");
    private JButton nextButton = new JButton("下一条");
    private JButton lastButton = new JButton("最后一条");
    private JButton insertButton = new JButton("插入");
    private JButton deleteButton = new JButton("删除");
    private JButton updateButton = new JButton("修改");
    /**
     * 操作类
     */
    private CommodityDate connection = new CommodityDate();
    /**
     * 保存数据库返回的信息
     */
    private String[] strings;

    /**
     * 无参构造函数做初始化
     */
    public Demo() {
        // 窗口的标题
        this.setTitle("访问数据库");
        // 窗口出现的x坐标、y坐标、宽度和高度
        this.setBounds(100, 200, 600, 200);
        // 使用System exit方法退出应用程序。 仅在应用程序中使用它。
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        init();
        // 设置窗口可见性
        this.setVisible(true);
    }

    /**
     * 初始化函数
     */
    private void init() {
        // 将panel面板放到JFrame中
        this.add(productInformationPanel, BorderLayout.CENTER);
        productInformationPanel.setLayout(gridLayout);
        productInformationPanel.add(articleNumberLabel);
        productInformationPanel.add(articleNumberTextField);
        productInformationPanel.add(productNameLabel);
        productInformationPanel.add(productNameTextField);
        productInformationPanel.add(brandLabel);
        productInformationPanel.add(brandTextField);
        productInformationPanel.add(priceLabel);
        productInformationPanel.add(priceTextField);
        productInformationPanel.add(inventoryLabel);
        productInformationPanel.add(inventoryTextField);
        this.add(operationPanel, BorderLayout.SOUTH);
        // 第一条
        operationPanel.add(firstButton);
        firstButton.addActionListener(e -> {
            strings = connection.first();
            setTexts(strings);
        });
        // 前一条
        operationPanel.add(previousButton);
        previousButton.addActionListener(e -> {
            String articleNumber = articleNumberTextField.getText();
            if (checkNull(articleNumber) || !checkNumberInt(articleNumber)) {
                return;
            }
            strings = connection.previous(articleNumber);
            setTexts(strings);
        });
        // 下一条
        operationPanel.add(nextButton);
        nextButton.addActionListener(e -> {
            String articleNumber = articleNumberTextField.getText();
            if (checkNull(articleNumber) || !checkNumberInt(articleNumber)) {
                return;
            }
            strings = connection.next(articleNumber);
            setTexts(strings);
        });
        // 最后一条
        operationPanel.add(lastButton);
        lastButton.addActionListener(e -> {
            strings = connection.last();
            setTexts(strings);
        });
        // 插入
        operationPanel.add(insertButton);
        insertButton.addActionListener(e -> {
            String articleNumber = articleNumberTextField.getText();
            String productName = productNameTextField.getText();
            String brand = brandTextField.getText();
            String price = priceTextField.getText();
            String inventory = inventoryTextField.getText();
            if (checkNull(articleNumber, productName, brand, price, inventory) || !checkNumberInt(articleNumber, inventory) || !checkNumberDouble(price)) {
                return;
            }
            try {
                connection.insert(articleNumber, productName, brand, price, inventory);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "该商品号" + articleNumber + "的信息已经存在");
            }
        });
        // 删除
        operationPanel.add(deleteButton);
        deleteButton.addActionListener(e -> {
            String articleNumber = articleNumberTextField.getText();
            if (checkNull(articleNumber) || !checkNumberInt(articleNumber)) {
                return;
            }
            if (connection.delete(articleNumber) == 0) {
                JOptionPane.showMessageDialog(this, "该商品号" + articleNumber + "不存在，删除失败");
            }
        });
        // 更新
        operationPanel.add(updateButton);
        updateButton.addActionListener(e -> {
            String articleNumber = articleNumberTextField.getText();
            String productName = productNameTextField.getText();
            String brand = brandTextField.getText();
            String price = priceTextField.getText();
            String inventory = inventoryTextField.getText();
            if (checkNull(articleNumber, productName, brand, price, inventory) || !checkNumberInt(articleNumber, inventory) || !checkNumberDouble(price)) {
                return;
            }
            connection.update(articleNumber, productName, brand, price, inventory);
        });
    }

    /**
     * 将数组中的信息显示在面板上
     *
     * @param text 信息集
     */
    private void setTexts(String[] text) {
        articleNumberTextField.setText(text[0]);
        productNameTextField.setText(text[1]);
        brandTextField.setText(text[2]);
        priceTextField.setText(text[3]);
        inventoryTextField.setText(text[4]);
    }

    /**
     * 检查数据是否为空，并提示信息
     *
     * @param strings 要检查的数据
     * @return 为空返回true, 否则返回false
     */
    private boolean checkNull(String... strings) {
        for (String str : strings) {
            if ("".equals(str)) {
                JOptionPane.showMessageDialog(this, "请输入信息");
                return true;
            }
        }
        return false;
    }

    /**
     * 检查数字是否为整型
     *
     * @param s 要检查的数字
     * @return 为整型返回true, 否则返回false
     */
    private boolean checkNumberInt(String... s) {
        boolean b = true;
        Matcher m = P_INT.matcher(s[0]);
        if (!m.matches()) {
            JOptionPane.showMessageDialog(this, "商品号应该为整型数字");
            b = false;
        }
        if (s.length == 1) {
            return b;
        }
        m = P_INT.matcher(s[1]);
        if (!m.matches()) {
            JOptionPane.showMessageDialog(this, "库存量应该为整型数字");
            b = false;
        }
        return b;
    }

    /**
     * 检查数字是否为double类型
     *
     * @param s 要检查的数字
     * @return 为double类型返回true, 否则返回false
     */
    private boolean checkNumberDouble(String s) {
        Matcher m = P_DOUBLE.matcher(s);
        if (!m.matches()) {
            JOptionPane.showMessageDialog(this, "价格应该为浮点型数字");
            return false;
        }
        return true;
    }
}
