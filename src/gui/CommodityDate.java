package gui;

import java.sql.*;

/**
 * 数据库操作类
 *
 * @author 0.0
 */
public class CommodityDate {
    /**
     * 数据库IP地址
     */
    private static final String SQL_IP = "localhost";
    /**
     * 数据库端口
     */
    private static final String SQL_PORT = "3306";
    /**
     * 数据库名
     */
    private static final String SQL_NAME = "test";
    /**
     * 连接数据库的URL
     */
    private static final String URL = "jdbc:mysql://" + SQL_IP + ":" + SQL_PORT + "/" + SQL_NAME;
    /**
     * 数据库用户名
     */
    private static final String SUER_NAME = "root";
    /**
     * 数据库密码
     */
    private static final String PASSWORD = "liang020901";
    /**
     * 声明连接对象
     */
    private Connection connection;
    /**
     * 声明预编译对象
     */
    private Statement statement = null;
    /**
     * 声明预编译语句对象
     */
    private PreparedStatement preparedStatement;
    /**
     * 声明结果集对象
     */
    private ResultSet resultSet;
    private String str;
    private int count = 0;


    public void createConnection() {
        // 加载驱动
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("未能成功加载驱动程序,请检查是否导入驱动程序!");
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(URL, SUER_NAME, PASSWORD);
            System.out.println(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closeResultSet() {
        try {
            // 关闭结果集对象
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closePreparedStatement() {
        try {
            if (preparedStatement != null) {
                // 关闭连接对象
                preparedStatement.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 定义数据库查询记录的方法
     *
     * @param sql SQL语句
     * @return 结果集
     */
    private ResultSet query(String sql) {
        try {
            createConnection();
            // 创建预编译语句对象
            preparedStatement = connection.prepareStatement(sql);
            // 执行SQL查询语句，返回结果集
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    /**
     * 插入
     *
     * @return 数据库影响的行数
     */
    public int insert(String s1, String s2, String s3, String s4, String s5) throws SQLException {
        // 定义数据库添加记录的方法
        createConnection();
        // 添加记录的SQL语句
        str = "insert into test.products(articleNumber,productName,brand,price,inventory)values(?,?,?,?,?)";
        // 创建预编译语句对象
        preparedStatement = connection.prepareStatement(str);
        preparedStatement.setInt(1, Integer.parseInt(s1));
        preparedStatement.setString(2, s2);
        preparedStatement.setString(3, s3);
        preparedStatement.setDouble(4, Double.parseDouble(s4));
        preparedStatement.setInt(5, Integer.parseInt(s5));
        // 执行SQL添加语句，返回整型结果
        count = preparedStatement.executeUpdate();
        closePreparedStatement();
        closeConnection();
        return count;
    }

    /**
     * 定义数据库修改记录的方法
     *
     * @return 数据库影响的行数
     */
    public int update(String s1, String s2, String s3, String s4, String s5) {
        try {
            createConnection();
            str = "update test.products set articleNumber=?,productName=?,brand=?,price=?,inventory=? where articleNumber =" + Integer.parseInt(s1);
            // 修改记录的SQL语句
            // 创建预编译语句对象
            preparedStatement = connection.prepareStatement(str);
            // 设置预编译语句第1个?的参数值
            preparedStatement = connection.prepareStatement(str);
            preparedStatement.setInt(1, Integer.parseInt(s1));
            preparedStatement.setString(2, s2);
            preparedStatement.setString(3, s3);
            preparedStatement.setDouble(4, Double.parseDouble(s4));
            preparedStatement.setInt(5, Integer.parseInt(s5));
            // 执行SQL修改语句，返回整型结果
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closePreparedStatement();
        closeConnection();
        return count;
    }

    /**
     * 定义数据库删除记录的方法
     *
     * @return 数据库影响的行数
     */
    public int delete(String s) {
        try {
            createConnection();
            // 删除记录的SQL语句
            str = "delete from test.products where articleNumber=?";
            // 创建预编译语句对象
            preparedStatement = connection.prepareStatement(str);
            // 设置预编译语句第1个?的参数值
            preparedStatement.setInt(1, Integer.parseInt(s));
            // 执行SQL删除语句，返回整型结果
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closePreparedStatement();
        closeConnection();
        return count;
    }

    public String[] first() {
        String[] strings = new String[5];
        try {
            // 建立连接
            createConnection();
            String sql = "select articleNumber, productName, brand, price, inventory from test.products order by articleNumber limit 0,1;";
            // 创建预编译语句对象
            preparedStatement = connection.prepareStatement(sql);
            // 执行预编译语句
            ResultSet result = query(sql);
            while (result.next()) {
                strings[0] = String.valueOf(result.getInt(1));
                strings[1] = result.getString(2);
                strings[2] = result.getString(3);
                strings[3] = String.valueOf(result.getDouble(4));
                strings[4] = String.valueOf(result.getInt(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeResultSet();
        closePreparedStatement();
        closeConnection();
        return strings;
    }

    public String[] last() {
        String[] strings = new String[5];
        try {
            // 建立连接
            createConnection();
            String sql = "select articleNumber, productName, brand, price, inventory from test.products order by articleNumber desc limit 0,1;";
            // 创建预编译语句对象
            preparedStatement = connection.prepareStatement(sql);
            // 执行预编译语句
            ResultSet result = query(sql);
            while (result.next()) {
                strings[0] = String.valueOf(result.getInt(1));
                strings[1] = result.getString(2);
                strings[2] = result.getString(3);
                strings[3] = String.valueOf(result.getDouble(4));
                strings[4] = String.valueOf(result.getInt(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeResultSet();
        closePreparedStatement();
        closeConnection();
        return strings;
    }

    /**
     * 商品号前一条数据
     *
     * @param articleNumber 商品号
     * @return 商品号的前一条数据
     */
    public String[] previous(String articleNumber) {
        String[] strings = new String[5];
        try {
            // 建立连接
            createConnection();
            String sql = "select * from test.products where articleNumber=(select max(articleNumber) from test.products where articleNumber<" + articleNumber + ")";
            // 创建预编译语句对象
            preparedStatement = connection.prepareStatement(sql);
            // 执行预编译语句
            ResultSet result = query(sql);
            while (result.next()) {
                strings[0] = String.valueOf(result.getInt(1));
                strings[1] = result.getString(2);
                strings[2] = result.getString(3);
                strings[3] = String.valueOf(result.getDouble(4));
                strings[4] = String.valueOf(result.getInt(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeResultSet();
        closePreparedStatement();
        closeConnection();
        return strings;
    }

    /**
     * 商品号下一条数据
     *
     * @param articleNumber 商品号
     * @return 商品号下一条数据
     */
    public String[] next(String articleNumber) {
        String[] strings = new String[5];
        try {
            // 建立连接
            createConnection();
            String sql = "select articleNumber, productName, brand, price, inventory from test.products  where articleNumber=(select min(articleNumber) from test.products  where articleNumber>" + articleNumber + ")";
            // 创建预编译语句对象
            preparedStatement = connection.prepareStatement(sql);
            // 执行预编译语句
            ResultSet result = query(sql);
            while (result.next()) {
                strings[0] = String.valueOf(result.getInt(1));
                strings[1] = result.getString(2);
                strings[2] = result.getString(3);
                strings[3] = String.valueOf(result.getDouble(4));
                strings[4] = String.valueOf(result.getInt(5));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeResultSet();
        closePreparedStatement();
        closeConnection();
        return strings;
    }
}
