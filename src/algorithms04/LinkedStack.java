package algorithms04;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 下压栈,基于链表
 *
 * @author HHH
 */
public class LinkedStack<Item> implements StackBase<Item> {
    /**
     * 栈顶(最近添加的元素)
     */
    private Node first;
    /**
     * 栈中含有的元素数量
     */
    private int n;

    /**
     * 返回true,如果栈为空,否则false
     *
     * @return true, 如果栈为空, 否则false
     */
    @Override
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * 返回栈中含有的元素个数
     *
     * @return 栈中含有的元素个数
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 把元素压入栈顶
     *
     * @param item 要添加的元素
     */
    @Override
    public void push(Item item) {
        Node oldFirst = first;
        first = new Node(item, oldFirst);
        n++;
    }

    /**
     * 返回栈顶元素,并删除它
     *
     * @return 栈顶元素, 并删除它
     */
    @Override
    public Item pop() {
        Item item = first.item;
        first = first.next;
        n--;
        return item;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<Item> iterator() {
        return new LinkedIterator();
    }

    private class LinkedIterator implements Iterator<Item> {
        private Node current = first;

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return current != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public Item next() {
            Item item = current.item;
            current = current.next;
            return item;
        }
    }

    /**
     * 结点,使用私有内部类实现
     */
    private class Node {
        private final Item item;
        private final Node next;

        public Node(Item item, Node next) {
            this.item = item;
            this.next = next;
        }
    }
}
