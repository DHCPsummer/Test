package algorithms04.string.searchtree;

import edu.princeton.cs.algs4.Queue;

/**
 * 基于单词查找树的符号表
 *
 * @author HHH
 */
public class TrieSt<Value> implements StringStBase<Value> {
    /**
     * 基数
     */
    private static final int R = EXTENDED_ASCII;
    private Node root;
    private int n;

    /**
     * 向表中插入键值对(如果值为null，则删除键)
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void put(String key, Value value) {
        root = put(root, key, value, 0);
    }

    private Node put(Node x, String key, Value value, int d) {
        if (x == null) {
            x = new Node();
        }
        // 到达key最后一个字符更新value
        if (d == key.length()) {
            if (x.value == null) {
                n++;
            }
            x.value = value;
            return x;
        }
        // 找到第d个字符所对应的子单词查找树
        char c = key.charAt(d);
        x.next[c] = put(x.next[c], key, value, d + 1);
        return x;
    }

    /**
     * 返回键key所对应的值,如果键不存在返回null
     *
     * @param key 键
     * @return 键key所对应的值, 如果键不存在返回null
     */
    @Override
    public Value get(String key) {
        Node x = get(root, key, 0);
        if (x == null) {
            return null;
        }
        return (Value) x.value;
    }

    private Node get(Node x, String key, int d) {
        if (x == null) {
            return null;
        }
        if (d == key.length()) {
            return x;
        }
        // 找到第d个字符所对应的子单词查找树
        char c = key.charAt(d);
        return get(x.next[c], key, d + 1);
    }

    /**
     * 删除键key和它对应的值
     *
     * @param key 要删除的键
     */
    @Override
    public void delete(String key) {
        delete(root, key, 0);
    }

    private Node delete(Node x, String key, int d) {
        if (x == null) {
            return null;
        }
        if (d == key.length()) {
            if (x.value != null) {
                n--;
            }
            x.value = null;
        } else {
            char c = key.charAt(d);
            x.next[c] = delete(x.next[c], key, d + 1);
        }
        if (x.value != null) {
            return x;
        }
        for (char c = 0; c < R; c++) {
            if (x.next[c] != null) {
                return x;
            }
        }
        return null;
    }

    /**
     * 返回ture，如果表中存在该键和它的值
     *
     * @param key 键
     * @return ture，如果表中存在该键和它的值
     */
    @Override
    public boolean contains(String key) {
        return get(key) != null;
    }

    /**
     * 返回true，如果表为空
     *
     * @return true，如果表为空
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * 返回字符串s的前缀中最长的键
     *
     * @param s 字符串
     * @return 字符串s的前缀中最长的键
     */
    @Override
    public String longestPrefixOf(String s) {
        int length = search(root, s, 0, 0);
        return s.substring(length);
    }

    private int search(Node x, String s, int d, int length) {
        if (x == null) {
            return length;
        }
        if (x.value != null) {
            length = d;
        }
        if (d == s.length()) {
            return length;
        }
        char c = s.charAt(d);
        return search(x.next[c], s, d + 1, length);
    }

    /**
     * 返回所有以s为前缀的键的集合
     *
     * @param pre 前缀字符串
     * @return 所有以s为前缀的键的集合
     */
    @Override
    public Iterable<String> keysWithPrefix(String pre) {
        Queue<String> queue = new Queue<>();
        collect(get(root, pre, 0), pre, queue);
        return queue;
    }

    private void collect(Node x, String pre, Queue<String> q) {
        if (x == null) {
            return;
        }
        // 找到整一个字符串,将字符串加入队列中
        if (x.value != null) {
            q.enqueue(pre);
        }
        for (char c = 0; c < R; c++) {
            collect(x.next[c], pre + c, q);
        }
    }

    /**
     * 返回所有和字符串s匹配的键（“.”能匹配任意字符）
     *
     * @param pat 匹配字符串
     * @return 所有和字符串s匹配的键（“.”能匹配任意字符）
     */
    @Override
    public Iterable<String> keysThatMatch(String pat) {
        Queue<String> queue = new Queue<>();
        collect(root, "", pat, queue);
        return queue;
    }

    private void collect(Node x, String pre, String pat, Queue<String> q) {
        int d = pre.length();
        if (x == null) {
            return;
        }
        // 找到整一个字符串,将字符串加入队列中
        if (d == pat.length() && x.value != null) {
            q.enqueue(pre);
        }
        // 只查找与匹配字符串长度相同的字符串
        if (d == pat.length()) {
            return;
        }
        char next = pat.charAt(d);
        for (char c = 0; c < R; c++) {
            // 存在通配符‘.’则要处理所有链接
            if (next == '.' || next == c) {
                collect(x.next[c], pre + c, pat, q);
            }
        }
    }

    /**
     * 返回键值对的数量
     * 延时实现版本
     *
     * @return 键值对的数量
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 返回符号表中所有的键的集合
     *
     * @return 符号表中所有的键的集合
     */
    @Override
    public Iterable<String> keys() {
        return keysWithPrefix("");
    }

    /**
     * 结点内部类，由于不能使用泛型数组，所有该类必须是静态类，而该静态类不能使用Value
     */
    private static class Node {
        private final Node[] next = new Node[R];
        private Object value;
    }
}
