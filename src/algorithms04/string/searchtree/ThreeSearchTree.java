package algorithms04.string.searchtree;

import edu.princeton.cs.algs4.Queue;

/**
 * 基于三向单词查找树的符号表
 *
 * @author HHH
 */
public class ThreeSearchTree<Value> implements StringStBase<Value> {
    private Node root;
    private int n;

    /**
     * 向表中插入键值对(如果值为null，则删除键)
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void put(String key, Value value) {
        root = put(root, key, value, 0);
    }

    private Node put(Node x, String key, Value value, int d) {
        char c = key.charAt(d);
        if (x == null) {
            x = new Node();
            x.c = c;
        }
        if (c < x.c) {
            x.left = put(x.left, key, value, d);
        } else if (c > x.c) {
            x.right = put(x.right, key, value, d);
        } else if (d < key.length() - 1) {
            x.mid = put(x.mid, key, value, d + 1);
        } else {
            if (x.value == null) {
                n++;
            }
            x.value = value;
        }
        return x;
    }

    /**
     * 返回键key所对应的值,如果键不存在返回null
     *
     * @param key 键
     * @return 键key所对应的值, 如果键不存在返回null
     */
    @Override
    public Value get(String key) {
        Node x = get(root, key, 0);
        if (x == null) {
            return null;
        }
        return x.value;
    }

    private Node get(Node x, String key, int d) {
        if (x == null) {
            return null;
        }
        char c = key.charAt(d);
        if (c < x.c) {
            return get(x.left, key, d);
        } else if (c > x.c) {
            return get(x.right, key, d);
        } else if (d < key.length() - 1) {
            return get(x.mid, key, d + 1);
        } else {
            return x;
        }
    }

    /**
     * TODO: 三向单词查找树delete()为实现
     * 删除键key和它对应的值
     *
     * @param key 要删除的键
     */
    @Override
    public void delete(String key) {

    }

    /**
     * 返回ture，如果表中存在该键和它的值
     *
     * @param key 键
     * @return ture，如果表中存在该键和它的值
     */
    @Override
    public boolean contains(String key) {
        return get(key) != null;
    }

    /**
     * 返回true，如果表为空
     *
     * @return true，如果表为空
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * 返回字符串s的前缀中最长的键
     *
     * @param s 字符串
     * @return 字符串s的前缀中最长的键
     */
    @Override
    public String longestPrefixOf(String s) {
        if (s == null) {
            throw new IllegalArgumentException("calls longestPrefixOf() with null argument");
        }
        if (s.length() == 0) {
            return null;
        }
        int length = 0;
        Node x = root;
        int i = 0;
        while (x != null && i < s.length()) {
            char c = s.charAt(i);
            if (c < x.c) {
                x = x.left;
            } else if (c > x.c) {
                x = x.right;
            } else {
                i++;
                if (x.value != null) {
                    length = i;
                }
                x = x.mid;
            }
        }
        return s.substring(0, length);
    }

    /**
     * 返回所有以s为前缀的键的集合
     *
     * @param s 前缀字符串
     * @return 所有以s为前缀的键的集合
     */
    @Override
    public Iterable<String> keysWithPrefix(String s) {
        if (s == null) {
            throw new IllegalArgumentException("calls keysWithPrefix() with null argument");
        }
        Queue<String> queue = new Queue<>();
        Node x = get(root, s, 0);
        if (x == null) {
            return queue;
        }
        if (x.value != null) {
            queue.enqueue(s);
        }
        collect(x.mid, new StringBuilder(s), queue);
        return queue;
    }

    private void collect(Node x, StringBuilder prefix, Queue<String> queue) {
        if (x == null) {
            return;
        }
        collect(x.left, prefix, queue);
        if (x.value != null) {
            queue.enqueue(prefix.toString() + x.c);
        }
        collect(x.mid, prefix.append(x.c), queue);
        prefix.deleteCharAt(prefix.length() - 1);
        collect(x.right, prefix, queue);
    }

    /**
     * 返回所有和字符串s匹配的键（“.”能匹配任意字符）
     *
     * @param s 匹配字符串
     * @return 所有和字符串s匹配的键（“.”能匹配任意字符）
     */
    @Override
    public Iterable<String> keysThatMatch(String s) {
        Queue<String> queue = new Queue<>();
        collect(root, new StringBuilder(), 0, s, queue);
        return queue;
    }

    private void collect(Node x, StringBuilder prefix, int i, String pattern, Queue<String> queue) {
        if (x == null) {
            return;
        }
        char c = pattern.charAt(i);
        char wildcardCharacter = '.';
        if (c == wildcardCharacter || c < x.c) {
            collect(x.left, prefix, i, pattern, queue);
        }
        if (c == wildcardCharacter || c == x.c) {
            if (i == pattern.length() - 1 && x.value != null) {
                queue.enqueue(prefix.toString() + x.c);
            }
            if (i < pattern.length() - 1) {
                collect(x.mid, prefix.append(x.c), i + 1, pattern, queue);
                prefix.deleteCharAt(prefix.length() - 1);
            }
        }
        if (c == wildcardCharacter || c > x.c) {
            collect(x.right, prefix, i, pattern, queue);
        }
    }

    /**
     * 返回键值对的数量
     *
     * @return 键值对的数量
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 返回符号表中所有的键的集合
     *
     * @return 符号表中所有的键的集合
     */
    @Override
    public Iterable<String> keys() {
        Queue<String> queue = new Queue<>();
        collect(root, new StringBuilder(), queue);
        return queue;
    }

    private class Node {
        private char c;
        private Node left;
        private Node mid;
        private Node right;
        private Value value;
    }
}
