package algorithms04.string.searchtree;

/**
 * 以字符串为键的符号表API
 * 不接受重复键或空键，值不能为空
 *
 * @author HHH
 */
public interface StringStBase<Value> {
    /**
     * ASCII编码允许的字符数量
     */
    int ASCII = 128;
    /**
     * 扩展了ASCII编码允许的字符数量
     */
    int EXTENDED_ASCII = 256;
    /**
     * Unicode16编码允许的字符数量
     */
    int UNICODE16 = 65536;

    /**
     * 向表中插入键值对(如果值为null，则删除键)
     *
     * @param key   键
     * @param value 值
     */
    void put(String key, Value value);

    /**
     * 返回键key所对应的值,如果键不存在返回null
     *
     * @param key 键
     * @return 键key所对应的值, 如果键不存在返回null
     */
    Value get(String key);

    /**
     * 删除键key和它对应的值
     *
     * @param key 要删除的键
     */
    void delete(String key);

    /**
     * 返回ture，如果表中存在该键和它的值
     *
     * @param key 键
     * @return ture，如果表中存在该键和它的值
     */
    boolean contains(String key);

    /**
     * 返回true，如果表为空
     *
     * @return true，如果表为空
     */
    boolean isEmpty();

    /**
     * 返回字符串s的前缀中最长的键
     *
     * @param s 字符串
     * @return 字符串s的前缀中最长的键
     */
    String longestPrefixOf(String s);

    /**
     * 返回所有以s为前缀的键的集合
     *
     * @param s 前缀字符串
     * @return 所有以s为前缀的键的集合
     */
    Iterable<String> keysWithPrefix(String s);

    /**
     * 返回所有和字符串s匹配的键（“.”能匹配任意字符）
     *
     * @param s 匹配字符串
     * @return 所有和字符串s匹配的键（“.”能匹配任意字符）
     */
    Iterable<String> keysThatMatch(String s);

    /**
     * 返回键值对的数量
     *
     * @return 键值对的数量
     */
    int size();

    /**
     * 返回符号表中所有的键的集合
     *
     * @return 符号表中所有的键的集合
     */
    Iterable<String> keys();
}
