package algorithms04.string.datacompression;

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.MinPQ;

/**
 * 基于霍夫曼压缩的数据压缩
 *
 * @author HHH
 */
public class Huffman {
    public static final int R = 256;

    /**
     * 不实例化。
     */
    private Huffman() {}

    public static void main(String[] args) {
        String expand = "+";
        String compress = "-";
        if (expand.equals(args[0])) {
            expand();
        }
        if (compress.equals(args[0])) {
            compress();
        }
    }

    public static void expand() {
        Node root = readTrie();
        int n = BinaryStdIn.readInt();
        for (int i = 0; i < n; i++) {
            Node x = root;
            while (!x.isLeaf()) {
                if (BinaryStdIn.readBoolean()) {
                    x = x.right;
                } else {
                    x = x.left;
                }
            }
            BinaryStdOut.write(x.ch);
        }
        BinaryStdOut.close();
    }

    /**
     * 从比特流地前序表示中重建单词查找树
     *
     * @return 单词查找树地根结点
     */
    private static Node readTrie() {
        if (BinaryStdIn.readBoolean()) {
            return new Node(BinaryStdIn.readChar(), 0, null, null);
        }
        return new Node('\0', 0, readTrie(), readTrie());
    }

    public static void compress() {
        // 第一轮读取输入
        String s = BinaryStdIn.readString();
        char[] input = s.toCharArray();
        // 统计频率
        int[] frequencies = new int[R];
        for (char c : input) {
            frequencies[c]++;
        }
        // 构建霍夫曼编码树
        Node root = buildTrie(frequencies);
        // 递归地构造编译表
        String[] st = new String[R];
        buildCode(st, root, "");
        // 递归打印解码用单词查找树
        writeTrie(root);
        // 打印字符总数
        BinaryStdOut.write(input.length);

        // 第二轮读取输入使用霍夫曼编码处理输入
        for (char c : input) {
            String code = st[c];
            for (int j = 0; j < code.length(); j++) {
                BinaryStdOut.write(code.charAt(j) == '1');
            }
        }
        BinaryStdOut.close();
    }

    /**
     * 利用frequencies数组构造一棵霍夫曼编码树
     *
     * @param frequencies 包含字符频率的数组
     * @return 根结点
     */
    private static Node buildTrie(int[] frequencies) {
        // 使用多棵单节点树初始化优先队列
        MinPQ<Node> pq = new MinPQ<>();
        for (char c = 0; c < R; c++) {
            if (frequencies[c] > 0) {
                pq.insert(new Node(c, frequencies[c], null, null));
            }
        }
        // 合并两棵频率很小的树
        while (pq.size() > 1) {
            Node left = pq.delMin();
            Node right = pq.delMin();
            Node parent = new Node('\0', left.frequency + right.frequency, left, right);
            pq.insert(parent);
        }
        return pq.delMin();
    }

    /**
     * 通过递归使用单词查找树构造编译表
     */
    private static void buildCode(String[] st, Node x, String s) {
        if (x.isLeaf()) {
            st[x.ch] = s;
            return;
        }
        buildCode(st, x.left, s + '0');
        buildCode(st, x.right, s + '1');
    }

    /**
     * 将单词查找树写为比特字符串
     */
    private static void writeTrie(Node x) {
        if (x.isLeaf()) {
            BinaryStdOut.write(true);
            BinaryStdOut.write(x.ch);
            return;
        }
        BinaryStdOut.write(false);
        writeTrie(x.left);
        writeTrie(x.right);
    }

    private static class Node implements Comparable<Node> {
        private final Node left;
        private final Node right;
        /**
         * 叶子结点中需要被编码的字符
         * 内部结点不会使用该变量
         */
        private final char ch;
        /**
         * 字符出现的频率
         * 解压过程不会使用该变量
         */
        private final int frequency;

        public Node(char ch, int frequency, Node left, Node right) {
            this.ch = ch;
            this.frequency = frequency;
            this.left = left;
            this.right = right;
        }

        /**
         * 返回true，如果该结点是叶子结点
         *
         * @return true，如果该结点是叶子结点
         */
        public boolean isLeaf() {
            return left == null && right == null;
        }


        @Override
        public int compareTo(Node o) {
            return this.frequency - o.frequency;
        }
    }
}
