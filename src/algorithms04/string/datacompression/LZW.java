package algorithms04.string.datacompression;

import algorithms04.string.searchtree.ThreeSearchTree;
import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

/**
 * 字符串压缩LZW算法<br/>
 * 实现输入为8位的字节流，输出位12位的编码
 *
 * @author HHH
 */
public class LZW {
    /**
     * 扩展的ASCII
     */
    private static final int R = 256;
    /**
     * 编码总数 = 2 ^ 12
     */
    private static final int L = 4096;
    /**
     * 编码宽度
     */
    private static final int W = 12;

    /**
     * 不实例化。
     */
    private LZW() {
    }

    /**
     * 压缩
     */
    public static void compress() {
        String input = BinaryStdIn.readString();
        ThreeSearchTree<Integer> st = new ThreeSearchTree<>();
        for (int i = 0; i < R; i++) {
            st.put("" + (char) i, i);
        }
        // R为文件结束(EOF)的编码
        int code = R + 1;
        while (input.length() > 0) {
            // 找到匹配的最长键
            String s = st.longestPrefixOf(input);
            // 打印出s的编码
            BinaryStdOut.write(st.get(s), W);

            int t = s.length();
            // 将s加入符号表
            if (t < input.length() && code < L) {
                st.put(input.substring(0, t + 1), code++);
            }
            input = input.substring(t);
        }
        BinaryStdOut.write(R, W);
        BinaryStdOut.close();
    }

    /**
     * 解压
     */
    public static void expand() {
        String[] st = new String[L];
        // 下一个待补全的编码值
        int i;
        for (i = 0; i < R; i++) {
            st[i] = "" + (char) i;
        }
        // 不使用，文件结束标记(EOF)的前瞻字符
        st[i++] = " ";

        int codeword = BinaryStdIn.readInt(W);
        String val = st[codeword];
        while (true) {
            // 输出当前子字符串
            BinaryStdOut.write(val);
            codeword = BinaryStdIn.readInt(W);
            if (codeword == R) {
                break;
            }
            // 获取下一个编码
            String s = st[codeword];
            // 前瞻字符不可用时，根据上一个字符串的首字母得到编码的字符串
            if (i == codeword) {
                s = val + val.charAt(0);
            }
            if (i < L) {
                st[i++] = val + s.charAt(0);
            }
            val = s;
        }
        BinaryStdOut.close();
    }

    public static void main(String[] args) {
        String expand = "+";
        String compress = "-";
        if (expand.equals(args[0])) {
            expand();
        }
        if (compress.equals(args[0])) {
            compress();
        }
    }
}
