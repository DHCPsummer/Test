package algorithms04.string.datacompression;

import edu.princeton.cs.algs4.Alphabet;
import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

/**
 * 基于双位压缩的数据压缩
 *
 * @author HHH
 */
public class Genome {
    /**
     * 不实例化。
     */
    private Genome() {}

    public static void main(String[] args) {
        String expand = "+";
        String compress = "-";
        if (expand.equals(args[0])) {
            expand();
        }
        if (compress.equals(args[0])) {
            compress();
        }
    }

    /**
     * 压缩
     */
    public static void compress() {
        Alphabet dna = new Alphabet("ACTG");
        String s = BinaryStdIn.readString();
        int n = s.length();
        BinaryStdOut.write(n);
        for (int i = 0; i < n; i++) {
            int d = dna.toIndex(s.charAt(i));
            BinaryStdOut.write(d, dna.lgR());
        }
        BinaryStdOut.close();
    }

    /**
     * 解压
     */
    public static void expand() {
        Alphabet dna = new Alphabet("ACTG");
        int w = dna.lgR();
        int n = BinaryStdIn.readInt();
        for (int i = 0; i < n; i++) {
            char c = BinaryStdIn.readChar(w);
            BinaryStdOut.write(dna.toChar(c));
        }
        BinaryStdOut.close();
    }
}