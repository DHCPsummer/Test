package algorithms04.string.datacompression;

import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.StdOut;

/**
 * 将比特流打印在标准输出上<br/>
 * % java BinaryDump 16 < abra.txt
 *
 * @author HHH
 */
public class BinaryDump {
    public static void main(String[] args) {
        int width = Integer.parseInt(args[0]);
        int cnt;
        for (cnt = 0; !BinaryStdIn.isEmpty(); cnt++) {
            if (width == 0) {
                BinaryStdIn.readBoolean();
                continue;
            }
            if (cnt != 0 && cnt % width == 0) {
                StdOut.println();
            }
            if (BinaryStdIn.readBoolean()) {
                StdOut.print("1");
            } else {
                StdOut.print("0");
            }
        }
        StdOut.println();
        StdOut.println(cnt + " bits\n" + cnt / 8.0 +"byte\n" + cnt/8/1024.0 + "kb\n" + cnt/8/1024/1024.0 + "MB");
    }
}
