package algorithms04.string;

import algorithms04.graphs.digraph.Digraph;
import algorithms04.graphs.digraph.DirectedDepthFirstSearch;
import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.Stack;

/**
 * 正则表达式的模式匹配<br/>
 * 使用了NFA（非确定有限状态机）去匹配
 *
 * @author HHH
 */
public class RegularExpressions {
    /**
     * 匹配转换
     */
    private final char[] re;
    /**
     * 存储空转换(不用匹配文本)可以到达的状态
     */
    private final Digraph g;
    /**
     * 状态数量
     */
    private final int m;

    /**
     * 用regexp构造一个NFA
     *
     * @param regexp 正则表达式
     */
    public RegularExpressions(String regexp) {
        Stack<Integer> ops = new Stack<>();
        re = regexp.toCharArray();
        m = re.length;
        g = new Digraph(m + 1);

        for (int i = 0; i < m; i++) {
            int lp = i;
            if (re[i] == '(' || re[i] == '|') {
                ops.push(i);
            } else if (re[i] == ')') {
                int or = ops.pop();
                if (re[or] == '|') {
                    lp = ops.pop();
                    g.addEdge(lp, or + 1);
                    g.addEdge(or, i);
                } else {
                    lp = or;
                }
            }

            if (i < m - 1 && re[i + 1] == '*') {
                g.addEdge(lp, i + 1);
                g.addEdge(i + 1, lp);
            }
            if (re[i] == '(' || re[i] == '*' || re[i] == ')') {
                g.addEdge(i, i + 1);
            }
        }
    }

    /**
     * 返回true,如果NFA能识别文本txt
     * @param txt 文本
     * @return true,如果NFA能识别文本txt
     */
    public boolean recognizes(String txt) {
        Bag<Integer> pc = new Bag<>();
        DirectedDepthFirstSearch dfs = new DirectedDepthFirstSearch(g,0);
        for (int v = 0; v < g.v(); v++) {
            if (dfs.marked(v)) {
                pc.add(v);
            }
        }
        // 计算txt[i+1]可能到达的所有NFA状态
        for (int i = 0; i < txt.length(); i++) {
            Bag<Integer> match = new Bag<>();
            for (int v : pc) {
                if (v < m) {
                    if (re[v] == txt.charAt(i) || re[v] == '.') {
                        match.add(v+1);
                    }
                }
            }
            pc = new Bag<>();
            dfs = new DirectedDepthFirstSearch(g, match);
            for (int v = 0; v < g.v(); v++) {
                if (dfs.marked(v)) {
                    pc.add(v);
                }
            }
        }

        for (int v : pc) {
            if (v == m) {
                return true;
            }
        }
        return false;
    }
}
