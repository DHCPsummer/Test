package algorithms04.string.searchsubstring;

/**
 * 字符串查找KMP算法<br/>
 * 只适合扩展ASCII(r = 256)
 *
 * @author HHH
 */
public class KnuthMorrisPratt implements SearchSubstringBase {
    private final String pat;
    private final int[][] dfa;

    /**
     * 使用模式字符串构造DFA
     *
     * @param pat 模式字符串
     */
    public KnuthMorrisPratt(String pat) {
        this.pat = pat;
        int m = pat.length();
        int r = 265;
        dfa = new int[r][m];
        dfa[pat.charAt(0)][0] = 1;
        for (int x = 0, j = 1; j < m; j++) {
            // 复制匹配失败情况下的值
            for (int c = 0; c < r; c++) {
                dfa[c][j] = dfa[c][x];
            }
            // 设置匹配成功情况下的值
            dfa[pat.charAt(j)][j] = j + 1;
            // 更新重启状态
            x = dfa[pat.charAt(j)][x];
        }
    }

    /**
     * 返回模式字符串在文本字符串中第一次出现的索引。
     *
     * @param txt 文本字符串
     * @return 模式字符串在文本字符串中第一次出现的索引;如果没有这样的匹配
     */
    @Override
    public int search(String txt) {
        int i;
        int j;
        int n = txt.length();
        int m = pat.length();
        for (i = 0, j = 0; i < n && j < m; i++) {
            j = dfa[txt.charAt(i)][j];
        }
        // 是否找到pat
        if (j == m) {
            return i - m;
        } else {
            return n;
        }
    }
}
