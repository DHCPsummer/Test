package algorithms04.string.searchsubstring;

/**
 * @author HHH
 */
public class ViolenceFind {
    /**
     * 暴力子字符串查找算法<br/>
     * 在txt中寻找pat<br/>
     * 返回字符串pat第一个字符在文本txt中的索引，如果找到，否则返回文本txt的长度
     *
     * @param txt 文本
     * @param pat 要寻找的子字符串
     * @return 字符串pat第一个字符在文本txt中的索引，如果找到，否则返回文本txt的长度
     */
    public static int search(String txt, String pat) {
        // 跟踪文本
        int i;
        // 跟踪要找的子字符串
        int j;
        int m = pat.length();
        int n = txt.length();
        for (i = 0, j = 0; i < n && j < m; i++) {
            if (txt.charAt(i) == pat.charAt(j)) {
                j++;
            } else {
                // i和j指向的字符不匹配，显式回退
                i -= j;
                j = 0;
            }
        }
        // 是否找到
        if (j == m) {
            return i - m;
        } else {
            return n;
        }
    }
}
