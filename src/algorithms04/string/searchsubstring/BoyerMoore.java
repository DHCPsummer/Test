package algorithms04.string.searchsubstring;

/**
 * 子字符串查找BM算法<br>
 * 只适合扩展ASCII(r = 256)
 *
 * @author HHH
 */
public class BoyerMoore implements SearchSubstringBase{
    private final int[] right;
    private final String pat;

    public BoyerMoore(String pat) {
        this.pat = pat;
        int m = pat.length();
        int r = 256;
        right = new int[r];
        // 不包含在模式字符串中的字符的值为1
        for (int c = 0; c < r; c++) {
            right[c] = -1;
        }
        // 包含在模式字符串中的字符的值为它在其中出现的最右位置
        for (int j = 0; j < m; j++) {
            right[pat.charAt(j)] = j;
        }
    }

    /**
     * 返回模式字符串在文本字符串中第一次出现的索引。
     *
     * @param txt 文本字符串
     * @return 模式字符串在文本字符串中第一次出现的索引;如果没有这样的匹配
     */
    @Override
    public int search(String txt) {
        int n = txt.length();
        int m = pat.length();
        int skip;
        for (int i = 0; i <= n - m; i += skip) {
            skip = 0;
            for (int j = m - 1; j >= 0; j--) {
                if (pat.charAt(j) != txt.charAt(i + j)) {
                    skip = j - right[txt.charAt(i + j)];
                    if (skip < 1) {
                        skip = 1;
                    }
                    break;
                }
            }
            // 找到匹配
            if (skip == 0) {
                return i;
            }
        }
        // 未找到匹配
        return n;
    }
}
