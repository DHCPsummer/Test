package algorithms04.string.searchsubstring;

/**
 * 子字符串查找API
 *
 * @author HHH
 */
public interface SearchSubstringBase {
    /**
     * 返回模式字符串在文本字符串中第一次出现的索引。
     *
     * @param txt 文本字符串
     * @return 模式字符串在文本字符串中第一次出现的索引;如果没有这样的匹配
     */
    int search(String txt);
}
