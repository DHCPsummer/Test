package algorithms04.string.searchsubstring;

import java.math.BigInteger;
import java.util.Random;

/**
 * 寻找子字符串（RK）算法<br/>
 * 只适合扩展ASCII(r = 256)
 *
 * @author HHH
 */
public class RabinKarp implements SearchSubstringBase{
    /**
     * 字母表大小
     */
    private static final int R = 256;
    /**
     * 模式字符串(仅拉斯维加斯算法需要)
     */
    private final String pat;
    /**
     * 模式字符串的长度
     */
    private final int m;
    /**
     * 一个很大的素数
     */
    private final long q;
    /**
     * 模式字符串的散列值
     */
    private final long patHash;
    /**
     * R ^ (M-1) % Q
     */
    private long rm;

    public RabinKarp(String pat) {
        this.pat = pat;
        this.m = pat.length();
        q = longRandomPrime();
        rm = 1;
        // 计算R ^ (M-1) % Q
        for (int i = 1; i <= m - 1; i++) {
            // 用于减去第一个数字是的计算
            rm = (R * rm) % q;
        }
        patHash = hash(pat, m);
    }

    /**
     * 一个随机的31位素数
     */
    private static long longRandomPrime() {
        BigInteger prime = BigInteger.probablePrime(31, new Random());
        return prime.longValue();
    }

    /**
     * 返回模式字符串在文本字符串中第一次出现的索引。
     *
     * @param txt 文本字符串
     * @return 模式字符串在文本字符串中第一次出现的索引;如果没有这样的匹配
     */
    @Override
    public int search(String txt) {
        int n = txt.length();
        if (n < m) {
            return n;
        }
        long txtHash = hash(txt, m);
        // 检查在偏移量0处是否匹配
        if ((patHash == txtHash) && check(txt, 0)) {
            return 0;
        }
        // 检查散列匹配;如果散列匹配，检查精确匹配
        for (int i = m; i < n; i++) {
            // 减去第一个数字，加上最后一个数字，再次检查匹配
            txtHash = (txtHash + q - rm * txt.charAt(i - m) % q) % q;
            txtHash = (txtHash * R + txt.charAt(i)) % q;
            // 匹配
            int offset = i - m + 1;
            if ((patHash == txtHash) && check(txt, offset)) {
                return offset;
            }
        }
        // 不匹配
        return n;
    }

    /**
     * 拉斯维加斯的版本:pat[] 是否匹配 txt[i..i-m+1] ?
     */
    private boolean check(String txt, int i) {
        for (int j = 0; j < m; j++) {
            if (pat.charAt(j) != txt.charAt(i + j)) {
                return false;
            }
        }
        return true;
    }

     /**
      * 蒙特卡罗版本: 总是返回真
      */
     private boolean check(int i) {
        return true;
    }

    /**
     * 计算key[0...m-1]的哈希值。
     */
    private long hash(String key, int m) {
        long h = 0;
        for (int j = 0; j < m; j++) {
            h = (R * h + key.charAt(j)) % q;
        }
        return h;
    }
}
