package algorithms04.string.stringsort;

import algorithms04.sort.Sort;

/**
 * 字符串排序
 * <p>
 * 低位优先排序LSD-leastSignificantDigitFirstSort();<br/>
 * 高位优先排序MSD-mostSignificantDigitFirstSort();<br/>
 * 三向字符串快速排序（结合了LSD和MSD)-quick3Sort();
 * </p>
 *
 * @author HHH
 */
public class StringSort {
    private static final int ASCII = 128;
    private static final int EXTENDED_ASCII = 256;
    private static final int UNICODE16 = 65536;
    /**
     * 基数
     */
    private static final int R = EXTENDED_ASCII;
    /**
     * 小数组的切换阀值
     */
    private static final int M = 0;
    private final Sort sort = new Sort();
    /**
     * 辅助数组
     */
    private String[] aux;

    /**
     * 低位优先排序LSD<br/>
     * 通过前w个字符将a[]用键索引计数法排序<br/>
     * 数组中每个字符串的长度都要一样
     *
     * @param a 数组
     */
    public void leastSignificantDigitFirstSort(String[] a) {
        if (a.length == 0) {
            return;
        }
        String s = a[0];
        leastSignificantDigitFirstSort(a, s.length());
    }

    /**
     * @param a 数组
     * @param w 字符串有多少个字符
     */
    private void leastSignificantDigitFirstSort(String[] a, int w) {
        int n = a.length;
        aux = new String[n];
        // 根据第d个字符用键索引计数法排序
        for (int d = w - 1; d >= 0; d--) {
            // 计算出现的频率
            int[] count = new int[R + 1];
            for (String s : a) {
                count[s.charAt(d) + 1]++;
            }
            // 将频率转换为索引
            for (int j = 0; j < R; j++) {
                count[j + 1] += count[j];
            }
            // 将元素分类
            for (String s : a) {
                aux[count[s.charAt(d)]++] = s;
            }
            // 回写
            System.arraycopy(aux, 0, a, 0, n);
        }
    }

    /**
     * 高位优先排序MSD
     *
     * @param a 字符串数组
     */
    public void mostSignificantDigitFirstSort(String[] a) {
        int n = a.length;
        aux = new String[n];
        mostSignificantDigitFirstSort(a, 0, n - 1, 0);
    }

    /**
     * 将第d个字符为键将数组a[lo]至a[hi]排序
     */
    private void mostSignificantDigitFirstSort(String[] a, int lo, int hi, int d) {
        // 小数组使用插入排序
        if (hi <= lo + M) {
            sort.insertionSort(a, lo, hi, d);
            return;
        }
        // 计算频率
        int[] count = new int[R + 2];
        for (int i = lo; i <= hi; i++) {
            count[charAt(a[i], d) + 2]++;
        }
        // 将频率转化成索引
        for (int r = 0; r < R + 1; r++) {
            count[r + 1] += count[r];
        }
        // 数据分类
        for (int i = lo; i <= hi; i++) {
            aux[count[charAt(a[i], d) + 1]++] = a[i];
        }
        // 回写
        if (hi + 1 - lo >= 0) {
            System.arraycopy(aux, 0, a, lo, hi + 1 - lo);
        }
        // 递归的以每个字符为键进行
        for (int r = 0; r < R; r++) {
            mostSignificantDigitFirstSort(a, lo + count[r], lo + count[r + 1] - 1, d + 1);
        }
    }

    /**
     * 三向字符串快速排序（结合了LSD和MSD)
     *
     * @param a 字符串数组
     */
    public void quick3Sort(String[] a) {
        quick3Sort(a, 0, a.length - 1, 0);
    }

    private void quick3Sort(String[] a, int lo, int hi, int d) {
        if (hi <= lo) {
            return;
        }
        int lt = lo;
        int gt = hi;
        int v = charAt(a[lo], d);
        int i = lo + 1;
        while (i <= gt) {
            int t = charAt(a[i], d);
            if (t < v) {
                exchange(a, lt++, i++);
            } else if (t > v) {
                exchange(a, i, gt--);
            } else {
                i++;
            }
        }
        // a[lo...lt-1] < v = a[lt...gt] < a[gt...hi]
        quick3Sort(a, lo, lt - 1, d);
        if (v >= 0) {
            quick3Sort(a, lt, gt, d + 1);
        }
        quick3Sort(a, gt + 1, hi, d);
    }

    /**
     * 将字符中中的字符索引转化为数组索引
     * 返回数组索引，如果指定的位置超过了字符串的末尾是该方法返回-1
     *
     * @param s 字符串
     * @param d 字符索引
     * @return 数组索引，如果指定的位置超过了字符串的末尾是该方法返回-1
     */
    private int charAt(String s, int d) {
        if (d < s.length()) {
            return s.charAt(d);
        } else {
            return -1;
        }
    }

    /**
     * 交换数组a中i索引和索引j的两个元素的位置
     *
     * @param a 数组
     * @param i 元素索引
     * @param j 元素索引
     */
    private void exchange(String[] a, int i, int j) {
        String t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
