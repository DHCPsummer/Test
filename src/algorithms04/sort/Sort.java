package algorithms04.sort;

/**
 * 排序类
 *
 * @author HHH
 */
public class Sort {
    /**
     * 冒泡排序
     *
     * @param a 数组
     */
    public void bubbleSort(Comparable[] a) {
        int n = a.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1 - i; j++) {
                if (less(a[j + 1], a[j])) {
                    exchange(a, j + 1, j);
                }
            }
        }
    }

    /**
     * 快排的简单实现
     *
     * @param a 数组
     */
    public void quicksort(int[] a) {
        quicksort(a, 0, a.length - 1);
    }

    private void quicksort(int[] a, int low, int high) {
        if (low > high) {
            return;
        }
        int left = low;
        int right = high;

        // 基准位
        int temp = a[low];
        while (left < right) {
            // 先从右边开始往左递减，找到比temp小的值才停止
            while (temp <= a[right] && left < right) {
                right--;
            }

            // 再看左边开始往右递增，找到比temp大的值才停止
            while (temp >= a[left] && left < right) {
                left++;
            }

            // 满足left < right就交换a[left]和a[right]的值,然后继续循环while(left < right)
            if (left < right) {
                int t = a[left];
                a[left] = a[right];
                a[right] = t;
            }
        }

        // 最后将基准位跟a[left]与a[right]相等的位置,进行交换,此时left = right
        a[low] = a[left];
        a[left] = temp;

        // 左递归
        quicksort(a, low, right - 1);
        // 右递归
        quicksort(a, right + 1, high);
    }

    /**
     * 快排
     *
     * @param a 数组
     */
    public void quickSort(Comparable[] a) {
        quickSort(a, 0, a.length - 1);
    }

    private void quickSort(Comparable[] a, int lo, int hi) {
        if (hi <= lo) {
            return;
        }
        int j = partition(a, lo, hi);

        // 将数组左半部分排序
        quickSort(a, lo, j - 1);
        // 将数组右半部分排序
        quickSort(a, j + 1, hi);
    }

    /**
     * 快排的切分算法
     * 将数组a切分成a[lo...i-1],a[i],a[i+1...hi]
     *
     * @param a  要切分的数组
     * @param lo 数组最左索引
     * @param hi 数组最右索引
     * @return 切分完成后切分元素的索引
     */
    private int partition(Comparable[] a, int lo, int hi) {
        // 左右扫描指针
        int i = lo;
        int j = hi + 1;
        // 切分元素
        Comparable v = a[lo];
        while (true) {
            while (less(a[++i], v)) {
                if (i == hi) {
                    break;
                }
            }
            while (less(v, a[--j])) {
                if (j == lo) {
                    break;
                }
            }
            if (i >= j) {
                break;
            }
            exchange(a, i, j);
        }
        // 将切分元素放到正确的索引上
        exchange(a, lo, j);
        return j;
    }

    /**
     * 插入排序
     *
     * @param a 数组
     */
    public void insertionSort(Comparable[] a) {
        int n = a.length;
        for (int i = 1; i < n; i++) {
            for (int j = i; j > 0 && less(a[j], a[j - 1]); j--) {
                exchange(a, j, j - 1);
            }
        }
    }

    /**
     * 对前d个字符据相同的字符串的插入排序
     *
     * @param a 字符串数组
     */
    public void insertionSort(String[] a, int lo, int hi, int d) {
        for (int i = lo; i <= hi; i++) {
            for (int j = i; j > lo && less(a[j], a[j - 1], d); j--) {
                exchange(a, j, j - 1);
            }
        }
    }

    private boolean less(String v, String w, int d) {
        return v.substring(d).compareTo(w.substring(d)) < 0;
    }

    /**
     * 希尔排序
     *
     * @param a 数组
     */
    public void shellSort(Comparable[] a) {
        int n = a.length;
        int h = 1;

        while (h < n / 3) {
            // h = 1,4,13,40,121,364...
            h = 3 * h + 1;
        }

        while (h >= 1) {
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && less(a[j], a[j - h]); j = j - h) {
                    exchange(a, j, j - h);
                }
            }
            h = h / 3;
        }
    }

    /**
     * 选择排序
     *
     * @param a 数组
     */
    public void selectionSort(Comparable[] a) {
        int n = a.length;
        for (int i = 0; i < n; i++) {
            int min = i;
            for (int j = i + 1; j < n; j++) {
                if (less(a[j], a[min])) {
                    min = j;
                }
            }
            exchange(a, i, min);
        }
    }

    /**
     * 堆排序
     */
    public void heapSort(Comparable[] a) {
        heapSort(a, a.length - 1);
    }

    private void heapSort(Comparable[] a, int n) {
        for (int k = n / 2; k >= 1; k--) {
            sink(a, k, n);
        }
        while (n > 1) {
            exchange(a, 1, n--);
            sink(a, 1, n);
        }
    }

    /**
     * 由上至下的堆有序化（下沉）
     */
    private void sink(Comparable[] a, int k, int n) {
        while (2 * k <= n) {
            int j = 2 * k;
            if (j < n && less(a[j], a[j + 1])) {
                j++;
            }
            if (!less(a[k], a[j])) {
                break;
            }
            exchange(a, k, j);
            k = j;
        }
    }


    /**
     * 自顶向下归并排序
     *
     * @param a 数组
     */
    public void mergeSort(Comparable[] a) {
        mergeSort(a, 0, a.length - 1);
    }

    private void mergeSort(Comparable[] a, int lo, int hi) {
        if (hi <= lo) {
            return;
        }
        int mid = (lo + hi) / 2;
        // 将左边排序
        mergeSort(a, lo, mid);
        // 将右边排序
        mergeSort(a, mid + 1, hi);
        // 归并结果
        merge(a, lo, mid, hi);
    }

    /**
     * 自底向上的归并排序
     *
     * @param a 数组
     */
    public void mergeBuSort(Comparable[] a) {
        int n = a.length;
        for (int sz = 1; sz < n; sz = sz + sz) {
            for (int lo = 0; lo < n - sz; lo = lo + sz + sz) {
                merge(a, lo, lo + sz - 1, Math.min(lo + sz + sz - 1, n - 1));
            }
        }
    }

    /**
     * 原地归并
     *
     * @param a   数组
     * @param lo  数组最左边的索引
     * @param mid 数组中间的索引
     * @param hi  数组最右边的索引
     */
    private void merge(Comparable[] a, int lo, int mid, int hi) {
        // 辅助数组aux
        Comparable[] aux = new Comparable[a.length];
        int i = lo;
        int j = mid + 1;

        if (hi + 1 - lo >= 0) {
            System.arraycopy(a, lo, aux, lo, hi + 1 - lo);
        }

        for (int k = lo; k <= hi; k++) {
            if (i > mid) {
                a[k] = aux[j++];
            } else if (j > hi) {
                a[k] = aux[i++];
            } else if (less(aux[j], aux[i])) {
                a[k] = aux[j++];
            } else {
                a[k] = aux[i++];
            }
        }
    }

    /**
     * 返回true,如果v小于w,否则false
     *
     * @param v 要比较的元素
     * @param w 要比较的元素
     * @return true, 如果v小于w, 否则false
     */
    private boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    /**
     * 交换数组a中i索引和索引j的两个元素的位置
     *
     * @param a 数组
     * @param i 元素索引
     * @param j 元素索引
     */
    private void exchange(Comparable[] a, int i, int j) {
        Comparable t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
