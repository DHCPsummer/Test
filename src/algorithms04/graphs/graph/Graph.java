package algorithms04.graphs.graph;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.In;

/**
 * 图
 *
 * @author HHH
 */
public class Graph implements GraphBase {
    /**
     * 顶点数目
     */
    private int v;
    /**
     * 边的数目
     */
    private int e;
    /**
     * 邻接表
     */
    private Bag<Integer>[] adj;

    /**
     * 构造一个顶点数为v,边数为0的图
     *
     * @param v 顶点数
     */
    public Graph(int v) {
        this.v = v;
        this.e = 0;
        adj = (Bag<Integer>[]) new Bag[v];
        for (int i = 0; i < v; i++) {
            adj[i] = new Bag<>();
        }
    }

    /**
     * 从标准流中读入一个图
     *
     * @param in 标准流url
     */
    public Graph(In in) {
        this(in.readInt());
        int e = in.readInt();
        for (int i = 0; i < e; i++) {
            int v = in.readInt();
            int w = in.readInt();
            addEdge(v, w);
        }
    }

    /**
     * 返回图的顶点总数
     *
     * @return 图所有的顶点数
     */
    @Override
    public int v() {
        return v;
    }

    /**
     * 返回图的边总数
     *
     * @return 图的边总数
     */
    @Override
    public int e() {
        return e;
    }

    /**
     * 向图中添加一条边v-w
     */
    @Override
    public void addEdge(int w, int v) {
        adj[v].add(w);
        adj[w].add(v);
        e++;
    }

    /**
     * 返回和v顶点相连的所有顶点的集合
     *
     * @param v 顶点
     * @return 和v顶点相连的所有顶点的集合
     */
    @Override
    public Iterable<Integer> adj(int v) {
        return adj[v];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(v).append("个顶点").append(e).append("条边\n");
        for (int i = 0; i < v; i++) {
            sb.append(i).append(":");
            for (int w : adj(i)) {
                sb.append(w).append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}