package algorithms04.graphs.graph;

/**
 * 路径API
 *
 * @author HHH
 */
public interface PathBase {
    /**
     * 返回true,如果存在起点s到顶点v的路径,否则false
     *
     * @param v 顶点
     * @return true, 如果存在起点s到顶点v的路径, 否则false
     */
    boolean hasPathTo(int v);

    /**
     * 返回起点s到顶点v的所有路径的集合,如果路径不存在返回null
     *
     * @param v 顶点
     * @return 起点s到顶点v的所有路径的集合, 如果路径不存在返回null
     */
    Iterable<Integer> pathTo(int v);
}
