package algorithms04.graphs.graph;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.ST;

/**
 * 无向图符号图
 *
 * @author HHH
 */
public class SymbolGraph implements SymbolGraphBase {
    /**
     * 符号名->索引
     */
    private final ST<String, Integer> st;
    /**
     * 索引->符号名
     */
    private final String[] keys;
    private final Graph g;

    public SymbolGraph(String stream, String sp) {
        // 第一遍构造索引
        st = new ST<>();
        In in = new In(stream);
        while (in.hasNextLine()) {
            String[] a = in.readLine().split(sp);
            for (String s : a) {
                if (!st.contains(s)) {
                    st.put(s, st.size());
                }
            }
        }

        // 获取顶点名的反向索引
        keys = new String[st.size()];
        for (String name : st.keys()) {
            keys[st.get(name)] = name;
        }

        // 第二遍构造图
        g = new Graph(st.size());
        in = new In(stream);
        while (in.hasNextLine()) {
            String[] a = in.readLine().split(sp);
            int v = st.get(a[0]);
            for (int i = 1; i < a.length; i++) {
                g.addEdge(v,st.get(a[i]));
            }
        }
    }

    /**
     * 返回true，如果key是一个顶点
     *
     * @param key 名称
     * @return true，如果key是一个顶点
     */
    @Override
    public boolean contains(String key) {
        return st.contains(key);
    }

    /**
     * 返回key的索引
     *
     * @param key 名称
     * @return key的索引
     */
    @Override
    public int index(String key) {
        return st.get(key);
    }

    /**
     * 返回索引v的名称
     *
     * @param v 索引
     * @return 索引v的名称
     */
    @Override
    public String name(int v) {
        return keys[v];
    }

    /**
     * 返回隐藏的Graph对象
     *
     * @return 隐藏的Graph对象
     */
    @Override
    public Graph g() {
        return g;
    }
}
