package algorithms04.graphs.graph;

/**
 * 使用DFS找出图中的所有连通分量
 *
 * @author HHH
 */
public class Cc implements CcBase{
    private final boolean[] marked;
    private final int[] id;
    private int count;

    /**
     * 预处理构造的函数
     */
    public Cc(Graph g) {
        marked = new boolean[g.v()];
        id = new int[g.v()];
        for (int s = 0; s < g.v(); s++) {
            if (!marked[s]) {
                dfs(g,s);
                count++;
            }
        }
    }

    private void dfs(Graph g, int v) {
        marked[v] = true;
        id[v] = count;
        for (int w : g.adj(v)) {
            if (!marked[w]) {
                dfs(g, w);
            }
        }
    }

    /**
     * 返回true,如果顶点v和顶点w连通,否则false
     *
     * @param v 顶点
     * @param w 顶点
     * @return true, 如果顶点v和顶点w连通, 否则false
     */
    @Override
    public boolean connected(int v, int w) {
        return id[v] == id[w];
    }

    /**
     * 返回连通分量数
     *
     * @return 连通分量数
     */
    @Override
    public int count() {
        return count;
    }

    /**
     * 返回顶点v所在的连通分量的表示符(0-count-1)
     *
     * @param v 顶点
     * @return 顶点v所在的连通分量的表示符(0 - count - 1)
     */
    @Override
    public int id(int v) {
        return id[v];
    }
}
