package algorithms04.graphs.graph;

/**
 * 图处理API
 *
 * @author HHH
 */
public interface SearchBase {
    /**
     * 返回true,如果顶点v与起点s连通,否则false
     *
     * @param v 顶点
     * @return true, 如果顶点v与起点s连通, 否则false
     */
    boolean marked(int v);

    /**
     * 返回与顶点s连通的顶点总数
     *
     * @return 与顶点s连通的顶点总数
     */
    int count();
}
