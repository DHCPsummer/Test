package algorithms04.graphs.graph;

/**
 * 符号图API
 *
 * @author HHH
 */
public interface SymbolGraphBase {
    /**
     * 返回true，如果key是一个顶点
     *
     * @param key 名称
     * @return true，如果key是一个顶点
     */
    boolean contains(String key);

    /**
     * 返回key的索引
     *
     * @param key 名称
     * @return key的索引
     */
    int index(String key);

    /**
     * 返回索引v的名称
     *
     * @param v 索引
     * @return 索引v的名称
     */
    String name(int v);

    /**
     * 返回隐藏的Graph对象
     *
     * @return 隐藏的Graph对象
     */
    GraphBase g();
}