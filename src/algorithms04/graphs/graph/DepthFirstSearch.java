package algorithms04.graphs.graph;

/**
 * 深度优先搜索
 *
 * @author HHH
 */
public class DepthFirstSearch implements SearchBase{
    private final boolean[] marked;
    private int count;

    /**
     * 在图g中找到和起点s连通的所有顶点
     *
     * @param g 图
     * @param s 起点
     */
    public DepthFirstSearch(Graph g, int s) {
        marked = new boolean[g.v()];
        dfs(g, s);
    }

    private void dfs(Graph g, int v) {
        marked[v] = true;
        count++;
        for (int w : g.adj(v)) {
            if (!marked[w]) {
                dfs(g, w);
            }
        }
    }

    /**
     * 返回true,如果顶点v与起点s连通,否则false
     *
     * @param v 顶点
     * @return true, 如果顶点v与起点s连通, 否则false
     */
    @Override
    public boolean marked(int v) {
        return marked[v];
    }

    /**
     * 返回与顶点s连通的顶点总数
     *
     * @return 与顶点s连通的顶点总数
     */
    @Override
    public int count() {
        return count;
    }
}
