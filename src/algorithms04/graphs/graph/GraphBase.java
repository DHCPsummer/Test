package algorithms04.graphs.graph;

/**
 * 无向图AIP
 *
 * @author HHH
 */
public interface GraphBase {
    /**
     * 返回图的顶点总数
     *
     * @return 图所有的顶点数
     */
    int v();

    /**
     * 返回图的边总数
     *
     * @return 图的边总数
     */
    int e();

    /**
     * 向图中添加一条边v-w
     * @param v 顶点
     * @param w 顶点
     */
    void addEdge(int v,int w);

    /**
     * 返回和v顶点相连的所有顶点的集合
     * @param v 顶点
     * @return 和v顶点相连的所有顶点的集合
     */
    Iterable<Integer> adj(int v);
}
