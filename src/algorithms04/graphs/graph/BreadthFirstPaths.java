package algorithms04.graphs.graph;

import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.Stack;

/**
 * 广度优先路径
 * 单点路径最短问题
 *
 * @author HHH
 */
public class BreadthFirstPaths implements PathBase {
    /**
     * 顶点
     */
    private final int s;
    private final boolean[] marked;
    /**
     * 从起点到一个顶点已知路径上的最后一个顶点
     */
    private final int[] edgeTo;

    public BreadthFirstPaths(Graph g, int s) {
        marked = new boolean[g.v()];
        edgeTo = new int[g.v()];
        this.s = s;
        bfs(g, s);
    }

    private void bfs(Graph g, int s) {
        Queue<Integer> queue = new Queue<>();
        marked[s] = true;
        queue.enqueue(s);
        while (!queue.isEmpty()) {
            int v = queue.dequeue();
            for (int w : g.adj(v)) {
                if (!marked[w]) {
                    edgeTo[w] = v;
                    marked[w] = true;
                    queue.enqueue(w);
                }
            }
        }
    }

    /**
     * 返回true,如果存在起点s到顶点v的路径,否则false
     *
     * @param v 顶点
     * @return true, 如果存在起点s到顶点v的路径, 否则false
     */
    @Override
    public boolean hasPathTo(int v) {
        return !marked[v];
    }

    /**
     * 返回起点s到顶点v的所有路径的集合,如果路径不存在返回null
     *
     * @param v 顶点
     * @return 起点s到顶点v的所有路径的集合, 如果路径不存在返回null
     */
    @Override
    public Iterable<Integer> pathTo(int v) {
        if (hasPathTo(v)) {
            return null;
        }
        Stack<Integer> path = new Stack<>();
        for (int x = v; x != s; x = edgeTo[x]) {
            path.push(x);
        }
        path.push(s);
        return path;
    }
}
