package algorithms04.graphs.graph;

import edu.princeton.cs.algs4.In;

/**
 * @author HHH
 */
public class Test {
    public static void main(String[] args) {
        In in = new In("D:\\IDEA\\workspace\\Test\\src\\algorithms04\\graphs\\graph\\test.txt");
        Graph graph = new Graph(in);
        BreadthFirstPaths paths = new BreadthFirstPaths(graph, 0);
        Iterable<Integer> iterable = paths.pathTo(6);
        for (Integer integer : iterable) {
            System.out.print(integer + " ");
        }
        System.out.println("-------------");
        DepthFirstPaths dfp = new DepthFirstPaths(graph, 0);
        Iterable<Integer> iterable1 = dfp.pathTo(6);
        for (Integer integer : iterable1) {
            System.out.print(integer + " ");
        }
    }
}
