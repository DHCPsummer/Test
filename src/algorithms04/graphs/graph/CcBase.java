package algorithms04.graphs.graph;

/**
 * 连通分量API
 *
 * @author HHH
 */
public interface CcBase {
    /**
     * 返回true,如果顶点v和顶点w连通,否则false
     *
     * @param v 顶点
     * @param w 顶点
     * @return true, 如果顶点v和顶点w连通, 否则false
     */
    boolean connected(int v, int w);

    /**
     * 返回连通分量数
     *
     * @return 连通分量数
     */
    int count();

    /**
     * 返回顶点v所在的连通分量的表示符(0-count-1)
     *
     * @param v 顶点
     * @return 顶点v所在的连通分量的表示符(0 - count - 1)
     */
    int id(int v);
}
