package algorithms04.graphs.digraph;

/**
 * 有向图的可达性
 *
 * @author HHH
 */
public class DirectedDepthFirstSearch implements DirectedDepthFirstSearchBase {
    private final boolean[] marked;
    private int count;

    /**
     * 在有向图g中找到从起点s可达的所有顶点
     *
     * @param g 图
     * @param s 起点
     */
    public DirectedDepthFirstSearch(Digraph g, int s) {
        marked = new boolean[g.v()];
        dfs(g, s);
    }

    /**
     * 在有向图g中找到从顶点集合sources中的所有顶点可达的所有顶点
     *
     * @param g       图
     * @param sources 顶点集合
     */
    public DirectedDepthFirstSearch(Digraph g, Iterable<Integer> sources) {
        marked = new boolean[g.v()];
        for (int s : sources) {
            if (!marked[s]) {
                dfs(g, s);
            }
        }
    }

    private void dfs(Digraph g, int v) {
        marked[v] = true;
        count++;
        for (int w : g.adj(v)) {
            if (!marked[w]) {
                dfs(g, w);
            }
        }
    }

    /**
     * 返回true,如果顶点v是可达的,否则false
     *
     * @param v 顶点
     * @return true, 如果顶点v是可达的, 否则false
     */
    @Override
    public boolean marked(int v) {
        return marked[v];
    }

    /**
     * 返回顶点s可达的顶点总数
     *
     * @return 顶点s可达的顶点总数
     */
    @Override
    public int count() {
        return count;
    }
}
