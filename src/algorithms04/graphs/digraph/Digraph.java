package algorithms04.graphs.digraph;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.In;

/**
 * 有向图
 *
 * @author HHH
 */
public class Digraph implements DigraphBase{
    /**
     * 顶点数目
     */
    private int v;
    /**
     * 边的数目
     */
    private int e;
    /**
     * 邻接表
     */
    private Bag<Integer>[] adj;

    /**
     * 无参构造器
     */
    public Digraph() {
    }

    /**
     * 构造一个顶点数为v,边数为0的图
     *
     * @param v 顶点数
     */
    public Digraph(int v) {
        this.v = v;
        this.e = 0;
        adj = (Bag<Integer>[]) new Bag[v];
        for (int i = 0; i < v; i++) {
            adj[i] = new Bag<>();
        }
    }

    /**
     * 从标准流中读入一个图
     * @param in 标准流url
     */
    public Digraph(In in) {
        this(in.readInt());
        int e = in.readInt();
        for (int i = 0; i < e; i++) {
            int v = in.readInt();
            int w = in.readInt();
            addEdge(v,w);
        }
    }

    /**
     * 返回图的顶点总数
     *
     * @return 图所有的顶点数
     */
    @Override
    public int v() {
        return v;
    }

    /**
     * 返回图的边总数
     *
     * @return 图的边总数
     */
    @Override
    public int e() {
        return e;
    }

    /**
     * 向图中添加一条边v->w
     */
    @Override
    public void addEdge(int v,int w) {
        adj[v].add(w);
        e++;
    }

    /**
     * 返回和v顶点指出的边所连接的所有顶点的集合
     *
     * @param v 顶点
     * @return 和v顶点指出的边所连接的所有顶点的集合
     */
    @Override
    public Iterable<Integer> adj(int v) {
        return adj[v];
    }

    /**
     * 返回该图的反向图
     *
     * @return 该图的反向图
     */
    @Override
    public Digraph reverse() {
        Digraph r = new Digraph(v);
        for (int i = 0; i < v; i++) {
            for (int w : adj(i)) {
                r.addEdge(w,i);
            }
        }
        return r;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(v).append("个顶点").append(e).append("条边\n");
        for (int i = 0; i < v; i++) {
            sb.append(i).append(":");
            for (int w : adj(i)) {
                sb.append(w).append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
