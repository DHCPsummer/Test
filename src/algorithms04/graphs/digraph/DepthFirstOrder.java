package algorithms04.graphs.digraph;

import algorithms04.graphs.msp.DirectedEdge;
import algorithms04.graphs.msp.EdgeWeightedDigraph;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.Stack;

/**
 * 有向图中基于深度有向搜索的顶点排序
 *
 * @author HHH
 */
public class DepthFirstOrder {
    private final boolean[] marked;
    /**
     * 所有顶点的前序排列，dfs()的递归顺序
     */
    private final Queue<Integer> pre;
    /**
     * 所有顶点的后序排列,顶点遍历完成的顺序
     */
    private final Queue<Integer> post;

    public DepthFirstOrder(Digraph g) {
        marked = new boolean[g.v()];
        pre = new Queue<>();
        post = new Queue<>();
        for (int v = 0; v < g.v(); v++) {
            if (!marked[v]) {
                dfs(g, v);
            }
        }
    }

    /**
     * 确定边加权有向图G的深度优先级
     * @param g 边加权有向图
     */
    public DepthFirstOrder(EdgeWeightedDigraph g) {
        pre = new Queue<>();
        post = new Queue<>();
        marked = new boolean[g.v()];
        for (int v = 0; v < g.v(); v++) {
            if (!marked[v]) {
                dfs(g, v);
            }
        }
    }

    private void dfs(Digraph g, int v) {
        pre.enqueue(v);
        marked[v] = true;
        for (int w : g.adj(v)) {
            if (!marked[w]) {
                dfs(g, w);
            }
        }
        post.enqueue(v);
    }

    private void dfs(EdgeWeightedDigraph g, int v) {
        marked[v] = true;
        pre.enqueue(v);
        for (DirectedEdge e : g.adj(v)) {
            int w = e.to();
            if (!marked[w]) {
                dfs(g, w);
            }
        }
        post.enqueue(v);
    }

    public Iterable<Integer> pre() {
        return pre;
    }

    public Iterable<Integer> post() {
        return post;
    }

    public Iterable<Integer> reversePost() {
        Stack<Integer> reversePost = new Stack<>();
        for (int v : post) {
            reversePost.push(v);
        }
        return reversePost;
    }
}
