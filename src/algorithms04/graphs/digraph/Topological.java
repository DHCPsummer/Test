package algorithms04.graphs.digraph;

import algorithms04.graphs.msp.EdgeWeightedDigraph;
import algorithms04.graphs.msp.EdgeWeightedDirectedCycle;

/**
 * 拓扑排序,优先级调用
 *
 * @author HHH
 */
public class Topological implements TopologicalBase{
    /**
     * 顶点的拓扑顺序
     */
    private Iterable<Integer> order;

    public Topological(Digraph g) {
        DirectedCycle cycleFinder = new DirectedCycle(g);
        if (!cycleFinder.hasCycle()) {
            DepthFirstOrder dfo = new DepthFirstOrder(g);
            order = dfo.reversePost();
        }
    }

    public Topological(EdgeWeightedDigraph g) {
        EdgeWeightedDirectedCycle finder = new EdgeWeightedDirectedCycle(g);
        if (!finder.hasCycle()) {
            DepthFirstOrder dfs = new DepthFirstOrder(g);
            order = dfs.reversePost();
        }
    }

    /**
     * 返回true,如果图是有向无环图
     *
     * @return true, 如果图是有向无环图
     */
    @Override
    public boolean isDirectedAcyclicGraph() {
        return order != null;
    }

    /**
     * 返回拓扑有序的所有顶点的集合
     *
     * @return 拓扑有序的所有顶点的集合
     */
    @Override
    public Iterable<Integer> order() {
        return order;
    }
}
