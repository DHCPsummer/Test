package algorithms04.graphs.digraph;

/**
 * 强连通分量API
 *
 * @author HHH
 */
public interface StronglyCc {
    /**
     * 返回true，如果顶点v和顶点w是强连通的
     *
     * @param v 顶点
     * @param w 顶点
     * @return true，如果顶点v和顶点w是强连通的
     */
    boolean stronglyConnected(int v, int w);

    /**
     * 返回图中的强连通分量的总数
     *
     * @return 图中的强连通分量的总数
     */
    int count();

    /**
     * 返回顶点v所在的强连通分量的标识符(1~count-1)
     * @param v 顶点
     * @return 顶点v所在的强连通分量的标识符(1~count-1)
     */
    int id(int v);
}
