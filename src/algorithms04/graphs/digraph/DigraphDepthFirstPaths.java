package algorithms04.graphs.digraph;

import algorithms04.graphs.graph.PathBase;
import edu.princeton.cs.algs4.Stack;

/**
 * 有向图的深度优先路径
 *
 * @author HHH
 */
public class DigraphDepthFirstPaths implements PathBase {
    /**
     * 顶点
     */
    private final int s;
    private final boolean[] marked;
    /**
     * 从起点到一个顶点已知路径上的最后一个顶点
     */
    private final int[] edgeTo;

    /**
     * 在图g中好处所有起点为s的路径
     *
     * @param g 图
     * @param s 起点
     */
    public DigraphDepthFirstPaths(Digraph g, int s) {
        marked = new boolean[g.v()];
        edgeTo = new int[g.v()];
        this.s = s;
        dfs(g, s);
    }

    private void dfs(Digraph g, int v) {
        marked[v] = true;
        for (int w : g.adj(v)) {
            if (!marked[w]) {
                edgeTo[w] = v;
                dfs(g, w);
            }
        }
    }

    /**
     * 返回true,如果存在起点s到顶点v的路径,否则false
     *
     * @param v 顶点
     * @return true, 如果存在起点s到顶点v的路径, 否则false
     */
    @Override
    public boolean hasPathTo(int v) {
        return !marked[v];
    }

    /**
     * 返回起点s到顶点v的所有路径的集合,如果路径不存在返回null
     *
     * @param v 顶点
     * @return 起点s到顶点v的所有路径的集合, 如果路径不存在返回null
     */
    @Override
    public Iterable<Integer> pathTo(int v) {
        if (hasPathTo(v)) {
            return null;
        }
        Stack<Integer> path = new Stack<>();
        for (int x = v; x != s; x = edgeTo[x]) {
            path.push(x);
        }
        path.push(s);
        return path;
    }
}
