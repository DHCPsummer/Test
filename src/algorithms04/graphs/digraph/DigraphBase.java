package algorithms04.graphs.digraph;

import algorithms04.graphs.graph.GraphBase;

/**
 * 有向图API
 *
 * @author HHH
 */
public interface DigraphBase extends GraphBase {
    /**
     * 返回该图的反向图
     *
     * @return 该图的反向图
     */
    DigraphBase reverse();
}
