package algorithms04.graphs.digraph;

import edu.princeton.cs.algs4.In;

/**
 * @author HHH
 */
public class Test {
    public static void main(String[] args) {
        In in = new In("D:\\IDEA\\workspace\\Test\\src\\algorithms04\\graphs\\digraph\\tinyDG.txt");
        Digraph digraph = new Digraph(in);
        KosarajuStronglyCc cc = new KosarajuStronglyCc(digraph);
        System.out.println(cc.stronglyConnected(2,3));

    }
}
