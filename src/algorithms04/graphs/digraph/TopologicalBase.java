package algorithms04.graphs.digraph;

/**
 * 拓扑排序API
 *
 * @author HHH
 */
public interface TopologicalBase {
    /**
     * 返回true,如果图是有向无环图
     *
     * @return true, 如果图是有向无环图
     */
    boolean isDirectedAcyclicGraph();

    /**
     * 返回拓扑有序的所有顶点的集合
     *
     * @return 拓扑有序的所有顶点的集合
     */
    Iterable<Integer> order();
}
