package algorithms04.graphs.digraph;

/**
 * 使用DFS找出图中的所有强连通分量
 *
 * @author HHH
 */
public class KosarajuStronglyCc implements StronglyCc {
    /**
     * 已访问过的顶点
     */
    private final boolean[] marked;
    /**
     * 强连通分量的标识符
     */
    private final int[] id;
    /**
     * 强连通分量的数量
     */
    private int count;

    public KosarajuStronglyCc(Digraph g) {
        marked = new boolean[g.v()];
        id = new int[g.v()];
        DepthFirstOrder order = new DepthFirstOrder(g.reverse());
        for (int s : order.reversePost()) {
            if (!marked[s]) {
                dfs(g,s);
                count++;
            }
        }
    }

    /**
     * 返回true，如果顶点v和顶点w是强连通的
     *
     * @param v 顶点
     * @param w 顶点
     * @return true，如果顶点v和顶点w是强连通的
     */
    @Override
    public boolean stronglyConnected(int v, int w) {
        return id[v] == id[w];
    }

    /**
     * 返回图中的强连通分量的总数
     *
     * @return 图中的强连通分量的总数
     */
    @Override
    public int count() {
        return count;
    }

    /**
     * 返回顶点v所在的强连通分量的标识符(1~count-1)
     *
     * @param v 顶点
     * @return 顶点v所在的强连通分量的标识符(1 ~ count - 1)
     */
    @Override
    public int id(int v) {
        return id[v];
    }

    private void dfs(Digraph g, int v) {
        marked[v] = true;
        id[v] = count;
        for (int w : g.adj(v)) {
            if (!marked[w]) {
                dfs(g, w);
            }
        }
    }
}
