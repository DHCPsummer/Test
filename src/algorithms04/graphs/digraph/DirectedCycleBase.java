package algorithms04.graphs.digraph;

/**
 * 有向环API
 *
 * @author HHH
 */
public interface DirectedCycleBase {
    /**
     * 返回true如果图含有有向环
     *
     * @return true如果图含有有向环
     */
    boolean hasCycle();

    /**
     * 返回某一个有向环中所有顶点，如果有
     *
     * @return 某一个有向环中所有顶点, 如果有
     */
    Iterable<Integer> cycle();
}
