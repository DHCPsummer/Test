package algorithms04.graphs.digraph;

/**
 * 有向图的可达性API
 *
 * @author HHH
 */
public interface DirectedDepthFirstSearchBase {
    /**
     * 返回true,如果顶点v是可达的,否则false
     *
     * @param v 顶点
     * @return true,如果顶点v是可达的,否则false
     */
    boolean marked(int v);

    /**
     * 返回顶点s可达的顶点总数
     *
     * @return 顶点s可达的顶点总数
     */
    int count();
}
