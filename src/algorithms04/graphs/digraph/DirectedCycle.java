package algorithms04.graphs.digraph;

import edu.princeton.cs.algs4.Stack;

/**
 * 寻找有向图有向环
 *
 * @author HHH
 */
public class DirectedCycle implements DirectedCycleBase {
    private final boolean[] marked;
    private final int[] edgeTo;
    /**
     * 顶点是否在栈中
     */
    private final boolean[] onStack;
    /**
     * 有向环的所有顶点
     */
    private Stack<Integer> cycle;

    /**
     * 在图g中寻找有向环的构造函数
     *
     * @param g 图
     */
    public DirectedCycle(Digraph g) {
        onStack = new boolean[g.v()];
        marked = new boolean[g.v()];
        edgeTo = new int[g.v()];
        for (int i = 0; i < g.v(); i++) {
            if (!marked[i]) {
                dfs(g, i);
            }
        }
    }

    private void dfs(Digraph g, int v) {
        onStack[v] = true;
        marked[v] = true;
        for (int w : g.adj(v)) {
            if (hasCycle()) {
                return;
            } else if (!marked[w]) {
                edgeTo[w] = v;
                dfs(g, w);
            } else if (onStack[w]) {
                cycle = new Stack<>();
                for (int x = v; x != w; x = edgeTo[x]) {
                    cycle.push(x);
                }
                cycle.push(w);
                cycle.push(v);
            }
        }
        onStack[v] = false;
    }

    /**
     * 返回true如果图含有有向环
     *
     * @return true如果图含有有向环
     */
    @Override
    public boolean hasCycle() {
        return cycle != null;
    }

    /**
     * 返回某一个有向环中所有顶点，如果有
     *
     * @return 某一个有向环中所有顶点, 如果有
     */
    @Override
    public Iterable<Integer> cycle() {
        return cycle;
    }
}
