package algorithms04.graphs.mst;


import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.UF;

/**
 * @author HHH
 */
public class KruskalMinimumSpanningTree implements MinimumSpanningTree {
    private final Queue<Edge> mst;

    public KruskalMinimumSpanningTree(EdgeWeightedGraph g) {
        mst = new Queue<>();
        MinPQ<Edge> pq = new MinPQ<>();
        for (Edge e : g.edges()) {
            pq.insert(e);
        }
        UF uf = new UF(g.v());

        while (!pq.isEmpty() && mst.size() < g.v() - 1) {
            // 从pq得到权重最小的边和它的顶点
            Edge e = pq.delMin();
            int v = e.either();
            int w = e.other(v);
            // 忽略失效的边
            if (uf.find(v) == uf.find(w)) {
                continue;
            }
            // 合并分离变量
            uf.union(v, w);
            // 将边添加到最小生成树中
            mst.enqueue(e);
        }
    }

    /**
     * 返回最小生成树的所有边的集合
     *
     * @return 最小生成树的所有边的集合
     */
    @Override
    public Iterable<Edge> edges() {
        return mst;
    }

    /**
     * 返回最小生成树的权重
     *
     * @return 最小生成树的权重
     */
    @Override
    public double weight() {
        double sum = 0;
        while (!mst.isEmpty()) {
            sum = sum + mst.dequeue().weight();
        }
        return sum;
    }
}
