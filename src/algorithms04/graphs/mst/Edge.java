package algorithms04.graphs.mst;

/**
 * 带权重的边
 *
 * @author HHH
 */
public class Edge implements EdgeBase, Comparable<Edge> {
    /**
     * 顶点之一
     */
    private final int v;
    /**
     * 另一个顶点
     */
    private final int w;
    /**
     * 边的权重
     */
    private final double weight;

    public Edge(int v, int w, double weight) {
        this.v = v;
        this.w = w;
        this.weight = weight;
    }

    /**
     * 返回边的权重
     *
     * @return 边的权重
     */
    @Override
    public double weight() {
        return weight;
    }

    /**
     * 返回边两端顶点之一
     *
     * @return 边两端顶点之一
     */
    @Override
    public int either() {
        return v;
    }

    /**
     * 返回顶点v的另一个顶点
     *
     * @param vertex 顶点
     * @return 顶点v的另一个顶点
     */
    @Override
    public int other(int vertex) {
        if (vertex == v) {
            return w;
        } else if (vertex == w) {
            return v;
        } else {
            throw new RuntimeException("不一致的边");
        }
    }

    @Override
    public int compareTo(Edge o) {
        return Double.compare(weight, o.weight);
    }

    @Override
    public String toString() {
        return String.format("%d-%d %.2f", v, w, weight);
    }
}
