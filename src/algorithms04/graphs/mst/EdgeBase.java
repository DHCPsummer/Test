package algorithms04.graphs.mst;

/**
 * 加权边API
 *
 * @author HHH
 */
public interface EdgeBase {
    /**
     * 返回边的权重
     *
     * @return 边的权重
     */
    double weight();

    /**
     * 返回边两端顶点之一
     *
     * @return 边两端顶点之一
     */
    int either();

    /**
     * 返回顶点v的另一顶点
     *
     * @param v 顶点
     * @return 顶点v的另一顶点
     */
    int other(int v);
}
