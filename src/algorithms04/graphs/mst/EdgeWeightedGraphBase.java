package algorithms04.graphs.mst;

/**
 * 加权无向图API
 *
 * @author HHH
 */
public interface EdgeWeightedGraphBase {
    /**
     * 返回图的顶点总数
     *
     * @return 图所有的顶点数
     */
    int v();

    /**
     * 返回图的边总数
     *
     * @return 图的边总数
     */
    int e();

    /**
     * 向图中添加一条边e
     *
     * @param edge 边
     */
    void addEdge(Edge edge);

    /**
     * 返回和v顶点相连的所有边的集合
     *
     * @param v 顶点
     * @return 和v顶点相连的所有顶点的集合
     */
    Iterable<Edge> adj(int v);

    /**
     * 返回图所有的边
     *
     * @return 回图所有的边
     */
    Iterable<Edge> edges();
}
