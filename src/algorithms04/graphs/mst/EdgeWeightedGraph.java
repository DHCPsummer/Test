package algorithms04.graphs.mst;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.In;

/**
 * 加权无向图
 *
 * @author HHH
 */
public class EdgeWeightedGraph implements EdgeWeightedGraphBase {
    /**
     * 顶点总数
     */
    private final int v;
    /**
     * 边总数
     */
    private int e;
    /**
     * 邻接表
     */
    private final Bag<Edge>[] adj;

    /**
     * 构造一个顶点数量v，边数0的加权无向图
     *
     * @param v 顶点数
     */
    public EdgeWeightedGraph(int v) {
        this.v = v;
        this.e = 0;
        adj = (Bag<Edge>[]) new Bag[v];
        for (int i = 0; i < v; i++) {
            adj[i] = new Bag<>();
        }
    }

    /**
     * 从标准流中读入一个图
     *
     * @param in 标准流url
     */
    public EdgeWeightedGraph(In in) {
        this(in.readInt());
        int e = in.readInt();
        for (int i = 0; i < e; i++) {
            int v = in.readInt();
            int w = in.readInt();
            double weight = in.readDouble();
            Edge edge = new Edge(v,w,weight);
            addEdge(edge);
        }
    }

    /**
     * 返回图的顶点总数
     *
     * @return 图所有的顶点数
     */
    @Override
    public int v() {
        return v;
    }

    /**
     * 返回图的边总数
     *
     * @return 图的边总数
     */
    @Override
    public int e() {
        return e;
    }

    /**
     * 向图中添加一条边e
     *
     * @param edge 边
     */
    @Override
    public void addEdge(Edge edge) {
        int v = edge.either();
        int w = edge.other(v);
        adj[v].add(edge);
        adj[w].add(edge);
        e++;
    }

    /**
     * 返回和v顶点相连的所有边的集合
     *
     * @param v 顶点
     * @return 和v顶点相连的所有顶点的集合
     */
    @Override
    public Iterable<Edge> adj(int v) {
        return adj[v];
    }

    /**
     * 返回图所有的边
     *
     * @return 回图所有的边
     */
    @Override
    public Iterable<Edge> edges() {
        Bag<Edge> b = new Bag<>();
        for (int i = 0; i < v; i++) {
            for (Edge e : adj[i]) {
                if (e.other(i) > i) {
                    b.add(e);
                }
            }
        }
        return b;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(v).append(" ").append(e);
        for (int i = 0; i < v; i++) {
            s.append(i).append(": ");
            for (Edge e : adj[i]) {
                s.append(e).append("  ");
            }
        }
        return s.toString();
    }
}
