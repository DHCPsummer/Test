package algorithms04.graphs.mst;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.IndexMinPQ;

/**
 * 最小生成树Prim算法的及时实现
 *
 * @author HHH
 */
public class PrimMinimumSpanningTree implements MinimumSpanningTree {
    /**
     * 距离树最近的边
     */
    private final Edge[] edgeTo;
    /**
     * 边w的权重
     * distTo[w] = edgeTo[w].weight();
     */
    private final double[] distTo;
    /**
     * 如果v在树中则为true
     */
    private final boolean[] marked;
    /**
     * 有效地横切边
     */
    private final IndexMinPQ<Double> pq;
    /**
     * 最小生成树的权重
     */
    private double weightSum;

    public PrimMinimumSpanningTree(EdgeWeightedGraph g) {
        edgeTo = new Edge[g.v()];
        marked = new boolean[g.v()];
        distTo = new double[g.v()];
        for (int v = 0; v < g.v(); v++) {
            distTo[v] = Double.POSITIVE_INFINITY;
        }
        pq = new IndexMinPQ<>(g.v());
        // 用顶点0和权重0初始化pq
        distTo[0] = 0.0;
        pq.insert(0, 0.0);
        while (!pq.isEmpty()) {
            // 将最近的顶点添加到树中
            int v = pq.delMin();
            weightSum += distTo[v];
            visit(g, v);
        }
    }

    /**
     * 将顶点v添加到树中,更新数据
     */
    private void visit(EdgeWeightedGraph g, int v) {
        marked[v] = true;
        for (Edge e : g.adj(v)) {
            int w = e.other(v);
            // v-w失效
            if (marked[w]) {
                continue;
            }
            // 连接w和树的最佳边Edge变为e
            if (e.weight() < distTo[w]) {
                edgeTo[w] = e;
                distTo[w] = e.weight();
                if (pq.contains(w)) {
                    pq.changeKey(w, distTo[w]);
                } else {
                    pq.insert(w, distTo[w]);
                }
            }
        }
    }

    /**
     * 返回最小生成树的所有边的集合
     *
     * @return 最小生成树的所有边的集合
     */
    @Override
    public Iterable<Edge> edges() {
        Bag<Edge> mst = new Bag<>();
        for (int v = 1; v < edgeTo.length; v++) {
            mst.add(edgeTo[v]);
        }
        return mst;
    }

    /**
     * 返回最小生成树的权重
     *
     * @return 最小生成树的权重
     */
    @Override
    public double weight() {
        return weightSum;
    }
}
