package algorithms04.graphs.mst;

/**
 * 最小生成树API
 *
 * @author HHH
 */
public interface MinimumSpanningTree {
    /**
     * 返回最小生成树的所有边的集合
     * @return 最小生成树的所有边的集合
     */
    Iterable<Edge> edges();

    /**
     * 返回最小生成树的权重
     * @return 最小生成树的权重
     */
    double weight();
}
