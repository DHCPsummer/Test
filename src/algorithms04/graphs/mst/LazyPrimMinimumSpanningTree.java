package algorithms04.graphs.mst;

import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Queue;

/**
 * 最小生成树Prim算法的延时实现
 *
 * @author HHH
 */
public class LazyPrimMinimumSpanningTree implements MinimumSpanningTree {
    /**
     * 最小生成树的顶点
     */
    private final boolean[] marked;
    /**
     * 最小生成树的边
     */
    private final Queue<Edge> mst;
    /**
     * 横切边(包括已经失效的边)
     */
    private final MinPQ<Edge> pq;

    public LazyPrimMinimumSpanningTree(EdgeWeightedGraph g) {
        pq = new MinPQ<>();
        marked = new boolean[g.v()];
        mst = new Queue<>();

        visit(g, 0);
        while (!pq.isEmpty()) {
            // 从pq中得到权重最小的边
            Edge e = pq.delMin();

            int v = e.either();
            int w = e.other(v);
            // 跳过失效的边
            if (marked[v] && marked[w]) {
                continue;
            }
            // 把边添加到树中
            mst.enqueue(e);
            // 将顶点(v或w)添加到树中
            if (!marked[v]) {
                visit(g, v);
            }
            if (!marked[w]) {
                visit(g, w);
            }
        }
    }

    /**
     * 标记顶点v并将所有连接v和未被标记的顶点的边加入pq
     *
     * @param g 加权无向图
     * @param v 顶点
     */
    private void visit(EdgeWeightedGraph g, int v) {
        marked[v] = true;
        for (Edge e : g.adj(v)) {
            if (!marked[e.other(v)]) {
                pq.insert(e);
            }
        }
    }

    /**
     * 返回最小生成树的所有边的集合
     *
     * @return 最小生成树的所有边的集合
     */
    @Override
    public Iterable<Edge> edges() {
        return mst;
    }

    /**
     * 返回最小生成树的权重
     *
     * @return 最小生成树的权重
     */
    @Override
    public double weight() {
        double sum = 0;
        while (!mst.isEmpty()) {
            sum = sum + mst.dequeue().weight();
        }
        return sum;
    }
}
