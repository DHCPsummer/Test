package algorithms04.graphs.msp;

import edu.princeton.cs.algs4.IndexMinPQ;
import edu.princeton.cs.algs4.Stack;

/**
 * 有向图最短路径Dijkstra算法
 * 权重只能是正数,可以是有环图
 *
 * @author HHH
 */
public class DijkstraSpanningPaths implements SpanningPathsBase {
    /**
     * 距离树最近的边
     */
    private final DirectedEdge[] edgeTo;
    /**
     * 边w的权重
     * distTo[w] = edgeTo[w].weight();
     */
    private final double[] distTo;
    /**
     * 有效的横切边
     */
    private final IndexMinPQ<Double> pq;

    public DijkstraSpanningPaths(EdgeWeightedDigraph g, int s) {
        edgeTo = new DirectedEdge[g.v()];
        distTo = new double[g.v()];
        pq = new IndexMinPQ<>(g.v());
        for (int v = 0; v < g.v(); v++) {
            distTo[v] = Double.POSITIVE_INFINITY;
        }
        distTo[s] = 0.0;
        pq.insert(s, 0.0);
        while (!pq.isEmpty()) {
            relax(g, pq.delMin());
        }
    }

    /**
     * 顶点松弛
     */
    private void relax(EdgeWeightedDigraph g, int v) {
        for (DirectedEdge e : g.adj(v)) {
            int w = e.to();
            if (distTo[w] > distTo[v] + e.weight()) {
                distTo[w] = distTo[v] + e.weight();
                edgeTo[w] = e;
                if (pq.contains(w)) {
                    // 顶点在队列中,但优先级需要降低
                    pq.changeKey(w, distTo[w]);
                } else {
                    // 顶点不在队列中
                    pq.insert(w, distTo[w]);
                }
            }
        }
    }

    /**
     * 返回从起点s到顶点v的最短距离，如果不存在返回无穷大
     *
     * @param v 顶点
     * @return 从起点s到顶点v的最短距离，如果不存在返回无穷大
     */
    @Override
    public double distTo(int v) {
        return distTo[v];
    }

    /**
     * 返回true如果存在顶点s到顶点v的路径
     *
     * @param v 顶点
     * @return true如果存在顶点s到顶点v的路径
     */
    @Override
    public boolean hasPathTo(int v) {
        return distTo[v] < Double.POSITIVE_INFINITY;
    }

    /**
     * 返回从起点s到顶点v的路径集合,否则null
     *
     * @param v 顶点
     * @return 从起点s到顶点v的路径集合, 否则null
     */
    @Override
    public Iterable<DirectedEdge> pathTo(int v) {
        if (!hasPathTo(v)) {
            return null;
        }
        Stack<DirectedEdge> path = new Stack<>();
        for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.from()]) {
            path.push(e);
        }
        return path;
    }
}
