package algorithms04.graphs.msp;

import edu.princeton.cs.algs4.Stack;

/**
 * 用于确定边加权有向图是否具有有向环的数据类型
 * ctrl+c、 ctrl+v
 *
 * @author HHH
 */
public class EdgeWeightedDirectedCycle {
    private final boolean[] marked;
    private final DirectedEdge[] edgeTo;
    private final boolean[] onStack;
    private Stack<DirectedEdge> cycle;

    public EdgeWeightedDirectedCycle(EdgeWeightedDigraph g) {
        marked = new boolean[g.v()];
        onStack = new boolean[g.v()];
        edgeTo = new DirectedEdge[g.v()];
        for (int v = 0; v < g.v(); v++) {
            if (!marked[v]) {
                dfs(g, v);
            }
        }
    }

    private void dfs(EdgeWeightedDigraph g, int v) {
        onStack[v] = true;
        marked[v] = true;
        for (DirectedEdge e : g.adj(v)) {
            int w = e.to();
            // 有向循环时发生短路
            if (cycle != null) {
                return;
            }
            // 找到了新顶点，所以递归
            else if (!marked[w]) {
                edgeTo[w] = e;
                dfs(g, w);
            }
            // 反向有向循环
            else if (onStack[w]) {
                cycle = new Stack<>();
                DirectedEdge f = e;
                while (f.from() != w) {
                    cycle.push(f);
                    f = edgeTo[f.from()];
                }
                cycle.push(f);
                return;
            }
        }
        onStack[v] = false;
    }

    public boolean hasCycle() {
        return cycle != null;
    }

    public Iterable<DirectedEdge> cycle() {
        return cycle;
    }
}
