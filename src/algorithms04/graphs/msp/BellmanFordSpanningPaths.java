package algorithms04.graphs.msp;

import algorithms04.Queue;
import edu.princeton.cs.algs4.Stack;

/**
 * 使用基于队列的Bellman-Ford算法找出加权有向图中的最短路径，权重可以是负的且可以含有环，但不能含有负权重环，
 *
 * @author HHH
 */
public class BellmanFordSpanningPaths implements NegativeWeightCycle, SpanningPathsBase {
    /**
     * 从起点到某个顶点的路径长度
     */
    private final double[] distTo;
    /**
     * 从起点到某个顶点的最后一条路径
     */
    private final DirectedEdge[] edgeTo;
    /**
     * 该顶点是否在队列中
     */
    private final boolean[] onQ;
    /**
     * 正在被放松的顶点
     */
    private final Queue<Integer> queue;
    /**
     * relax()的调用次数
     */
    private int cost;
    /**
     * edgeTo[]中的负权重环
     */
    private Iterable<DirectedEdge> cycle;

    public BellmanFordSpanningPaths(EdgeWeightedDigraph g, int s) {
        distTo = new double[g.v()];
        edgeTo = new DirectedEdge[g.v()];
        onQ = new boolean[g.v()];
        queue = new Queue<>();
        for (int v = 0; v < g.v(); v++) {
            distTo[v] = Double.POSITIVE_INFINITY;
        }
        distTo[s] = 0.0;
        queue.enqueue(s);
        onQ[s] = true;
        while (!queue.isEmpty() && !hasNegativeCycle()) {
            int v = queue.dequeue();
            onQ[v] = false;
            relax(g, v);
        }
    }

    /**
     * 判断该有向图是否含有负权重环
     * @param g 图
     */
    public BellmanFordSpanningPaths(EdgeWeightedDigraph g) {
        distTo = new double[g.v()];
        edgeTo = new DirectedEdge[g.v()];
        onQ = new boolean[g.v()];
        queue = new Queue<>();
        for (int v = 0; v < g.v(); v++) {
            distTo[v] = 0;
        }
    }

    /**
     * 顶点松弛
     */
    private void relax(EdgeWeightedDigraph g, int v) {
        for (DirectedEdge e : g.adj(v)) {
            int w = e.to();
            if (distTo[w] > distTo[v] + e.weight()) {
                distTo[w] = distTo[v] + e.weight();
                edgeTo[w] = e;
                if (!onQ[w]) {
                    queue.enqueue(w);
                    onQ[w] = true;
                }
            }
            if (cost++ % g.v() == 0) {
                findNegativeCycle();
            }
        }
    }

    /**
     * 寻找途中的有向环
     */
    private void findNegativeCycle() {
        int v = edgeTo.length;
        EdgeWeightedDigraph spt = new EdgeWeightedDigraph(v);
        for (DirectedEdge directedEdge : edgeTo) {
            if (directedEdge != null) {
                spt.addEdge(directedEdge);
            }
        }
        EdgeWeightedDirectedCycle cf = new EdgeWeightedDirectedCycle(spt);
        cycle = cf.cycle();
    }

    /**
     * 返回true，如果含有负权重环
     *
     * @return true，如果含有负权重环
     */
    @Override
    public boolean hasNegativeCycle() {
        return cycle != null;
    }

    /**
     * 返回负权重环，如果有，否则null
     *
     * @return 负权重环，如果有，否则null
     */
    @Override
    public Iterable<DirectedEdge> negativeCycle() {
        return cycle;
    }

    /**
     * 返回从起点s到顶点v的最短距离，如果不存在返回无穷大
     *
     * @param v 顶点
     * @return 从起点s到顶点v的最短距离，如果不存在返回无穷大
     */
    @Override
    public double distTo(int v) {
        return distTo[v];
    }

    /**
     * 返回true如果存在顶点s到顶点v的路径
     *
     * @param v 顶点
     * @return true如果存在顶点s到顶点v的路径
     */
    @Override
    public boolean hasPathTo(int v) {
        return distTo[v] < Double.POSITIVE_INFINITY;
    }

    /**
     * 返回从起点s到顶点v的路径集合,否则null
     *
     * @param v 顶点
     * @return 从起点s到顶点v的路径集合, 否则null
     */
    @Override
    public Iterable<DirectedEdge> pathTo(int v) {
        if (!hasPathTo(v)) {
            return null;
        }
        Stack<DirectedEdge> path = new Stack<>();
        for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.from()]) {
            path.push(e);
        }
        return path;
    }
}