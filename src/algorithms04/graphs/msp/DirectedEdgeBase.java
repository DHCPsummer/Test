package algorithms04.graphs.msp;

/**
 * 加权有向边API
 *
 * @author HHH
 */
public interface DirectedEdgeBase {
    /**
     * 返回边的权重
     *
     * @return 边的权重
     */
    double weight();

    /**
     * 返回指出这条边的顶点
     *
     * @return 指出这条边的顶点
     */
    int from();

    /**
     * 返回这条边指向的顶点
     * @return 这条边指向的顶点
     */
    int to();
}
