package algorithms04.graphs.msp;

/**
 * 基于DSP，任意顶点最短路径
 *
 * @author HHH
 */
public class DijkstraAllPairsSpanningPaths {
    private final DijkstraSpanningPaths[] all;

    public DijkstraAllPairsSpanningPaths(EdgeWeightedDigraph g) {
        all = new DijkstraSpanningPaths[g.v()];
        for (int v = 0; v < g.v(); v++) {
            all[v] = new DijkstraSpanningPaths(g, v);
        }
    }

    /**
     * 返回顶点s到顶点t的所有最短路径的集合,没有返回null
     *
     * @param s 顶点
     * @param t 顶点
     * @return 顶点s到顶点t的所有最短路径的集合, 没有返回null
     */
    Iterable<DirectedEdge> path(int s, int t) {
        return all[s].pathTo(t);
    }

    /**
     * 返回从顶点s到顶点v的最短距离，如果不存在返回无穷大
     *
     * @param s 顶点
     * @param t 顶点
     * @return 从顶点s到顶点v的最短距离，如果不存在返回无穷大
     */
    double dist(int s, int t) {
        return all[s].distTo(t);
    }
}
