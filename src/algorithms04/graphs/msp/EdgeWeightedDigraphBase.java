package algorithms04.graphs.msp;

/**
 * 加权有向图API
 *
 * @author HHH
 */
public interface EdgeWeightedDigraphBase {
    /**
     * 返回顶点总数
     * @return 顶点总数
     */
    int v();
    /**
     * 返回边总数
     * @return 边总数
     */
    int e();

    /**
     * 将边e添加到该有向图中
     * @param e 边
     */
    void addEdge(DirectedEdge e);

    /**
     * 返回从顶点v指出的所有的边的集合
     * @param v 顶点
     * @return 从顶点v指出的所有的边的集合
     */
    Iterable<DirectedEdge> adj(int v);

    /**
     * 返回该图所有边的集合
     * @return 该图所有边的集合
     */
    Iterable<DirectedEdge> edges();
}
