package algorithms04.graphs.msp;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.In;

/**
 * 加权有向图
 *
 * @author HHH
 */
public class EdgeWeightedDigraph implements EdgeWeightedDigraphBase {
    /**
     * 顶点总数
     */
    private final int v;
    /**
     * 邻接表
     */
    private final Bag<DirectedEdge>[] adj;
    /**
     * 边总数
     */
    private int e;

    /**
     * 构造一个顶点数量v，边数0的加权无向图
     *
     * @param v 顶点数
     */
    public EdgeWeightedDigraph(int v) {
        this.v = v;
        this.e = 0;
        adj = (Bag<DirectedEdge>[]) new Bag[v];
        for (int i = 0; i < v; i++) {
            adj[i] = new Bag<>();
        }
    }

    /**
     * 从标准流中读入一个图
     *
     * @param in 标准流url
     */
    public EdgeWeightedDigraph(In in) {
        this(in.readInt());
        int e = in.readInt();
        for (int i = 0; i < e; i++) {
            int v = in.readInt();
            int w = in.readInt();
            double weight = in.readDouble();
            DirectedEdge edge = new DirectedEdge(v, w, weight);
            addEdge(edge);
        }
    }

    /**
     * 返回图的顶点总数
     *
     * @return 图所有的顶点数
     */
    @Override
    public int v() {
        return v;
    }

    /**
     * 返回图的边总数
     *
     * @return 图的边总数
     */
    @Override
    public int e() {
        return e;
    }

    /**
     * 向图中添加一条边e
     *
     * @param edge 边
     */
    @Override
    public void addEdge(DirectedEdge edge) {
        adj[edge.from()].add(edge);
        e++;
    }

    /**
     * 返回和v顶点相连的所有边的集合
     *
     * @param v 顶点
     * @return 和v顶点相连的所有顶点的集合
     */
    @Override
    public Iterable<DirectedEdge> adj(int v) {
        return adj[v];
    }

    /**
     * 返回图所有的边
     *
     * @return 回图所有的边
     */
    @Override
    public Iterable<DirectedEdge> edges() {
        Bag<DirectedEdge> b = new Bag<>();
        for (int i = 0; i < v; i++) {
            for (DirectedEdge e : adj[i]) {
                b.add(e);
            }
        }
        return b;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(v).append("个顶点\n").append(e).append("条边\n");
        for (int i = 0; i < v; i++) {
            s.append(i).append(": ");
            for (DirectedEdge e : adj[i]) {
                s.append(e).append("  ");
            }
            s.append("\n");
        }
        return s.toString();
    }
}
