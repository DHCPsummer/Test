package algorithms04.graphs.msp;

/**
 * 加权有向边
 *
 * @author HHH
 */
public class DirectedEdge implements DirectedEdgeBase {
    /**
     * 边的起点
     */
    private final int v;
    /**
     * 边的终点
     */
    private final int w;
    /**
     * 边的权重
     */
    private final double weight;

    public DirectedEdge(int v, int w, double weight) {
        this.v = v;
        this.w = w;
        this.weight = weight;
    }


    /**
     * 返回边的权重
     *
     * @return 边的权重
     */
    @Override
    public double weight() {
        return weight;
    }

    /**
     * 返回指出这条边的顶点
     *
     * @return 指出这条边的顶点
     */
    @Override
    public int from() {
        return v;
    }

    /**
     * 返回这条边指向的顶点
     *
     * @return 这条边指向的顶点
     */
    @Override
    public int to() {
        return w;
    }

    @Override
    public String toString() {
        return String.format("%d->%d %.2f", v, w, weight);
    }
}
