package algorithms04.graphs.msp;

import edu.princeton.cs.algs4.In;

/**
 * @author HHH
 */
public class Test {
    public static void main(String[] args) {
        In in = new In("D:\\IDEA\\workspace\\Test\\src\\algorithms04\\graphs\\msp\\tinyEWDn.txt");
        EdgeWeightedDigraph digraph = new EdgeWeightedDigraph(in);

        SpanningPathsBase paths = new BellmanFordSpanningPaths(digraph, 0);
        for (int t = 0; t < digraph.v(); t++) {
            System.out.print(0 + " to " + t);
            System.out.printf(" (%4.2f) ", paths.distTo(t));
            if (paths.hasPathTo(t)) {
                for (DirectedEdge edge : paths.pathTo(t)) {
                    System.out.print(edge + " ");
                }
                System.out.println();
            }
        }
    }
}
