package algorithms04.graphs.msp;

import algorithms04.graphs.digraph.Topological;
import edu.princeton.cs.algs4.Stack;

/**
 * 无环加权有向图的最短路径算法
 *
 * @author HHH
 */
public class AcyclicPairsSpanningPaths implements SpanningPathsBase {
    private final DirectedEdge[] edgeTo;
    private final double[] distTo;

    public AcyclicPairsSpanningPaths(EdgeWeightedDigraph g, int s) {
        edgeTo = new DirectedEdge[g.v()];
        distTo = new double[g.v()];
        for (int v = 0; v < g.v(); v++) {
            distTo[v] = Double.POSITIVE_INFINITY;
        }
        distTo[s] = 0.0;
        Topological top = new Topological(g);
        for (int v : top.order()) {
            relax(g, v);
        }
    }

    /**
     * 顶点松弛
     */
    private void relax(EdgeWeightedDigraph g, int v) {
        for (DirectedEdge e : g.adj(v)) {
            int w = e.to();
            if (distTo[w] > distTo[v] + e.weight()) {
                distTo[w] = distTo[v] + e.weight();
                edgeTo[w] = e;
            }
        }
    }

    /**
     * 返回从起点s到顶点v的最短距离，如果不存在返回无穷大
     *
     * @param v 顶点
     * @return 从起点s到顶点v的最短距离，如果不存在返回无穷大
     */
    @Override
    public double distTo(int v) {
        return distTo[v];
    }

    /**
     * 返回true如果存在顶点s到顶点v的路径
     *
     * @param v 顶点
     * @return true如果存在顶点s到顶点v的路径
     */
    @Override
    public boolean hasPathTo(int v) {
        return distTo[v] < Double.POSITIVE_INFINITY;
    }

    /**
     * 返回从起点s到顶点v的路径集合,否则null
     *
     * @param v 顶点
     * @return 从起点s到顶点v的路径集合, 否则null
     */
    @Override
    public Iterable<DirectedEdge> pathTo(int v) {
        if (!hasPathTo(v)) {
            return null;
        }
        Stack<DirectedEdge> path = new Stack<>();
        for (DirectedEdge e = edgeTo[v]; e != null; e = edgeTo[e.from()]) {
            path.push(e);
        }
        return path;
    }
}
