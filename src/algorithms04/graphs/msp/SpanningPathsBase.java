package algorithms04.graphs.msp;

/**
 * 最短路径API（SP）
 *
 * @author HHH
 */
public interface SpanningPathsBase {
    /**
     * 返回从起点s到顶点v的最短距离，如果不存在返回无穷大
     *
     * @param v 顶点
     * @return 从起点s到顶点v的最短距离，如果不存在返回无穷大
     */
    double distTo(int v);

    /**
     * 返回true如果存在顶点s到顶点v的路径
     *
     * @param v 顶点
     * @return true如果存在顶点s到顶点v的路径
     */
    boolean hasPathTo(int v);

    /**
     * 返回从起点s到顶点v的路径集合,否则null
     *
     * @param v 顶点
     * @return 从起点s到顶点v的路径集合, 否则null
     */
    Iterable<DirectedEdge> pathTo(int v);
}
