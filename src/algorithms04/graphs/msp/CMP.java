package algorithms04.graphs.msp;

import edu.princeton.cs.algs4.In;

import java.util.ArrayList;

/**
 * 优先级限制下的并行任务调度问题的关键路径方法
 *
 * @author HHH
 */
public class CMP {
    public static void main(String[] args) {
        In in = new In("D:\\IDEA\\workspace\\Test\\src\\algorithms04\\graphs\\msp\\jobsPC.txt");
        int n = in.readInt();
        EdgeWeightedDigraph g = new EdgeWeightedDigraph(2 * n + 2);
        int s = 2 * n;
        int t = 2 * n + 1;
        ArrayList<String[]> strings = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            in = new In("D:\\IDEA\\workspace\\Test\\src\\algorithms04\\graphs\\msp\\jobsPC.txt");
            int count = 0;
            while (in.hasNextLine()) {
                // 跳过第一个数据(顶点个数)
                if (count == 0) {
                    count++;
                    continue;
                }
                String[] a = in.readLine().split(" ");
                strings.add(a);
            }
            // 跳过第一个数据(顶点个数)
            String[] b = strings.get(i + 1);
            double duration = Double.parseDouble(b[0]);
            g.addEdge(new DirectedEdge(i, i + n, duration));
            g.addEdge(new DirectedEdge(s, i, 0.0));
            g.addEdge(new DirectedEdge(i + n, t, 0.0));
            for (int j = 1; j < b.length; j++) {
                int successor = Integer.parseInt(b[j]);
                g.addEdge(new DirectedEdge(i + n, successor, 0.0));
            }
        }
        AcyclicPairsLongPaths lp = new AcyclicPairsLongPaths(g, s);

        // 打印
        System.out.println("Start times:");
        for (int i = 0; i < n; i++) {
            System.out.printf("%4d: %5.1f\n", i, lp.distTo(i));
        }
        System.out.printf("Finish time: %5.1f\n", lp.distTo(t));
    }
}
