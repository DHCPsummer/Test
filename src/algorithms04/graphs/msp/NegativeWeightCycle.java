package algorithms04.graphs.msp;

/**
 * 寻找负权重环API
 *
 * @author HHH
 */
public interface NegativeWeightCycle {
    /**
     * 返回true，如果含有负权重环
     * @return true，如果含有负权重环
     */
    boolean hasNegativeCycle();

    /**
     * 返回负权重环，如果有，否则null
     * @return 负权重环，如果有，否则null
     */
    Iterable<DirectedEdge> negativeCycle();
}
