package algorithms04;

/**
 * 栈API
 *
 * @author HHH
 */
public interface StackBase<Item> extends Iterable<Item> {
    /**
     * 返回true,如果栈为空,否则false
     *
     * @return true, 如果栈为空, 否则false
     */
    boolean isEmpty();

    /**
     * 返回栈中含有的元素个数
     *
     * @return 栈中含有的元素个数
     */
    int size();

    /**
     * 把元素压入栈顶
     *
     * @param item 要添加的元素
     */
    void push(Item item);

    /**
     * 返回栈顶元素,并删除它
     *
     * @return 栈顶元素, 并删除它
     */
    Item pop();
}
