package algorithms04;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 链表实现的队列
 * @author HHH
 */
public class Queue<Item> implements QueueBase<Item>{
    /**
     * 指向最早添加的结点的链接
     */
    private Node first;
    /**
     * 指向最近添加的结点的链接
     */
    private Node last;
    /**
     * 队列中元素的数量
     */
    private int n;

    /**
     * 返回true,如果队列为空,否则false
     *
     * @return true, 如果队列为空, 否则false
     */
    @Override
    public boolean isEmpty() {
        return first == null;
    }

    /**
     * 返回队列中含有的元素个数
     *
     * @return 队列中含有的元素个数
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 向队列中加入元素item(从表尾加)
     *
     * @param item 要加入的元素
     */
    @Override
    public void enqueue(Item item) {
        Node oldLast = last;
        last = new Node();
        last.item = item;
        last.next = null;
        if (isEmpty()) {
            first = last;
        } else {
            oldLast.next = last;
        }
        n++;
    }

    /**
     * 返回表头第一个元素,并将它删除
     *
     * @return 表头第一个元素, 并将它删除
     */
    @Override
    public Item dequeue() {
        Item item = first.item;
        first = first.next;
        if (isEmpty()) {
            last = null;
        }
        n--;
        return item;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<Item> iterator() {
        return new LinkedIterator();
    }

    private class LinkedIterator implements Iterator<Item> {
        private Node current = first;

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return current != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public Item next() {
            Item item = current.item;
            current = current.next;
            return item;
        }
    }

    /**
     * 结点,使用私有内部类实现
     */
    private class Node {
        private Item item;
        private Node next;
    }
}
