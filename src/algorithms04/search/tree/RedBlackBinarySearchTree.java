package algorithms04.search.tree;

import algorithms04.search.OrderSt;
import edu.princeton.cs.algs4.Queue;

/**
 * 红黑二叉树
 *
 * @author HHH
 */
public class RedBlackBinarySearchTree<Key extends Comparable<Key>, Value> implements OrderSt<Key, Value> {
    private static final boolean RED = true;
    private static final boolean BLACK = false;
    private Node root;

    /**
     * 返回最小的键
     *
     * @return 最小的键
     */
    @Override
    public Key min() {
        return min(root).key;
    }

    private Node min(Node x) {
        if (x.left == null) {
            return x;
        }
        return min(x.left);
    }

    /**
     * 返回最大的键
     *
     * @return 最大的键
     */
    @Override
    public Key max() {
        return max(root).key;
    }

    private Node max(Node x) {
        if (x.right == null) {
            return x;
        }
        return max(x.right);
    }

    /**
     * 返回小于等于键key的最大键
     *
     * @param key 键
     * @return 小于等于key的最大键
     */
    @Override
    public Key floor(Key key) {
        Node x = floor(root, key);
        if (x == null) {
            return null;
        }
        return x.key;
    }

    private Node floor(Node x, Key key) {
        if (x == null) {
            return null;
        }
        int cmp = key.compareTo(x.key);
        if (cmp == 0) {
            return x;
        }
        if (cmp < 0) {
            return floor(x.left, key);
        }
        Node t = floor(x.right, key);
        if (t != null) {
            return t;
        } else {
            return x;
        }
    }

    /**
     * 返回大于等于键key的最小键
     *
     * @param key 键
     * @return 大于等于键key的最小键
     */
    @Override
    public Key ceiling(Key key) {
        Node x = ceiling(root, key);
        if (x == null) {
            return null;
        }
        return x.key;
    }

    private Node ceiling(Node x, Key key) {
        if (x == null) {
            return null;
        }
        int cmp = key.compareTo(x.key);
        if (cmp == 0) {
            return x;
        }
        if (cmp > 0) {
            return ceiling(x.right, key);
        }
        Node t = ceiling(x.left, key);
        if (t != null) {
            return t;
        } else {
            return x;
        }
    }

    /**
     * 返回小于键key的键的数量
     *
     * @param key 键
     * @return 小于键key的键的数量
     */
    @Override
    public int rank(Key key) {
        return rank(root, key);
    }

    private int rank(Node x, Key key) {
        if (x == null) {
            return 0;
        }
        int cmp = key.compareTo(x.key);
        if (cmp < 0) {
            return rank(x.left, key);
        } else if (cmp > 0) {
            return rank(x.right, key) + 1 + size(x.left);
        } else {
            return size(x.left);
        }
    }

    /**
     * 返回排名(索引)为k的键
     *
     * @param k 排名(索引)
     * @return 排名(索引)为k的键
     */
    @Override
    public Key select(int k) {
        return select(root, k).key;
    }

    private Node select(Node x, int k) {
        if (x == null) {
            return null;
        }
        int t = size(x.left);
        if (t > k) {
            return select(x.left, k);
        } else if (t < k) {
            return select(x.right, k - t - 1);
        } else {
            return x;
        }
    }

    /**
     * 删除最小的键
     */
    @Override
    public void deleteMin() {
        if (!isRed(root.left) && !isRed(root.right)) {
            root.color = RED;
        }
        root = deleteMin(root);
        if (!isEmpty()) {
            root.color = BLACK;
        }
    }

    private Node deleteMin(Node h) {
        if (h.left == null) {
            return null;
        }
        if (!isRed(h.left) && !isRed(h.left.left)) {
            h = moveRedLeft(h);
        }
        h.left = deleteMin(h.left);
        return balance(h);
    }

    private Node moveRedLeft(Node h) {
        // 假设结点h为红色,h.left和h.left.left都是黑色
        // 将h.left或者h.left的子节点之一变红
        flipColors1(h);
        if (isRed(h.right.left)) {
            h.right = rotateRight(h.right);
            h = rotateLeft(h);
        }
        return h;
    }

    /**
     * 删除最大的键
     */
    @Override
    public void deleteMax() {
        if (!isRed(root.left) && !isRed(root.right)) {
            root.color = RED;
        }
        root = deleteMax(root);
        if (!isEmpty()) {
            root.color = BLACK;
        }
    }

    private Node deleteMax(Node h) {
        if (isRed(h.left)) {
            h = rotateRight(h);
        }
        if (h.right == null) {
            return null;
        }
        if (!isRed(h.right) && !isRed(h.right.left)) {
            h = moveRedRight(h);
        }
        h.right = deleteMax(h.right);
        return balance(h);
    }

    private Node moveRedRight(Node h) {
        flipColors1(h);
        if (isRed(h.left.left)) {
            h = rotateRight(h);
        }
        return h;
    }

    private Node balance(Node h) {
        if (isRed(h.right)) {
            h = rotateLeft(h);
        }

        // 结点h的右结点是红色，左结点是黑色，进行左旋转
        if (isRed(h.right) && !isRed(h.left)) {
            h = rotateLeft(h);
        }
        // 结点h的右结点和它的右节点的右节点都是红色，进行右旋转
        if (isRed(h.left) && isRed(h.left.left)) {
            h = rotateRight(h);
        }
        // 结点h的右节点和左节点均为红色时进行颜色转换
        if (isRed(h.left) && isRed(h.right)) {
            flipColors1(h);
        }

        // 更新结点数
        h.n = 1 + size(h.left) + size(h.right);
        return h;
    }

    private void flipColors1(Node h) {
        h.color = BLACK;
        h.left.color = RED;
        h.right.color = RED;
    }

    /**
     * 返回[lo...hi]之间键的数量
     *
     * @param lo 左边
     * @param hi 右边
     * @return [lo...hi]之间键的数量
     */
    @Override
    public int size(Key lo, Key hi) {
        int left = rank(lo);
        int right = rank(hi);
        if (right - left < 0) {
            return 0;
        } else if (contains(hi)) {
            return right - left + 1;
        } else {
            return right - left;
        }
    }

    /**
     * 返回[lo...hi]之间键所有的键，已排序
     *
     * @param lo 左边
     * @param hi 右边
     * @return [lo...hi]之间键所有的键
     */
    @Override
    public Iterable<Key> keys(Key lo, Key hi) {
        Queue<Key> queue = new Queue<>();
        keys(root, queue, lo, hi);
        return queue;
    }

    /**
     * 将键值对存入表中(若值为null则将键key从表中删除)
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void put(Key key, Value value) {
        root = put(root, key, value);
        root.color = BLACK;
    }

    private Node put(Node h, Key key, Value value) {
        // 标准插入操作
        if (h == null) {
            return new Node(key, value, 1, RED);
        }
        int cmp = key.compareTo(h.key);
        if (cmp < 0) {
            h.left = put(h.left, key, value);
        } else if (cmp > 0) {
            h.right = put(h.right, key, value);
        } else {
            h.value = value;
        }

        // 结点h的右结点是红色，左结点是黑色，进行左旋转
        if (isRed(h.right) && !isRed(h.left)) {
            h = rotateLeft(h);
        }
        // 结点h的右结点和它的右节点的右节点都是红色，进行右旋转
        if (isRed(h.left) && isRed(h.left.left)) {
            h = rotateRight(h);
        }
        // 结点h的右节点和左节点均为红色时进行颜色转换
        if (isRed(h.left) && isRed(h.right)) {
            flipColors(h);
        }

        // 更新结点数
        h.n = 1 + size(h.left) + size(h.right);
        return h;
    }

    /**
     * 返回键key对应的值,如果键不存在则返回null
     *
     * @param key 键
     * @return 键key对应的值, 如果键不存在则返回null
     */
    @Override
    public Value get(Key key) {
        return get(root, key);
    }

    private Value get(Node x, Key key) {
        if (x == null) {
            return null;
        }
        int cmp = key.compareTo(x.key);
        if (cmp < 0) {
            return get(x.left, key);
        } else if (cmp > 0) {
            return get(x.right, key);
        } else {
            return x.value;
        }
    }

    /**
     * 从表中删除键key及对应的值
     *
     * @param key 键
     */
    @Override
    public void delete(Key key) {
        if (!isRed(root.left) && !isRed(root.right)) {
            root.color = RED;
        }
        root = delete(root, key);
        if (!isEmpty()) {
            root.color = BLACK;
        }
    }

    private Node delete(Node h, Key key) {
        if (key.compareTo(h.key) < 0) {
            if (!isRed(h.left) && !isRed(h.left.left)) {
                h = moveRedLeft(h);
            }
            h.left = delete(h.left, key);
        } else {
            if (isRed(h.left)) {
                h = rotateRight(h);
            }
            if (key.compareTo(h.key) == 0 && (h.right == null)) {
                return null;
            }
            if (!isRed(h.right) && !isRed(h.right.left)) {
                h = moveRedRight(h);
            }
            if (key.compareTo(h.key) == 0) {
                h.value = get(h.right, min(h.right).key);
                h.key = min(h.right).key;
                h.right = deleteMin(h.right);
            } else {
                h.right = delete(h.right, key);
            }
        }
        return balance(h);
    }

    /**
     * 返回true如果键存在,否则false
     *
     * @param key 键
     * @return true如果键存在, 否则false
     */
    @Override
    public boolean contains(Key key) {
        return get(key) != null;
    }

    /**
     * 返回true如果表不为空,否则false
     *
     * @return true如果表不为空, 否则false
     */
    @Override
    public boolean isEmpty() {
        return size() != 0;
    }

    /**
     * 返回表键值对的数量
     *
     * @return 表键值对的数量
     */
    @Override
    public int size() {
        return size(root);
    }

    private int size(Node x) {
        if (x == null) {
            return 0;
        } else {
            return x.n;
        }
    }

    /**
     * 返回表中所有键的集合
     *
     * @return 表中所有键的集合
     */
    @Override
    public Iterable<Key> keys() {
        return keys(min(), max());
    }

    private void keys(Node x, Queue<Key> queue, Key lo, Key hi) {
        if (x == null) {
            return;
        }
        int cmpLo = lo.compareTo(x.key);
        int cmpHi = hi.compareTo(x.key);
        if (cmpLo < 0) {
            keys(x.left, queue, lo, hi);
        }
        if (cmpLo <= 0 && cmpHi >= 0) {
            queue.enqueue(x.key);
        }
        if (cmpHi > 0) {
            keys(x.right, queue, lo, hi);
        }
    }

    /**
     * 左旋转结点h的右链接
     *
     * @param h 结点
     */
    private Node rotateLeft(Node h) {
        Node x = h.right;
        h.right = x.left;
        x.left = h;
        x.color = h.color;
        h.color = RED;
        x.n = h.n;
        h.n = 1 + size(h.left) + size(h.right);
        return x;
    }

    /**
     * 右旋转结点h的左链接
     *
     * @param h 结点
     */
    private Node rotateRight(Node h) {
        Node x = h.left;
        h.left = x.right;
        x.right = h;
        x.color = h.color;
        h.color = RED;
        x.n = h.n;
        h.n = 1 + size(h.left) + size(h.right);
        return x;
    }

    /**
     * 转换结点h的颜色,全部转换成黑色
     *
     * @param h 结点
     */
    private void flipColors(Node h) {
        h.color = RED;
        h.left.color = BLACK;
        h.right.color = BLACK;
    }

    private boolean isRed(Node x) {
        if (x == null) {
            return false;
        }
        return x.color == RED;
    }

    private class Node {
        private Key key;
        private Value value;
        private Node left;
        private Node right;
        /**
         * 以该结点为跟的子树中的结点总数
         */
        private int n;
        /**
         * 由其父结点指向它的链接的颜色
         */
        private boolean color;

        public Node() {
        }

        public Node(Key key, Value value, int n, boolean color) {
            this.key = key;
            this.value = value;
            this.n = n;
            this.color = color;
        }
    }
}