package algorithms04.search.hash;


import algorithms04.Queue;
import algorithms04.search.St;
import algorithms04.search.st.SequentialSearchSt;

/**
 * 基于拉链法的散列表
 *
 * @author HHH
 */
public class SeparateChainingHashSt<Key, Value> implements St<Key, Value> {
    /**
     * 散列表的大小
     */
    private final int m;
    /**
     * 存放链表对象的数组
     */
    private final SequentialSearchSt<Key, Value>[] st;
    /**
     * 键值对总数
     */
    private int n;

    /**
     * 构造一个容量为997的散列表
     */
    public SeparateChainingHashSt() {
        this(997);
    }

    /**
     * 创建初始容量为m的散列表
     *
     * @param m 初始容量
     */
    public SeparateChainingHashSt(int m) {
        this.m = m;
        st = (SequentialSearchSt<Key, Value>[]) new SequentialSearchSt[m];
        for (int i = 0; i < m; i++) {
            st[i] = new SequentialSearchSt<>();
        }
    }

    /**
     * 将键值对存入表中(若值为null则将键key从表中删除)
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void put(Key key, Value value) {
        st[hash(key)].put(key, value);
        n++;
    }

    /**
     * 返回键key对应的值,如果键不存在则返回null
     *
     * @param key 键
     * @return 键key对应的值, 如果键不存在则返回null
     */
    @Override
    public Value get(Key key) {
        return st[hash(key)].get(key);
    }

    /**
     * 从表中删除键key及对应的值
     *
     * @param key 键
     */
    @Override
    public void delete(Key key) {
        st[hash(key)].delete(key);
    }

    /**
     * 返回true如果键存在,否则false
     *
     * @param key 键
     * @return true如果键存在, 否则false
     */
    @Override
    public boolean contains(Key key) {
        return get(key) != null;
    }

    /**
     * 返回true如果表不为空,否则false
     *
     * @return true如果表不为空, 否则false
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * 返回表键值对的数量
     *
     * @return 表键值对的数量
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 返回表中所有键的集合
     *
     * @return 表中所有键的集合
     */
    @Override
    public Iterable<Key> keys() {
        Queue<Key> queue = new Queue<>();
        for (int i = 0; i < m; i++) {
            for (Key integer : st[i].keys()) {
                queue.enqueue(integer);
            }
        }
        return queue;
    }

    private int hash(Key key) {
        return (key.hashCode() & 0x7fffffff) % m;
    }
}
