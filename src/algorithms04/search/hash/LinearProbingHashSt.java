package algorithms04.search.hash;

import algorithms04.search.St;

/**
 * 基于线性探索的符号表
 *
 * @author HHH
 */
public class LinearProbingHashSt<Key, Value> implements St<Key, Value> {
    /**
     * 键值对总数
     */
    private int n;
    /**
     * 散列表的大小
     */
    private int m = 16;
    private Key[] keys;
    private Value[] values;

    /**
     * 构造一个默认容量为16的散列表
     */
    public LinearProbingHashSt() {
        keys = (Key[]) new Object[m];
        values = (Value[]) new Object[m];
    }

    /**
     * 构造一个容量为cap的散列表
     *
     * @param cap 初始容量
     */
    public LinearProbingHashSt(int cap) {
        this.m = cap;
        keys = (Key[]) new Object[m];
        values = (Value[]) new Object[m];
    }

    /**
     * 将键值对存入表中(若值为null则将键key从表中删除)
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void put(Key key, Value value) {
        if (n >= m / 2) {
            resize(2 * m);
        }
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % m) {
            if (keys[i].equals(key)) {
                values[i] = value;
                return;
            }
        }
        keys[i] = key;
        values[i] = value;
        n++;
    }

    /**
     * 返回键key对应的值,如果键不存在则返回null
     *
     * @param key 键
     * @return 键key对应的值, 如果键不存在则返回null
     */
    @Override
    public Value get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % m) {
            if (keys[i].equals(key)) {
                return values[i];
            }
        }
        return null;
    }

    /**
     * 从表中删除键key及对应的值
     *
     * @param key 键
     */
    @Override
    public void delete(Key key) {
        if (!contains(key)) {
            return;
        }
        int i = hash(key);
        while (!key.equals(keys[i])) {
            i = (i + 1) % m;
        }
        keys[i] = null;
        values[i] = null;
        while (keys[i] != null) {
            Key keyToRedo = keys[i];
            Value valueToRedo = values[i];
            keys[i] = null;
            values[i] = null;
            n--;
            put(keyToRedo, valueToRedo);
            i = (i + 1) % m;
        }
        n--;
        if (n > 0 && n == m / 8) {
            resize(m / 2);
        }

    }

    /**
     * 返回true如果键存在,否则false
     *
     * @param key 键
     * @return true如果键存在, 否则false
     */
    @Override
    public boolean contains(Key key) {
        return get(key) != null;
    }

    /**
     * 返回true如果表不为空,否则false
     *
     * @return true如果表不为空, 否则false
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * 返回表键值对的数量
     *
     * @return 表键值对的数量
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 返回表中所有键的集合
     *
     * @return 表中所有键的集合
     */
    @Override
    public Iterable<Key> keys() {
        return null;
    }

    /**
     * 返回键key的散列值
     * @param key 键
     * @return key的散列值
     */
    private int hash(Key key) {
        return (key.hashCode() & 0x7fffffff) % m;
    }

    private void resize(int cap) {
        LinearProbingHashSt<Key, Value> t = new LinearProbingHashSt<>(cap);
        for (int i = 0; i < m; i++) {
            if (keys[i] != null) {
                t.put(keys[i], values[i]);
            }
        }
        keys = t.keys;
        values = t.values;
        m = t.m;
    }
}