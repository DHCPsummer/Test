package algorithms04.search;

/**
 * 有序符号表API
 * 不接受重复键或空键，值不能为空
 *
 * @author HHH
 */
public interface OrderSt<Key, Value> extends St<Key, Value> {
    /**
     * 返回最小的键
     *
     * @return 最小的键
     */
    Key min();

    /**
     * 返回最大的键
     *
     * @return 最大的键
     */
    Key max();

    /**
     * 返回小于等于键key的最大键
     *
     * @param key 键
     * @return 小于等于key的最大键
     */
    Key floor(Key key);

    /**
     * 返回大于等于键key的最小键
     *
     * @param key 键
     * @return 大于等于键key的最小键
     */
    Key ceiling(Key key);

    /**
     * 返回小于键key的键的数量
     *
     * @param key 键
     * @return 小于键key的键的数量
     */
    int rank(Key key);

    /**
     * 返回排名(索引)为k的键
     *
     * @param k 排名(索引)
     * @return 排名(索引)为k的键
     */
    Key select(int k);

    /**
     * 删除最小的键
     */
    void deleteMin();

    /**
     * 删除最大的键
     */
    void deleteMax();

    /**
     * 返回[lo...hi]之间键的数量
     *
     * @param lo 左边
     * @param hi 右边
     * @return [lo...hi]之间键的数量
     */
    int size(Key lo, Key hi);

    /**
     * 返回[lo...hi]之间键所有的键，已排序
     *
     * @param lo 左边
     * @param hi 右边
     * @return [lo...hi]之间键所有的键
     */
    Iterable<Key> keys(Key lo, Key hi);
}