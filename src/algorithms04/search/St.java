package algorithms04.search;

/**
 * 符号表API
 * 不接受重复键或空键，值不能为空
 *
 * @author HHH
 */
public interface St<Key, Value> {
    /**
     * 将键值对存入表中(若值为null则将键key从表中删除)
     *
     * @param key   键
     * @param value 值
     */
    void put(Key key, Value value);

    /**
     * 返回键key对应的值,如果键不存在则返回null
     *
     * @param key 键
     * @return 键key对应的值, 如果键不存在则返回null
     */
    Value get(Key key);

    /**
     * 从表中删除键key及对应的值
     *
     * @param key 键
     */
    void delete(Key key);

    /**
     * 返回true如果键存在,否则false
     *
     * @param key 键
     * @return true如果键存在, 否则false
     */
    boolean contains(Key key);

    /**
     * 返回true如果表不为空,否则false
     *
     * @return true如果表不为空, 否则false
     */
    boolean isEmpty();

    /**
     * 返回表键值对的数量
     *
     * @return 表键值对的数量
     */
    int size();

    /**
     * 返回表中所有键的集合
     *
     * @return 表中所有键的集合
     */
    Iterable<Key> keys();
}
