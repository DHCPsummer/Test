package algorithms04.search.st;

import algorithms04.search.OrderSt;
import edu.princeton.cs.algs4.Queue;

/**
 * 二分查找符号表，基于有序数组
 *
 * @author HHH
 */
public class BinarySearchSt<Key extends Comparable<Key>, Value> implements OrderSt<Key, Value> {
    /**
     * 键
     */
    private Key[] keys;
    /**
     * 值
     */
    private Value[] values;
    /**
     * 符号表长度
     */
    private int n;

    /**
     * 构造一个容量为10的符号表
     */
    public BinarySearchSt() {
        this(10);
    }

    /**
     * 构造一个容量为capacity的符号表
     *
     * @param capacity 容量
     */
    public BinarySearchSt(int capacity) {
        keys = (Key[]) new Comparable[capacity];
        values = (Value[]) new Object[capacity];
    }

    private void resize(int max) {
        Key[] temp = (Key[]) new Comparable[max];
        Value[] temp1 = (Value[]) new Object[max];
        System.arraycopy(keys, 0, temp, 0, n);
        System.arraycopy(values, 0, temp1, 0, n);
        keys = temp;
        values = temp1;
    }

    /**
     * 返回最小的键
     *
     * @return 最小的键
     */
    @Override
    public Key min() {
        return keys[0];
    }

    /**
     * 返回最大的键
     *
     * @return 最大的键
     */
    @Override
    public Key max() {
        return keys[n - 1];
    }

    /**
     * 返回小于等于键key的最大键
     *
     * @param key 键
     * @return 小于大于key的最大键
     */
    @Override
    public Key floor(Key key) {
        if (contains(key)) {
            return key;
        }
        int i = rank(key);
        return keys[i - 1];
    }

    /**
     * 返回大于等于键key的最小键
     *
     * @param key 键
     * @return 大于等于键key的最小键
     */
    @Override
    public Key ceiling(Key key) {
        int i = rank(key);
        return keys[i];
    }

    /**
     * 返回小于键key的键的数量
     *
     * @param key 键
     * @return 小于键key的键的数量
     */
    @Override
    public int rank(Key key) {
        int lo = 0;
        int hi = n - 1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            int cmp = key.compareTo(keys[mid]);
            if (cmp < 0) {
                hi = mid - 1;
            } else if (cmp > 0) {
                lo = mid + 1;
            } else {
                return mid;
            }
        }
        return lo;
    }

    /**
     * 返回排名(索引)为k的键
     *
     * @param k 排名(索引)
     * @return 排名(索引)为k的键
     */
    @Override
    public Key select(int k) {
        return keys[k];
    }

    /**
     * 删除最小的键
     */
    @Override
    public void deleteMin() {
        delete(min());
    }

    /**
     * 删除最大的键
     */
    @Override
    public void deleteMax() {
        delete(max());
    }

    /**
     * 返回[lo...hi]之间键的数量
     *
     * @param lo 左边
     * @param hi 右边
     * @return [lo...hi]之间键的数量
     */
    @Override
    public int size(Key lo, Key hi) {
        int left = rank(lo);
        int right = rank(hi);
        if (right - left < 0) {
            return 0;
        } else if (contains(hi)) {
            return right - left + 1;
        } else {
            return right - left;
        }
    }

    /**
     * 返回[lo...hi]之间键所有的键，已排序
     *
     * @param lo 左边
     * @param hi 右边
     * @return [lo...hi]之间键所有的键
     */
    @Override
    public Iterable<Key> keys(Key lo, Key hi) {
        Queue<Key> queue = new Queue<>();
        for (int i = rank(lo); i < rank(hi); i++) {
            queue.enqueue(keys[i]);
        }
        if (contains(hi)) {
            queue.enqueue(keys[rank(hi)]);
        }
        return queue;
    }

    /**
     * 将键值对存入表中(若值为null则将键key从表中删除)
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void put(Key key, Value value) {
        if (n == keys.length) {
            resize(2 * keys.length);
        }
        // 找到key的索引,这里可以保证数组keys[]是有序的
        int i = rank(key);
        // 命中更新
        if (i < n && keys[i].compareTo(key) == 0) {
            values[i] = value;
            return;
        }
        // 不命中
        // 将索引为i后面元素往后一位移,以便在索引i的位置插入新的元素,以保证符号表在插入后是有序的
        for (int j = n; j > i; j--) {
            keys[j] = keys[j - 1];
            values[j] = values[j - 1];
        }
        keys[i] = key;
        values[i] = value;
        n++;
    }

    /**
     * 返回键key对应的值,如果键不存在则返回null
     *
     * @param key 键
     * @return 键key对应的值, 如果键不存在则返回null
     */
    @Override
    public Value get(Key key) {
        if (isEmpty()) {
            return null;
        }
        int i = rank(key);
        if (i < n && keys[i].compareTo(key) == 0) {
            return values[i];
        } else {
            return null;
        }
    }

    /**
     * 从表中删除键key及对应的值
     *
     * @param key 键
     */
    @Override
    public void delete(Key key) {
        int i = rank(key);
        for (int j = i + 1; j < n; j++, i++) {
            keys[i] = keys[j];
            values[i] = values[j];
        }
        keys[n - 1] = null;
        values[n - 1] = null;
        n--;
    }

    /**
     * 返回true如果键存在,否则false
     *
     * @param key 键
     * @return true如果键存在, 否则false
     */
    @Override
    public boolean contains(Key key) {
        return get(key) != null;
    }

    /**
     * 返回true如果表不为空,否则false
     *
     * @return true如果表不为空, 否则false
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * 返回表键值对的数量
     *
     * @return 表键值对的数量
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 返回表中所有键的集合
     *
     * @return 表中所有键的集合
     */
    @Override
    public Iterable<Key> keys() {
        return keys(min(), max());
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (int i = 0; i < n; i++) {
            if (i != n - 1) {
                sb.append(keys[i]).append(" = ").append(values[i]).append(", ");
            } else {
                sb.append(keys[i]).append(" = ").append(values[i]);
            }
        }
        sb.append("}");
        return sb.toString();
    }
}