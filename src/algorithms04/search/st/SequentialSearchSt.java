package algorithms04.search.st;

import algorithms04.LinkedStack;
import algorithms04.search.St;

/**
 * 基于链表的符号表
 *
 * @author HHH
 */
public class SequentialSearchSt<Key, Value> implements St<Key, Value> {
    /**
     * 头指针
     */
    private Node first;
    /**
     * 符号表长度
     */
    private int n;

    /**
     * 将键值对存入表中(若值为null则将键key从表中删除)
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void put(Key key, Value value) {
        for (Node x = first; x != null; x = x.next) {
            // 命中更新
            if (key.equals(x.key)) {
                x.value = value;
                return;
            }
        }
        // 未命中新建结点,从链表头部加入
        first = new Node(key, value, first);
        n++;
    }

    /**
     * 返回键key对应的值,如果键不存在则返回null
     *
     * @param key 键
     * @return 键key对应的值, 如果键不存在则返回null
     */
    @Override
    public Value get(Key key) {
        for (Node x = first; x != null; x = x.next) {
            // 命中
            if (key.equals(x.key)) {
                return x.value;
            }
        }
        // 未命中
        return null;
    }

    /**
     * 从表中删除键key及对应的值
     *
     * @param key 键
     */
    @Override
    public void delete(Key key) {
        put(key, null);
        n--;
    }

    /**
     * 返回true如果键存在,否则false
     *
     * @param key 键
     * @return true如果键存在, 否则false
     */
    @Override
    public boolean contains(Key key) {
        return get(key) != null;
    }

    /**
     * 返回true如果表不为空,否则false
     *
     * @return true如果表不为空, 否则false
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * 返回表键值对的数量
     *
     * @return 表键值对的数量
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 返回表中所有键的集合
     *
     * @return 表中所有键的集合
     */
    @Override
    public Iterable<Key> keys() {
        LinkedStack<Key> stack = new LinkedStack<>();
        for (Node x = first; x != null; x = x.next) {
            if (x.value != null) {
                stack.push(x.key);
            }
        }
        return stack;
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "{}";
        }

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        int count = 1;
        for (Key i : keys()) {
            if (count != n) {
                sb.append(i).append(" = ").append(get(i)).append(", ");
                count++;
            } else {
                sb.append(i).append(" = ").append(get(i));
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private class Node {
        Key key;
        Value value;
        Node next;

        public Node() {
        }

        public Node(Key key, Value value, Node next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
}
