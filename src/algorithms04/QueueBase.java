package algorithms04;

/**
 * 队列API
 *
 * @author HHH
 */
public interface QueueBase<Item> extends Iterable<Item> {
    /**
     * 返回true,如果队列为空,否则false
     *
     * @return true, 如果队列为空, 否则false
     */
    boolean isEmpty();

    /**
     * 返回队列中含有的元素个数
     *
     * @return 队列中含有的元素个数
     */
    int size();

    /**
     * 向队列中加入元素item(从表尾加)
     *
     * @param item 要加入的元素
     */
    void enqueue(Item item);

    /**
     * 返回表头第一个元素,并将它删除
     *
     * @return 表头第一个元素, 并将它删除
     */
    Item dequeue();
}
