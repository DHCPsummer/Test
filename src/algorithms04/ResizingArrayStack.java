package algorithms04;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 栈，基于动态调整数组实现
 *
 * @author HHH
 */
public class ResizingArrayStack<Item> implements StackBase<Item> {
    /**
     * 栈元素
     */
    private Item[] a = (Item[]) new Object[1];
    /**
     * 栈中元素个数
     */
    private int n = 0;

    /**
     * 返回true,如果栈为空,否则false
     *
     * @return true, 如果栈为空, 否则false
     */
    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    /**
     * 返回栈中含有的元素个数
     *
     * @return 栈中含有的元素个数
     */
    @Override
    public int size() {
        return n;
    }

    /**
     * 把元素压入栈顶
     *
     * @param item 要添加的元素
     */
    @Override
    public void push(Item item) {
        if (n == a.length) {
            resize(a.length << 1);
        }
        a[n] = item;
        n++;
    }

    /**
     * 将数组的容量大小扩容到max
     *
     * @param max 要扩容到的值
     */
    private void resize(int max) {
        Item[] temp = (Item[]) new Object[max];
        System.arraycopy(a, 0, temp, 0, n);
        a = temp;
    }

    /**
     * 返回栈顶元素,并删除它
     *
     * @return 栈顶元素, 并删除它
     */
    @Override
    public Item pop() {
        Item item = a[--n];
        // 避免对象游离
        a[n] = null;
        // 缩小的倍数
        int reductionFactor = 4;
        // 栈中元素较少,将数组大小缩小
        if (n > 0 && n == a.length / reductionFactor) {
            resize(a.length / 2);
        }
        return item;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<Item> iterator() {
        return new ResizingArrayIterator();
    }

    private class ResizingArrayIterator implements Iterator<Item> {
        private int i = n;

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return i > 0;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public Item next() {
            return a[--i];
        }
    }
}
