package gdlgxy.coursedesign.firstquestion;

import java.util.Random;

/**
 * 操作类<br/>
 * 录入方法{@link Operate#insert()}<br/>
 * 删除方法{@link Operate#delete(int)}<br/>
 * 插入方法{@link Operate#insert(int)} <br/>
 * 查找方法{@link Operate#select(int)}
 *
 * @author 第二组
 */
public class Operate {
    /**
     * 存储链表
     */
    private LinkList<Student> listElem = new LinkList<>();
    /**
     * 随机数对象
     */
    private Random random = new Random();

    /**
     * 录入<br/>
     * 该方法将按id升序的方式插入一个各项费用随机且费用满足要求的学生
     */
    public void insert() {
        Student student = randomStudent(listElem.length());
        listElem.insert(listElem.length(), student);
        System.out.println("录入后的信息：");
        listElem.display();
    }

    /**
     * 删除学号为id的学生
     *
     * @param id 学生学号
     */
    public void delete(int id) {
        boolean success = false;
        for (int i = 0; i < listElem.length(); i++) {
            Student student = listElem.get(i);
            if (student.getId() == id) {
                listElem.remove(i);
                success = true;
            }
        }
        // 是否删除成功
        if (success) {
            System.out.println("删除成功");
            // 删除后是否还存在元素
            if (listElem.isEmpty()) {
                System.out.println("没有学生信息了");
            } else {
                System.out.println("删除后的信息：");
                listElem.display();
            }
        } else {
            System.out.println("删除失败,该学号" + id + "不存在");
        }
    }

    /**
     * 在学号为id的学生前插入学生
     *
     * @param id 学生学号
     */
    public void insert(int id) {
        // 这里捕获异常是为了能够让用户继续操作
        try {
            listElem.insert(id, randomStudent(listElem.length()));
        } catch (IndexOutOfBoundsException e) {
            System.out.println("插入失败\n该学号不存在，请先插入学号为" + id + "的学生");
            return;
        }
        System.out.println("插入成功,插入后的信息：");
        listElem.display();
    }

    /**
     * 查找学号为id的学生
     *
     * @param id 学号
     */
    public void select(int id) {
        Student result = null;
        for (Student student : listElem) {
            if (student.getId() == id) {
                result = student;
            }
        }
        if (result == null) {
            System.out.println("没有该学号为" + id + "学生");
        } else {
            System.out.println(result);
        }
    }

    public LinkList<Student> getListElem() {
        return listElem;
    }

    /**
     * 该方法生成一个学号为id，费用随机且满足条件的学生
     *
     * @param id 学号
     * @return 一个学号为id，费用随机且满足条件的学生
     */
    private Student randomStudent(int id) {
        int foodExpenses = random.nextInt(6000 - 3600 + 1) + 3600;
        int onlineShoppingExpenses = random.nextInt(2000 + 1);
        int partyExpenses = random.nextInt(1000 + 1);
        int textualResearchFee = random.nextInt(500 + 1);
        return new Student(id, foodExpenses, onlineShoppingExpenses, partyExpenses, textualResearchFee);
    }
}
