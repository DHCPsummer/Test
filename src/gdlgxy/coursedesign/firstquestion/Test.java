package gdlgxy.coursedesign.firstquestion;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 测试类
 *
 * @author 第二组
 */
public class Test {
    /**
     * 正则表达式匹配数字
     */
    private static final Pattern P = Pattern.compile("\\d+");

    public static void main(String[] args) {
        Operate operate = new Operate();
        Scanner sc = new Scanner(System.in);
        int input;
        do {
            System.out.print("0.退出,1.录入,2.查询,3.插入,4.删除");
            input = checkNumber(sc);
            int id;
            if (input == 0) {
                break;
            }
            switch (input) {
                case 1:
                    operate.insert();
                    break;
                case 2:
                    if (operate.getListElem().isEmpty()) {
                        System.out.println("请先插入学生信息");
                    } else {
                        System.out.print("请输入要查询的id：");
                        id = checkNumber(sc);
                        operate.select(id);
                    }
                    break;
                case 3:
                    System.out.print("请输入在哪个id前插入一个学生信息：");
                    id = checkNumber(sc);
                    operate.insert(id);
                    break;
                case 4:
                    if (operate.getListElem().isEmpty()) {
                        System.out.println("请先插入学生信息");
                    } else {
                        System.out.print("请输入要删除哪个id的学生信息：");
                        id = checkNumber(sc);
                        operate.delete(id);
                    }
                    break;
                default:
                    System.out.println("请输入0~4的数字");
                    break;
            }
        } while (true);
        System.out.println("退出");
    }

    public static int checkNumber(Scanner scanner) {
        String number = scanner.next();
        Matcher m = P.matcher(number);
        while (!m.matches()) {
            System.out.println("请输入整型数字");
            number = scanner.next();
            m = P.matcher(number);
        }
        return Integer.parseInt(number);
    }
}
