package gdlgxy.coursedesign.firstquestion;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 单链表<br/>
 * 如果调用{@link LinkList#insert(int, Object)}方法对同一个索引进行多次插入,将使用头插法插入<br/>
 * 该链表允许插入null值<br/>
 * 该单链表参考了JDK1.8的LinkedList类，但该类并没实现Cloneable接口、java.io.Serializable接口<br/>
 * Iterable接口的实现参考了《算法（第四版）》P98算法1.4
 *
 * @param <T> 此单链表中保存的元素类型
 * @author 第二组
 */
public class LinkList<T> implements IList<T>, Iterable<T> {
    /**
     * 链表第一个结点
     */
    private Node first;
    /**
     * 链表中含义的元素个数
     */
    private int size;

    @Override
    public void clear() {
        Node current = first;
        while (current != null) {
            Node x = current.next;
            current.item = null;
            current.next = null;
            current = x;
        }
        first = null;
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public int length() {
        return size;
    }

    @Override
    public T get(int i) {
        checkElementIndex(i);
        Node current = first;
        for (int j = 0; j < i; j++) {
            current = current.next;
        }
        return current.item;
    }

    @Override
    public void insert(int i, T x) {
        if (!(i >= 0 && i <= size)) {
            throw new IndexOutOfBoundsException("该索引值:" + i + "超过范围");
        }
        // 插入的位置是否为第一个
        if (i == 0) {
            Node oldFirst = first;
            first = new Node(x, oldFirst);
        } else {
            Node pre = first;
            // 找到插入位置的前一个结点
            for (int j = 0; j < i - 1; j++) {
                pre = pre.next;
            }
            Node oldNode = pre.next;
            pre.next = new Node(x, oldNode);
        }
        size++;
    }

    @Override
    public void remove(int i) {
        checkElementIndex(i);
        // 链表中是否只有一个结点
        if (size == 1) {
            first = null;
        } else if (i == 0) {
            first = first.next;
        } else {
            Node pre = first;
            // 找到删除位置的前一个结点
            for (int j = 0; j < i - 1; j++) {
                pre = pre.next;
            }
            pre.next = pre.next.next;
        }
        size--;
    }

    @Override
    public int indexOf(T x) {
        int index = 0;
        if (x == null) {
            for (Node n = first; n != null; n = n.next) {
                if (n.item == null) {
                    return index;
                }
                index++;
            }
        } else {
            for (Node n = first; n != null; n = n.next) {
                if (x.equals(n.item)) {
                    return index;
                }
                index++;
            }
        }
        return -1;
    }

    @Override
    public void display() {
        Node current = first;
//        StringBuilder sb = new StringBuilder();
//        while (current != null) {
//            sb.append(current.item).append(" \n");
//            current = current.next;
//        }
//        System.out.println(sb);
        String s = "";
        while (current != null) {
            s += current.item+" \n";
            current = current.next;
        }
        System.out.println(s);
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedIterator();
    }

    private void checkElementIndex(int index) {
        if (index < 0 || index >= length()) {
            throw new IndexOutOfBoundsException("该索引值:" + index + "超过范围");
        }
    }

    private class LinkedIterator implements Iterator<T> {
        private Node current = first;

        /**
         * 如果迭代有更多元素，则返回 {@code true}。（换句话说，如果 {@link #next} 将返回一个元素而不是抛出异常，则返回 {@code true}。）
         *
         * @return 如果迭代有更多元素，则返回{@code true}
         */
        @Override
        public boolean hasNext() {
            return current != null;
        }

        /**
         * 返回迭代中的下一个元素。
         *
         * @return 迭代中的下一个元素
         * @throws NoSuchElementException 如果迭代没有更多元素
         */
        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            T item = current.item;
            current = current.next;
            return item;
        }
    }

    /**
     * 结点类
     */
    private class Node {
        private T item;
        private Node next;

        public Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }
}
