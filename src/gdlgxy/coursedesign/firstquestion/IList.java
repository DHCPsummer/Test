package gdlgxy.coursedesign.firstquestion;

/**
 * 线性表的接口
 *
 * @param <T> 此集合中保存的元素类型
 * @author 第二组
 */
public interface IList<T> {
    /**
     * 将一个已经存在的线性表置成空表
     */
    void clear();

    /**
     * 判断当前线性表中的数据元素个数是否为0,若为0则函数返回true，否则返回false
     *
     * @return true，如果线性表中的元素不为0
     */
    boolean isEmpty();

    /**
     * 求线性表中的数据元素个数并由函数返回其值
     *
     * @return 线性表中的数据元素个数
     */
    int length();

    /**
     * 读取到线性表中的第i个数据元素并由函数返回其值
     *
     * @param i 索引
     * @return 线性表中的第i个数据元素
     * @throws IndexOutOfBoundsException i < 0 || i >= length()-1
     */
    T get(int i);

    /**
     * 在线性表的第i个数据元素之前插入一个值为x的数据元素。当i=0时表示在表头插入一个数据元素x,当i=length()时表示在表尾插入一个数据元素x
     *
     * @param i 索引
     * @param x 元素
     * @throws IndexOutOfBoundsException i < 0 || i > length()
     */
    void insert(int i, T x);

    /**
     * 将线性表中第i个数据元素删除
     *
     * @param i 索引
     * @throws IndexOutOfBoundsException i < 0 || i >= length()-1
     */
    void remove(int i);

    /**
     * 返回线性表中首次出现指定元素x的索引，如果列表不包含此元素，则返回 -1
     *
     * @param x 元素
     * @return 线性表中首次出现指定元素x的索引
     */
    int indexOf(T x);

    /**
     * 输出线性表中的数据元素
     */
    void display();
}
