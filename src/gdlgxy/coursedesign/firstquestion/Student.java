package gdlgxy.coursedesign.firstquestion;

/**
 * 学生类
 *
 * @author 第二组
 */
public class Student {
    /**
     * 学生学号
     */
    private int id;
    /**
     * 伙食费用<br/>
     * 取值范围：[3600 , 6000]
     */
    private int foodExpenses;
    /**
     * 网购费用<br/>
     * 取值范围：[0 , 2000]
     */
    private int onlineShoppingExpenses;
    /**
     * 聚会费用<br/>
     * 取值范围：[0 , 1000]
     */
    private int partyExpenses;
    /**
     * 考证费用<br/>
     * 取值范围：[0 , 500]
     */
    private int textualResearchFee;


    /**
     * 构造函数
     *
     * @param id                     学生学号
     * @param foodExpenses           伙食费
     * @param onlineShoppingExpenses 网上购物费用
     * @param partyExpenses          聚会费用
     * @param textualResearchFee     考证费用
     */
    public Student(int id, int foodExpenses, int onlineShoppingExpenses, int partyExpenses, int textualResearchFee) {
        this.id = id;
        this.foodExpenses = foodExpenses;
        this.onlineShoppingExpenses = onlineShoppingExpenses;
        this.partyExpenses = partyExpenses;
        this.textualResearchFee = textualResearchFee;
    }

    public int getId() {
        return id;
    }

    public int getFoodExpenses() {
        return foodExpenses;
    }

    public int getOnlineShoppingExpenses() {
        return onlineShoppingExpenses;
    }

    public int getPartyExpenses() {
        return partyExpenses;
    }

    public int getTextualResearchFee() {
        return textualResearchFee;
    }

    @Override
    public String toString() {
        return "学号为：" + id +
                ", 伙食费用为：" + foodExpenses +
                ", 网购费用为：" + onlineShoppingExpenses +
                ", 聚会费用为：" + partyExpenses +
                ", 考证费用为：" + textualResearchFee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        return id == student.id && foodExpenses == student.foodExpenses
                && onlineShoppingExpenses == student.onlineShoppingExpenses && partyExpenses == student.partyExpenses
                && textualResearchFee == student.textualResearchFee;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + foodExpenses;
        result = 31 * result + onlineShoppingExpenses;
        result = 31 * result + partyExpenses;
        result = 31 * result + textualResearchFee;
        return result;
    }
}
