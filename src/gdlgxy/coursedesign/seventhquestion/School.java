package gdlgxy.coursedesign.seventhquestion;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

/**
 * 学校类
 *
 * @author HHH
 */
public class School {
    /**
     * 学校名
     */
    private String name;
    /**
     * 学校id
     */
    private int id;
    /**
     * 该学校参赛学生
     */
    private Student[] students;
    /**
     * 学校的学生参加的项目和对应的分数
     */
    private HashMap<CompetitionItem,Integer> participatedInTheProject = new HashMap<>();

    /**
     * 构造函数
     *
     * @param name     学校名
     * @param id       学校id
     * @param students 该学校参赛学生
     */
    public School(String name, int id, Student[] students) {
        this.name = name;
        this.id = id;
        this.students = students;
    }

    /**
     * 计算学校团体总分
     *
     * @return 团体总分
     */
    public int getTotalScore() {
        int sum = 0;
        for (Student student : students) {
            sum += student.getTotalScore();
        }
        return sum;
    }

    /**
     * 计算学校男团体总分
     *
     * @return 男团体总分
     */
    public int getMaleTotalScore() {
        int sum = 0;
        for (Student student : students) {
            if (student.getSex() == '男') {
                sum += student.getTotalScore();
            }
        }
        return sum;
    }

    /**
     * 计算学校女团体总分
     *
     * @return 男团体总分
     */
    public int getFemaleTotalScore() {
        int sum = 0;
        for (Student student : students) {
            if (student.getSex() == '女') {
                sum += student.getTotalScore();
            }
        }
        return sum;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Student[] getStudents() {
        return students;
    }

    public HashMap<CompetitionItem,Integer> getParticipatedInTheProject() {
        for (Student student : students) {
            participatedInTheProject.putAll(student.getParticipatedInTheProject());
        }
        return participatedInTheProject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        School school = (School) o;

        if (id != school.id) {
            return false;
        }
        if (!Objects.equals(name, school.name)) {
            return false;
        }
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(students, school.students);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + Arrays.hashCode(students);
        return result;
    }

    @Override
    public String toString() {
        return "School{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
