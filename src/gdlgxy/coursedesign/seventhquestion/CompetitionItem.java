package gdlgxy.coursedesign.seventhquestion;

import java.util.Objects;

/**
 * 比赛项目
 *
 * @author HHH
 */
public class CompetitionItem {
    /**
     * 项目id
     */
    private int id;
    /**
     * 项目类型符<br/>true:取前五名<br/>false：取前三名
     */
    private boolean typeSymbol;
    /**
     * 比赛名
     */
    private String name;

    /**
     * 构造函数
     *
     * @param id         项目id
     * @param typeSymbol 项目类型符<br/>true:取前五名<br/>false：取前三名
     * @param name       比赛名
     */
    public CompetitionItem(int id, boolean typeSymbol, String name) {
        this.id = id;
        this.typeSymbol = typeSymbol;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public boolean isTypeSymbol() {
        return typeSymbol;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CompetitionItem that = (CompetitionItem) o;

        if (id != that.id) {
            return false;
        }
        if (typeSymbol != that.typeSymbol) {
            return false;
        }
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (typeSymbol ? 1 : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CompetitionItem{" +
                "id=" + id +
                ", typeSymbol=" + typeSymbol +
                ", name='" + name + '\'' +
                '}';
    }
}
