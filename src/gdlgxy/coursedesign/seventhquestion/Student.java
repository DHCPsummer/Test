package gdlgxy.coursedesign.seventhquestion;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 学生类
 *
 * @author HHH
 */
public class Student {
    /**
     * 学校名字
     */
    private String schoolName;
    /**
     * 学生名字
     */
    private String name;
    /**
     * 学生总分
     */
    private int totalScore;
    /**
     * 性别
     */
    private char sex;
    /**
     * 参加的项目和对应的分数
     */
    private HashMap<CompetitionItem, Integer> participatedInTheProject = new HashMap<>();

    /**
     * 构造函数
     *
     * @param schoolName 学校名字
     * @param name       学生名字
     * @param sex        性别
     */
    public Student(String schoolName, String name, char sex) {
        this.schoolName = schoolName;
        this.name = name;
        this.sex = sex;
    }

    /**
     * 为该名学生添加比赛项目和得到得分数
     *
     * @param competitionItem 比赛项目名
     * @param score           分数
     */
    public void setProject(CompetitionItem competitionItem, int score) {
        participatedInTheProject.put(competitionItem, score);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Student student = (Student) o;

        if (sex != student.sex) {
            return false;
        }
        if (!Objects.equals(schoolName, student.schoolName)) {
            return false;
        }
        return Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        int result = schoolName != null ? schoolName.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + sex;
        return result;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getName() {
        return name;
    }

    public int getTotalScore() {
        for (Map.Entry<CompetitionItem, Integer> entry : participatedInTheProject.entrySet()) {
            totalScore = totalScore + entry.getValue();
        }
        return totalScore;
    }

    public char getSex() {
        return sex;
    }

    public HashMap<CompetitionItem, Integer> getParticipatedInTheProject() {
        return participatedInTheProject;
    }
}
