package gdlgxy.algorithm.ch09;

import gdlgxy.algorithm.ch05.BiTreeNode;
import gdlgxy.algorithm.ch07.RecordNode;

/**
 * 第8章 二叉排序树类。
 *
 * @author gdlgxy
 */
public class BSTree {
    /**
     * 根结点
     */
    public BiTreeNode root;

    /**
     * 构造空二叉排序树
     */
    public BSTree() {
        root = null;
    }

    /**
     * 判断是否空二叉树
     *
     * @return true如果为空，否则false
     */
    public boolean isEmpty() {
        return this.root == null;
    }

    /**
     * 中根次序遍历以p结点为根的二叉树
     *
     * @param p 根结点
     */
    public void inOrderTraverse(BiTreeNode p) {
        if (p != null) {
            inOrderTraverse(p.leftChild);
            System.out.print(p.data.toString() + "");
            inOrderTraverse(p.rightChild);
        }
    }

    /**
     * 查找关键字值为key的结点,若查找成功返回结点值，否则返回null
     *
     * @param key 关键字
     * @return 节点值，如果找到，否则返回null
     */
    public Object searchBST(Comparable key) {
        if (key == null) {
            return null;
        }
        return searchBST(root, key);
    }

    /**
     * 二叉排序树查找的递归算法
     * 在二叉排序树中查找关键字为key的结点。若查找成功则返回结点值，否则返回null
     *
     * @param p   二叉排序树的根结点
     * @param key 关键字
     * @return 结点值，如果否则null
     */
    private Object searchBST(BiTreeNode p, Comparable key) {
        if (p != null) {
            // 查找成功
            if (key.compareTo(((RecordNode) p.data).key) == 0) {
                return p.data;
            }
            if (key.compareTo(((RecordNode) p.data).key) < 0) {
                // 在左子树中查找
                return searchBST(p.leftChild, key);
            } else {
                // 在右子树中查找
                return searchBST(p.rightChild, key);
            }
        }
        return null;
    }

    /**
     * 在二叉排序树中插入关键字为Key,数据项为theElement的结点,若插入成功返回true,否则返回false
     *
     * @param key        关键字
     * @param theElement 数据项
     * @return true，如果插入成功，否则false
     */
    public boolean insertBST(Comparable key, Object theElement) {
        // 不能插入空对象或不可比较大小的对象
        if (key == null) {
            return false;
        }
        if (root == null) {
            // 建立根结点
            root = new BiTreeNode(new RecordNode(key, theElement));
            return true;
        }
        return insertBST(root, key, theElement);
    }

    /**
     * 将关键字为key,数据项为theElement的结点插入到以p为根的二叉排序树中的递归算法
     *
     * @param p          树的根结点
     * @param key        关键字
     * @param theElement 数据项
     * @return true，如果插入成功，否则false
     */
    private boolean insertBST(BiTreeNode p, Comparable key, Object theElement) {
        // 不插入关键字重复的结点
        if (key.compareTo(((RecordNode) p.data).key) == 0) {
            return false;
        }
        if (key.compareTo(((RecordNode) p.data).key) < 0) {
            // 若p的左子树为空
            if (p.leftChild == null) {
                // 建立叶子结点作为p的左孩子
                p.leftChild = new BiTreeNode(new RecordNode(key, theElement));
                return true;
            } else {
                // 若p的左子树非空
                // 插入到p的左子树中
                return insertBST(p.leftChild, key, theElement);
            }
        } else if (p.rightChild == null) {
            // 若p的右子树为空
            // 建立叶子结点作为p的右孩子
            p.rightChild = new BiTreeNode(new RecordNode(key, theElement));
            return true;
        } else {
            // 若p的右子树非空
            // 插入到p的右子树中
            return insertBST(p.rightChild, key, theElement);
        }
    }

    /**
     * 二叉排序树中删除结点算法。若删除成功返回删除结点值，否则返回null
     * @param key 关键字
     * @return 删除结点值，否则false
     */
    public Object removeBST(Comparable key) {
        if (root == null || key == null) {
            return null;
        }
        //在以root为根的二叉排序树中删除关键字为elemKey的结点
        return removeBST(root, key, null);
    }

    /**
     * 在以p为根的二叉排序树中删除关键字为elemKey的结点。parent是p的父结点，递归算法
     * @param p 根结点
     * @param elemKey 关键字
     * @param parent p的父结点
     * @return 删除结点值，否则false
     */
    private Object removeBST(BiTreeNode p, Comparable elemKey, BiTreeNode parent) {
        if (p != null) {
            // 在左子树中删除
            if (elemKey.compareTo(((RecordNode) p.data).key) < 0) {
                // 在左子树中递归搜索
                return removeBST(p.leftChild, elemKey, p);
            } else if (elemKey.compareTo(((RecordNode) p.data).key) > 0) {
                // 在右子树中删除
                // 在右子树中递归搜索
                return removeBST(p.rightChild, elemKey, p);
            } else if (p.leftChild != null && p.rightChild != null) {
                // 相等且该结点有左右子树
                // 寻找p在中根次序下的后继结点innext
                BiTreeNode innext = p.rightChild;
                // 即寻找右子树中的最左孩子
                while (innext.leftChild != null) {
                    innext = innext.leftChild;
                }
                // 以后继结点值替换p
                p.data = innext.data;
                // 递归删除结点p
                return removeBST(p.rightChild, ((RecordNode) p.data).key, p);
            } else {
                // p是1度和叶子结点
                if (parent == null) {
                    // 删除根结点，即p==root
                    if (p.leftChild != null) {
                        root = p.leftChild;
                    } else {
                        root = p.rightChild;
                    }
                    // 返回删除结点p值
                    return p.data;
                }
                if (p == parent.leftChild) {
                    // p是parent的左孩子
                    if (p.leftChild != null) {
                        // 以p的左子树填补
                        parent.leftChild = p.leftChild;
                    } else {
                        parent.leftChild = p.rightChild;
                    }
                } else if (p.leftChild != null) {
                    // p是parent的右孩子且p的左子树非空
                    parent.rightChild = p.leftChild;
                } else {
                    parent.rightChild = p.rightChild;
                }
                return p.data;
            }
        }
        return null;
    }
}
