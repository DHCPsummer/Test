package gdlgxy.algorithm.ch03;


import gdlgxy.algorithm.ch02.Node;

/**
 * 链队列类
 *
 * @author gdlgxy
 */

public class LinkQueue implements IQueue {
    /**
     * 队头的引用
     */
    private Node front;
    /**
     * 队尾的引用，指向队尾元素
     */
    private Node rear;

    /**
     * 链队列类的构造函数
     */
    public LinkQueue() {
        front = rear = null;
    }

    /**
     * 将一个已经存在的队列置成空
     */
    @Override
    public void clear() {
        front = rear = null;
    }

    /**
     * 测试队列是否为空
     *
     * @return true，如果队列为空
     */
    @Override
    public boolean isEmpty() {
        return front == null;
    }

    /**
     * 求队列中的数据元素个数并由函数返回其值
     *
     * @return 队列中数据元素的个数
     */
    @Override
    public int length() {
        Node p = front;
        // 队列的长度
        int length = 0;
        // 一直查找到队尾
        while (p != null) {
            p = p.next;
            // 长度增1
            ++length;
        }
        return length;
    }

    /**
     * 把指定的元素a插入队列
     *
     * @param o 元素
     */
    @Override
    public void offer(Object o) {
        // 初始化新的结点
        Node p = new Node(o);
        // 队列非空
        if (front != null) {
            rear.next = p;
            // 改变队列尾的位置
            rear = p;
        } else {
            // 队列为空
            front = rear = p;
        }
    }

    /**
     * 查看队列的头而不移除它，返回队列顶对象，如果此队列为空，则返回 null
     *
     * @return 队列顶对象
     */
    @Override
    public Object peek() {
        // 队列非空
        if (front != null) {
            // 返回队列元素
            return front.data;
        } else {
            return null;
        }
    }

    /**
     * 移除队列的头并作为此函数的值返回该对象，如果此队列为空，则返回 null
     *
     * @return 队列的头
     */
    @Override
    public Object poll() {
        // 队列非空
        if (front != null) {
            // p指向队列头结点
            Node p = front;
            front = front.next;
            //被删的结点是队尾结点
            if (p == rear) {
                rear = null;
            }
            // 返回队列头结点数据
            return p.data;
        } else {
            return null;
        }
    }

    /**
     * 打印函数，打印所有队列中的元素(队列头到队列尾)
     */
    @Override
    public void display() {
        if (!isEmpty()) {
            Node p = front;
            // 从对头到队尾
            while (p != rear.next) {
                System.out.print(p.data.toString() + " ");
                p = p.next;
            }
        } else {
            System.out.println("此队列为空");
        }
    }

}
