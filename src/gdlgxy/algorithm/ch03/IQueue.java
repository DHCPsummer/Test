package gdlgxy.algorithm.ch03;

/**
 * 队列的接口
 *
 * @author gdlgxy
 */
public interface IQueue {
    /**
     * 将一个已经存在的队列置成空
     */
    void clear();

    /**
     * 测试队列是否为空
     *
     * @return true，如果队列为空
     */
    boolean isEmpty();

    /**
     * 求队列中的数据元素个数并由函数返回其值
     *
     * @return 队列中数据元素的个数
     */
    int length();

    /**
     * 查看队列的头而不移除它，返回队列顶对象，如果此队列为空，则返回 null
     *
     * @return 队列顶对象
     */
    Object peek();

    /**
     * 移除队列的头并作为此函数的值返回该对象，如果此队列为空，则返回 null
     *
     * @return 队列的头
     */
    Object poll();

    /**
     * 把指定的元素a插入队列
     *
     * @param o 元素
     */
    void offer(Object o);

    /**
     * 打印函数，打印所有队列中的元素(队列头到队列尾)
     */
    void display();
}
