package gdlgxy.algorithm.ch03;

import gdlgxy.algorithm.ch02.Node;

/**
 * 在单链表上实现的栈及其基本操作
 *
 * @author gdlgxy
 */
public class LinkStack implements IStack {
    /**
     * 栈顶元素的引用
     */
    private Node top;

    /**
     * 将一个已经存在的栈置成空
     */
    @Override
    public void clear() {
        top = null;
    }

    /**
     * 测试栈是否为空
     *
     * @return true，如果队列为空
     */
    @Override
    public boolean isEmpty() {
        return top == null;
    }

    /**
     * 求栈中的数据元素个数并由函数返回其值
     *
     * @return 栈中元素的个数
     */
    @Override
    public int length() {
        // 初始化,p指向栈顶结点,length为计数器
        Node p = top;
        int length = 0;
        // 从栈顶结点向后查找，直到p指向栈顶结点
        while (p != null) {
            // 指向后继结点
            p = p.next;
            // 长度增1
            ++length;
        }
        return length;
    }

    /**
     * 查看栈顶对象而不移除它，返回栈顶对象
     *
     * @return 栈顶对象但不移除
     */
    @Override
    public Object peek() {
        if (!isEmpty()) {
            // 返回栈顶元素
            return top.data;
        } else {
            return null;
        }
    }


    /**
     * 移除栈顶对象并作为此函数的值返回该对象
     *
     * @return 栈顶对象并移除该对象
     */
    @Override
    public Object pop() {
        if (!isEmpty()) {
            // p指向栈顶结点
            Node p = top;
            top = top.next;
            return p.data;
        } else {
            return null;
        }
    }

    /**
     * 把元素x压入栈顶
     *
     * @param x 元素
     */
    @Override
    public void push(Object x) {
        // 构造一个新的结点
        Node p = new Node(x);
        p.next = top;
        // 改变栈顶结点
        top = p;
    }

    /**
     * 打印函数，打印所有栈中的元素(栈底到栈顶)
     */
    @Override
    public void display() {
        // p指向栈顶结点，q指向p的下一结点
        Node p = top;
        // 打印所有非空的结点
        while (p != null) {
            // 打印
            System.out.print((p.data.toString() + " "));
            // 指向p下一元素
            p = p.next;
        }
    }
}
