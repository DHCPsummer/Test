package gdlgxy.algorithm.ch03;

/**
 * 栈的接口
 *
 * @author gdlgxy
 */
public interface IStack {
    /**
     * 将一个已经存在的栈置成空
     */
    void clear();

    /**
     * 测试栈是否为空
     *
     * @return true，如果队列为空
     */
    boolean isEmpty();

    /**
     * 求栈中的数据元素个数并由函数返回其值
     *
     * @return 栈中元素的个数
     */
    int length();

    /**
     * 查看栈顶对象而不移除它，返回栈顶对象
     *
     * @return 栈顶对象但不移除
     */
    Object peek();

    /**
     * 移除栈顶对象并作为此函数的值返回该对象
     *
     * @return 栈顶对象并移除该对象
     */
    Object pop();

    /**
     * 把元素o压入栈顶
     *
     * @param o 元素
     */
    void push(Object o);

    /**
     * 打印函数，打印所有栈中的元素(栈底到栈顶)
     */
    void display();
}
