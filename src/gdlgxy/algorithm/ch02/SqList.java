package gdlgxy.algorithm.ch02;

/**
 * 顺序线性表及其基本操作
 *
 * @author gdlgxy
 */
public class SqList implements IList {
    /**
     * 线性表存储空间
     */
    private final Object[] listElem;
    /**
     * 当前长度
     */
    private int curLen;

    /**
     * 无参构造器，默认构造一个容量为10的线性表
     */
    public SqList() {
        curLen = 0;
        listElem = new Object[10];
    }

    /**
     * 顺序表的构造函数，构造一个存储空间容量为maxSize的线性表
     *
     * @param maxSize 存储空间
     */
    public SqList(int maxSize) {
        // 置顺序表的当前长度为0
        curLen = 0;
        // 为顺序表分配maxSize个存储单元
        listElem = new Object[maxSize];
    }

    /**
     * 将一个已经存在的线性表置成空表
     */
    @Override
    public void clear() {
        // 置顺序表的当前长度为0
        curLen = 0;
    }

    /**
     * 判断当前线性表中的数据元素个数是否为0,若为0则函数返回true，否则返回false
     *
     * @return true，如果线性表中的元素不为0
     */
    @Override
    public boolean isEmpty() {
        return curLen == 0;
    }

    /**
     * 求线性表中的数据元素个数并由函数返回其值
     *
     * @return 线性表中的数据元素个数
     */
    @Override
    public int length() {
        // 返回顺序表的当前长度
        return curLen;
    }

    /**
     * 读取到线性表中的第i个数据元素并由函数返回其值
     *
     * @param i 索引
     * @return 线性表中的第i个数据元素
     * @throws Exception i < 0 || i >= length()-1
     */
    @Override
    public Object get(int i) throws Exception {
        if (i < 0 || i > curLen - 1) {
            // i小于0或者大于表长减1，输出异常
            throw new Exception("第" + i + "个元素不存在");
        }
        // 返回顺序表中第i个数据元素
        return listElem[i];
    }

    /**
     * 在线性表的第i个数据元素之前插入一个值为x的数据元素。当i=0时表示在表头插入一个数据元素x,当i=length()时表示在表尾插入一个数据元素x
     *
     * @param i 索引
     * @param x 元素
     * @throws Exception i < 0 || i > length()
     */
    @Override
    public void insert(int i, Object x) throws Exception {
        // 判断顺序表是否已满
        if (curLen == listElem.length) {
            // 输出异常
            throw new Exception("顺序表已满");
        }
        // i小于0或者大于表长
        if (i < 0 || i > curLen) {
            // 输出异常
            throw new Exception("插入位置不合理");
        }
        // 插入位置及之后的元素后移
        System.arraycopy(listElem, i, listElem, i + 1, curLen - i);
        // 插入x
        listElem[i] = x;
        // 表长度增1
        curLen++;
    }

    /**
     * 将线性表中第i个数据元素删除
     *
     * @param i 索引
     * @throws Exception i < 0 || i >= length()-1
     */
    @Override
    public void remove(int i) throws Exception {
        // i小于1或者大于表长减1
        if (i < 0 || i > curLen - 1) {
            // 输出异常
            throw new Exception("删除位置不合理");
        }
        // 被删除元素之后的元素左移
        if (curLen - 1 - i >= 0) {
            System.arraycopy(listElem, i + 1, listElem, i, curLen - 1 - i);
        }
        // 表长度减1
        curLen--;
    }

    /**
     * 返回线性表中首次出现指定元素x的索引，如果列表不包含此元素，则返回 -1
     *
     * @param x 元素
     * @return 线性表中首次出现指定元素x的索引
     */
    @Override
    public int indexOf(Object x) {
        // j为计数器
        int j = 0;
        // 从顺序表中的首结点开始查找，直到listElem[j]指向元素x或到达顺序表的表尾
        while (j < curLen && !listElem[j].equals(x)) {
            j++;
        }
        // 判断j的位置是否位于表中
        if (j < curLen) {
            // 返回x元素在顺序表中的位置
            return j;
        } else {
            // x元素不在顺序表中
            return -1;
        }
    }

    /**
     * 输出线性表中的数据元素
     */
    @Override
    public void display() {
        for (int j = 0; j < curLen; j++) {
            System.out.print(listElem[j] + " ");
        }
        // 换行
        System.out.println();
    }

    /**
     * 实现对顺序表就地逆置
     */
    public void reverse() {
        for (int i = 0, j = curLen - 1; i < j; i++, j--) {
            Object temp = listElem[i];
            listElem[i] = listElem[j];
            listElem[j] = temp;
        }
    }

    /**
     * 实现对顺序表右移k位
     * @param k 右移位数
     */
    public void shit(int k) {
        int n = curLen, p = 0, i, j, l;
        Object temp;
        for (i = 1; i <= k; i++) {
            // 求n和k的最大公约数p
            if (n % i == 0 && k % i == 0) {
                p = i;
            }
        }
        for (i = 0; i < p; i++) {
            j = i;
            l = (i + n - k) % n;
            temp = listElem[i];
            while (l != i) {
                listElem[j] = listElem[l];
                j = l;
                l = (j + n - k) % n;
            }// 循环右移一步
            listElem[j] = temp;
        }
    }
}
