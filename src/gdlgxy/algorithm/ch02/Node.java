package gdlgxy.algorithm.ch02;

/**
 * 单链表的结点
 *
 * @author gdlgxy
 */
public class Node {
    /**
     * 存放结点值
     */
    public Object data;
    /**
     * 后继结点的引用
     */
    public Node next;

    /**
     * 无参数时的构造函数
     */
    public Node() {
        this(null, null);
    }

    /**
     * 构造值为data的结点
     * @param data 结点存储的值
     */
    public Node(Object data) {
        this(data, null);
    }

    /**
     * 以值为date,下个一个结点为next构造一个结点
     * @param data 值
     * @param next 下一个结点
     */
    public Node(Object data, Node next) {
        this.data = data;
        this.next = next;
    }
}
