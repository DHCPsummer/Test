package gdlgxy.algorithm.ch02;


import java.util.Scanner;

/**
 * 带头结点的单链表及其基本操作
 *
 * @author gdlgxy
 */
public class LinkList implements IList {
    /**
     * 单链表的头指针
     */
    public Node head;

    /**
     * 单链表的构造函数
     */
    public LinkList() {
        // 初始化头结点
        head = new Node();
    }

    /**
     * 构造一个链表长度为n，order:true使用尾插法,false使用头插法
     *
     * @param n     链表长度
     * @param order 头插法还是尾插法
     */
    public LinkList(int n, boolean order) throws Exception {
        // 初始化头结点
        this();
        if (order) {
            // 用尾插法顺序建立单链表
            create1(n);
        } else {
            // 用头插法逆位序建立单链表
            create2(n);
        }
    }

    private void create1(int n) throws Exception {
        // 构造用于输入的对象
        Scanner sc = new Scanner(System.in);
        // 输入n个元素的值
        for (int j = 0; j < n; j++) {
            // 生成新结点,插入到表尾
            insert(length(), sc.next());
        }
    }

    private void create2(int n) throws Exception {
        // 构造用于输入的对象
        Scanner sc = new Scanner(System.in);
        // 输入n个元素的值
        for (int j = 0; j < n; j++) {
            // 生成新结点,插入到表头
            insert(0, sc.next());
        }
    }

    /**
     * 将一个已经存在的线性表置成空表
     */
    @Override
    public void clear() {
        head.data = null;
        head.next = null;
    }

    /**
     * 判断当前线性表中的数据元素个数是否为0,若为0则函数返回true，否则返回false
     *
     * @return true，如果线性表中的元素不为0
     */
    @Override
    public boolean isEmpty() {
        // 判断首结点是否为空
        return head.next == null;
    }

    /**
     * 求线性表中的数据元素个数并由函数返回其值
     *
     * @return 线性表中的数据元素个数
     */
    @Override
    public int length() {
        // 初始化,p指向首结点,length为计数器
        Node p = head.next;
        int length = 0;
        // 从首结点向后查找，直到p为空
        while (p != null) {
            // 指向后继结点
            p = p.next;
            // 长度增1
            ++length;
        }
        return length;
    }

    /**
     * 读取到线性表中的第i个数据元素并由函数返回其值
     *
     * @param i 索引
     * @return 线性表中的第i个数据元素
     * @throws Exception i < 0 || i >= length()-1
     */
    @Override
    public Object get(int i) throws Exception {
        // 初始化,p指向首结点,j为计数器
        Node p = head.next;
        int j = 0;
        // 从首结点向后查找，直到p指向第i个元素或p为空
        while (p != null && j < i) {
            // 指向后继结点
            p = p.next;
            // 计数器的值增1
            ++j;
        }
        // i小于0或者大于表长减1
        if (j > i || p == null) {
            // 输出异常
            throw new Exception("第" + i + "个元素不存在");
        }
        // 返回元素p
        return p.data;
    }

    /**
     * 在线性表的第i个数据元素之前插入一个值为x的数据元素。当i=0时表示在表头插入一个数据元素x,当i=length()时表示在表尾插入一个数据元素x
     *
     * @param i 索引
     * @param x 元素
     * @throws Exception i < 0 || i > length()
     */
    @Override
    public void insert(int i, Object x) throws Exception {
        // 初始化p为头结点,j为计数器
        Node p = head;
        // 第i个结点前驱的位置
        int j = -1;
        // 寻找i个结点的前驱
        while (p != null && j < i - 1) {
            p = p.next;
            ++j;// 计数器的值增1
        }
        // i不合法
        if (j > i - 1 || p == null) {
            // 输出异常
            throw new Exception("插入位置不合理");
        }
        // 生成新结点
        Node s = new Node(x);
        // 插入单链表中
        s.next = p.next;
        p.next = s;
    }

    /**
     * 将线性表中第i个数据元素删除
     *
     * @param i 索引
     * @throws Exception i < 0 || i >= length()-1
     */
    @Override
    public void remove(int i) throws Exception {
        // p指向要删除结点的前驱结点
        Node p = head;
        int j = -1;
        // 寻找i个结点的前驱
        while (p.next != null && j < i - 1) {
            p = p.next;
            ++j;// 计数器的值增1
        }
        // i小于0或者大于表长减1
        if (j > i - 1 || p.next == null) {
            // 输出异常
            throw new Exception("删除位置不合理");
        }
        // 删除结点
        p.next = p.next.next;
    }

    /**
     * 返回线性表中首次出现指定元素x的索引，如果列表不包含此元素，则返回 -1
     *
     * @param x 元素
     * @return 线性表中首次出现指定元素x的索引
     */
    @Override
    public int indexOf(Object x) {
        // 初始化,p指向首结点,j为计数器
        Node p = head.next;
        int j = 0;
        // 从单链表中的首结点元素开始查找，直到p.data指向元素x或到达单链表的表尾
        while (p != null && !p.data.equals(x)) {
            // 指向下一个元素
            p = p.next;
            ++j;// 计数器的值增1
        }
        // 如果p指向表中的某一元素
        if (p != null) {
            // 返回x元素在顺序表中的位置
            return j;
        } else {
            // x元素不在顺序表中
            return -1;
        }
    }


    /**
     * 输出线性表中的数据元素
     */
    @Override
    public void display() {
        // 取出带头结点的单链表中的首结点元素
        Node node = head.next;
        while (node != null) {
            // 输出数据元素的值
            System.out.print(node.data + " ");
            // 取下一个结点
            node = node.next;
        }
        System.out.println();
    }

    /**
     * 在非递减的有序单链表中插入一个值为x的数据元素，并使单链表仍保持有序的操作
     *
     * @param x 元素
     */
    public void insert(int x) {
        Node p = head.next;
        Node q = head;
        int temp;
        while (p != null) {
            temp = (Integer) p.data;
            if (temp < x) {
                q = p;
                p = p.next;
            } else {
                break;
            }
        }
        // 生成新结点
        Node s = new Node(x);
        // 将s结点插入到单链表的q结点与p结点之间
        s.next = p;
        q.next = s;
    }

    /**
     * 在非递减的有序单链表中插入一个值为x的数据元素，并使单链表仍保持有序的操作
     *
     * @param x 元素
     */
    public void insert1(int x) {
        Node p = head.next;

        while (p.next != null && (Integer) p.next.data < x) {
            p = p.next;
        }
        // 生成新结点
        Node s = new Node(x);
        // 将s结点插入到单链表的q结点与p结点之间
        s.next = p.next;
        p.next = s;
    }

    /**
     * 实现对单链表就地逆置(采用的是头插法)
     */
    public void reverse() {
        Node p = head.next;
        head.next = null;
        Node q;
        while (p != null) {
            q = p.next;
            p.next = head.next;
            head.next = p;
            p = q;
        }
    }

    /**
     * 实现删除单链表中数据域值等于x的所有结点的操作，并返回被删除结点的个数
     * @param x 元素
     * @return 被删除结点的个数
     */
    public int removeAll(Object x) {
        // 初始化,p指向首结点,j为计数器
        Node p = head.next;
        // 用来记录p的前驱结点
        Node q = head;
        // 用来记录被删除结点的个数
        int j = 0;
        // 从单链表中的首结点开始对整个链表遍历一次
        while (p != null) {
            if ((p.data).equals(x)) {
                q.next = p.next;
                // 计数器的值增1
                ++j;
            } else {
                q = p;
            }
            // 指向下一个元素
            p = p.next;
        }
        // 返回被删除结点的个数
        return j;
    }
}
