package gdlgxy.algorithm.ch06;

/**
 * 图的接口
 *
 * @author gdlgxy
 */
public interface IGraph {
    /**
     * 创建一个图
     */
    void createGraph();

    /**
     * 返回顶点数
     *
     * @return 顶点数
     */
    int getVexNum();

    /**
     * 返回边数
     *
     * @return 边数
     */
    int getArcNum();

    /**
     * 返回结点v表示的值，
     *
     * @param v 结点
     * @return v表示结点的值
     * @throws Exception 如果v < 0 ||  v >= vexNum
     */
    Object getVex(int v) throws Exception;

    /**
     * 给定顶点的值vex，返回其在图中的位置
     *
     * @param vex 顶点值
     * @return 顶点在图中的位置，如果图中不包含此顶点，则返回-1
     */
    int locateVex(Object vex);

    /**
     * 返回顶点v的第一个邻接点，若v没有邻接点，则返回-1
     *
     * @param v 顶点
     * @return 顶点v的第一个邻接点，若v没有邻接点，则返回-1
     * @throws Exception 如果v < 0 ||  v >= vexNum
     */
    int firstAdjVex(int v) throws Exception;

    /**
     * 返回顶点v相对于顶点w的下一个邻接点，若w是v的最后一个邻接点，则返回-1
     *
     * @param v 顶点
     * @param w 相对于的顶点
     * @return 顶点v相对于顶点w的下一个邻接点，若w是v的最后一个邻接点，则返回-1
     * @throws Exception 如果v < 0 ||  v >= vexNum
     */
    int nextAdjVex(int v, int w) throws Exception;
}
