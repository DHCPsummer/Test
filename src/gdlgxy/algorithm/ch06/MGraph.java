package gdlgxy.algorithm.ch06;

import java.util.Scanner;

/**
 * 用邻接矩阵类实现的图
 *
 * @author gdlgxy
 */
public class MGraph implements IGraph {
    /**
     * 无穷大 Integer.MAX_VALUE
     */
    public final static int INFINITY = Integer.MAX_VALUE;
    /**
     * 图的种类
     */
    private GraphKind kind;
    /**
     * 图的顶点数
     */
    private int vexNum;
    /**
     * 图的边数
     */
    private int arcNum;
    /**
     * 存储顶点的数组
     */
    private Object[] vexs;
    /**
     * 邻接矩阵
     */
    private int[][] arcs;

    /**
     * 无参构造图
     */
    public MGraph() {
        this(null, 0, 0, null, null);
    }

    /**
     * 有参构造函数
     *
     * @param kind   图的种类
     * @param vexNum 图的顶点数
     * @param arcNum 图的边数
     * @param vexs   用来存储顶点的数组
     * @param arcs   邻接矩阵
     */
    public MGraph(GraphKind kind, int vexNum, int arcNum, Object[] vexs, int[][] arcs) {
        this.kind = kind;
        this.vexNum = vexNum;
        this.arcNum = arcNum;
        this.vexs = vexs;
        this.arcs = arcs;
    }

    /**
     * 创建图
     */
    @Override
    public void createGraph() {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入图的种类");
        GraphKind kind = GraphKind.valueOf(sc.next());
        switch (kind) {
            case UDG:
                // 构造无向图
                createUDG();
                break;
            case DG:
                // 构造有向图
                createDG();
                break;
            case UDN:
                // 构造无向网
                createUDN();
                break;
            case DN:
                // 构造有向网
                createDN();
                break;
            default:
                break;
        }
    }

    /**
     * 构造无向图算法
     */
    private void createUDG() {
        Scanner sc = new Scanner(System.in);
        initializationGraph();
        System.out.println("请输入各个边的两个顶点");
        for (int k = 0; k < arcNum; k++) {
            int v = locateVex(sc.next());
            int u = locateVex(sc.next());
            arcs[v][u] = arcs[u][v] = 1;
        }
    }

    /**
     * 构造有向图算法
     */
    private void createDG() {
        Scanner sc = new Scanner(System.in);
        initializationGraph();
        System.out.println("请输入各个边的两个顶点");
        for (int k = 0; k < arcNum; k++) {
            int v = locateVex(sc.next());
            int u = locateVex(sc.next());
            arcs[v][u] = 1;
        }
    }

    /**
     * 无向图和有向图的初始化
     */
    private void initializationGraph(){
        Scanner sc = new Scanner(System.in);
        System.out.println("请分别输入图的顶点数、图的边数");
        vexNum = sc.nextInt();
        arcNum = sc.nextInt();
        vexs = new Object[vexNum];
        System.out.println("请分别输入图的各个顶点");
        // 构造顶点向量
        for (int v = 0; v < vexNum; v++) {
            vexs[v] = sc.next();
        }
        arcs = new int[vexNum][vexNum];
        // 初始化邻接矩阵
        for (int v = 0; v < vexNum; v++) {
            for (int u = 0; u < vexNum; u++) {
                arcs[v][u] = 0;
            }
        }

    }

    /**
     * 无向网的创建算法
     */
    private void createUDN() {
        Scanner sc = new Scanner(System.in);
        System.out.println("请分别输入图的顶点数、图的边数");
        vexNum = sc.nextInt();
        arcNum = sc.nextInt();
        vexs = new Object[vexNum];
        System.out.println("请分别输入图的各个顶点");
        // 构造顶点向量
        for (int v = 0; v < vexNum; v++) {
            vexs[v] = sc.next();
        }
        arcs = new int[vexNum][vexNum];
        // 初始化邻接矩阵
        for (int v = 0; v < vexNum; v++) {
            for (int u = 0; u < vexNum; u++) {
                arcs[v][u] = INFINITY;
            }
        }
        System.out.println("请输入各个边的两个顶点及其权值");
        for (int k = 0; k < arcNum; k++) {
            int v = locateVex(sc.next());
            int u = locateVex(sc.next());
            arcs[v][u] = arcs[u][v] = sc.nextInt();
        }
    }

    /**
     * 有向网的创建算法
     */
    private void createDN() {
        Scanner sc = new Scanner(System.in);
        System.out.println("请分别输入图的顶点数、图的边数");
        vexNum = sc.nextInt();
        arcNum = sc.nextInt();
        vexs = new Object[vexNum];
        System.out.println("请分别输入图的各个顶点");
        // 构造顶点向量
        for (int v = 0; v < vexNum; v++) {
            vexs[v] = sc.next();
        }
        arcs = new int[vexNum][vexNum];
        // 初始化邻接矩阵
        for (int v = 0; v < vexNum; v++) {
            for (int u = 0; u < vexNum; u++) {
                arcs[v][u] = INFINITY;
            }
        }

        System.out.println("请输入各个边的两个顶点及其权值");
        for (int k = 0; k < arcNum; k++) {
            int v = locateVex(sc.next());
            int u = locateVex(sc.next());
            arcs[v][u] = sc.nextInt();
        }
    }

    /**
     * 返回顶点数
     *
     * @return 顶点数
     */
    @Override
    public int getVexNum() {
        return vexNum;
    }

    /**
     * 设置顶点数
     *
     * @param vexNum 顶点数
     */
    public void setVexNum(int vexNum) {
        this.vexNum = vexNum;
    }

    /**
     * 返回边数
     *
     * @return 边数
     */
    @Override
    public int getArcNum() {
        return arcNum;
    }

    /**
     * 设置边的数目
     *
     * @param arcNum 边的数目
     */
    public void setArcNum(int arcNum) {
        this.arcNum = arcNum;
    }

    /**
     * 给定顶点的值vex，返回其在图中的位置
     *
     * @param vex 顶点值
     * @return 顶点在图中的位置，如果图中不包含此顶点，则返回-1
     */
    @Override
    public int locateVex(Object vex) {
        for (int v = 0; v < vexNum; v++) {
            if (vexs[v].equals(vex)) {
                return v;
            }
        }
        return -1;
    }

    /**
     * 返回结点v表示的值
     *
     * @param v 结点
     * @return v表示结点的值
     * @throws Exception 如果v < 0 ||  v >= vexNum
     */
    @Override
    public Object getVex(int v) throws Exception {
        if (v < 0 && v >= vexNum) {
            throw new Exception("第" + v + "个顶点不存在!");
        }
        return vexs[v];
    }

    /**
     * 返回顶点v的第一个邻接点，若v没有邻接点，则返回-1
     *
     * @param v 顶点
     * @return 顶点v的第一个邻接点，若v没有邻接点，则返回-1
     * @throws Exception 如果v < 0 ||  v >= vexNum
     */
    @Override
    public int firstAdjVex(int v) throws Exception {
        if (v < 0 && v >= vexNum) {
            throw new Exception("第" + v + "个顶点不存在!");
        }
        for (int j = 0; j < vexNum; j++) {
            if (arcs[v][j] != 0 && arcs[v][j] < INFINITY) {
                return j;
            }
        }
        return -1;
    }

    /**
     * 返回顶点v相对于顶点w的下一个邻接点，若w是v的最后一个邻接点，则返回-1
     *
     * @param v 顶点
     * @param w 相对于的顶点
     * @return 顶点v相对于顶点w的下一个邻接点，若w是v的最后一个邻接点，则返回-1
     * @throws Exception 如果v < 0 ||  v >= vexNum
     */
    @Override
    public int nextAdjVex(int v, int w) throws Exception {
        if (v < 0 && v >= vexNum) {
            throw new Exception("第" + v + "个顶点不存在!");
        }

        for (int j = w + 1; j < vexNum; j++) {
            if (arcs[v][j] != 0 && arcs[v][j] < INFINITY) {
                return j;
            }
        }
        return -1;
    }

    /**
     * 返回图的种类
     *
     * @return 图的种类
     */
    public GraphKind getKind() {
        return kind;
    }

    /**
     * 设置图的种类
     *
     * @param kind 图的种类
     */
    public void setKind(GraphKind kind) {
        this.kind = kind;
    }

    /**
     * 返回图的邻接矩阵
     *
     * @return 图的邻接矩阵
     */
    public int[][] getArcs() {
        return arcs;
    }

    /**
     * 设置图的邻接矩阵
     *
     * @param arcs 图的邻接矩阵
     */
    public void setArcs(int[][] arcs) {
        this.arcs = arcs;
    }

    /**
     * 返回存储顶点的数组
     *
     * @return 存储顶点的数组
     */
    public Object[] getVexs() {
        return vexs;
    }

    /**
     * 设置存储顶点的数组
     *
     * @param vexs 存储顶点的数组
     */
    public void setVexs(Object[] vexs) {
        this.vexs = vexs;
    }

}
