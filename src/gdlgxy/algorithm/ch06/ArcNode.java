package gdlgxy.algorithm.ch06;

/**
 * 图的邻接表存储表示中的边（或弧）结点类
 *
 * @author gdlgxy
 */
public class ArcNode {
    /**
     * 该弧所指向的顶点位置
     */
    public int adjVex;
    /**
     * 边（或弧）的权值
     */
    public int value;
    /**
     * 指向下一条弧
     */
    public ArcNode nextArc;

    /**
     * 构造边的结点类，保存的边没有权值，没有指向任何结点。没有指向任何边
     */
    public ArcNode() {
        this(-1, 0, null);
    }
    /**
     * 构造边的结点类
     *
     * @param adjVex  该弧所指向的顶点位置
     */
    public ArcNode(int adjVex) {
        this(adjVex, 0, null);
    }
    /**
     * 构造边的结点类
     *
     * @param adjVex  该弧所指向的顶点位置
     * @param value   边的权值
     */
    public ArcNode(int adjVex, int value) {
        this(adjVex, value, null);
    }

    /**
     * 构造边的结点类
     *
     * @param adjVex  该弧所指向的顶点位置
     * @param value   边的权值
     * @param nextArc 指向下一条边
     */
    public ArcNode(int adjVex, int value, ArcNode nextArc) {
        this.value = value;
        this.adjVex = adjVex;
        this.nextArc = nextArc;
    }

}
