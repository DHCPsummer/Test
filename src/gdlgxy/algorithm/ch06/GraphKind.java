package gdlgxy.algorithm.ch06;


/**
 * 图的种类 有向图、有向网、无向图、无向网
 *
 * @author gdlgxy
 */
public enum GraphKind {
    /**
     * 无向图(UnDirected Graph)
     */
    UDG,
    /**
     * 有向图(Directed Graph)
     */
    DG,
    /**
     * 无向网(UnDirected Network)
     */
    UDN,
    /**
     * 有向网(Directed Network)
     */
    DN;
}
