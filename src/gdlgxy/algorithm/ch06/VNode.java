package gdlgxy.algorithm.ch06;

/**
 * 图的邻接表存储表示中的顶点结点类
 *
 * @author gdlgxy
 */
public class VNode {
    /**
     * 顶点信息
     */
    public Object data;
    /**
     * 指向第一条依附于该顶点的边
     */
    public ArcNode firstArc;

    /**
     * 无参构造器
     */
    public VNode() {
        this(null, null);
    }

    /**
     * 构造一个顶点信息为data，顶点下一条边为null
     *
     * @param data 顶点信息
     */
    public VNode(Object data) {
        this(data, null);
    }

    /**
     * 构造一个顶点信息为data，顶点下一条边为firstArc
     *
     * @param data     顶点信息
     * @param firstArc 下一条边
     */
    public VNode(Object data, ArcNode firstArc) {
        this.data = data;
        this.firstArc = firstArc;
    }
}
