package gdlgxy.algorithm.ch05;

/**
 * 二叉链式存储结构下的二叉树结点
 *
 * @author gdlgxy
 */
public class BiTreeNode {
    /**
     * 结点的数据元素
     */
    public Object data;
    /**
     * 左孩子
     */
    public BiTreeNode leftChild;
    /**
     * 右孩子
     */
    public BiTreeNode rightChild;

    /**
     * 构造一个空结点
     */
    public BiTreeNode() {
        this(null);
    }

    /**
     * 以data构造一棵左孩子和右孩子为空结点
     * @param data 节点的值
     */
    public BiTreeNode(Object data) {
        this(data, null, null);
    }

    public BiTreeNode(Object data, BiTreeNode leftChild, BiTreeNode rightChild) {// 构造一棵数据元素和左右孩子都不为空的结点
        this.data = data;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }


}
