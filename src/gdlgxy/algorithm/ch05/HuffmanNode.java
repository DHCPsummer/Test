package gdlgxy.algorithm.ch05;

/**
 * 赫夫曼树的结点类
 *
 * @author gdlgxy
 */
public class HuffmanNode {
    /**
     * 结点的权值
     */
    public int weight;
    /**
     * 加入赫夫曼树的标志，flag=0时表示该结点未加入哈夫曼树，flag=1时则表示该结点已加入哈夫曼树
     */
    public int flag;
    /**
     * 父结点
     */
    public HuffmanNode parent;
    /**
     * 左孩子结点
     */
    public HuffmanNode lChild;
    /**
     * 右孩子结点
     */
    public HuffmanNode rChild;

    public HuffmanNode() {// 构造一个空结点
        this(0);
    }

    /**
     * 构造一个具有权值weight的结点
     *
     * @param weight 节点权值
     */
    public HuffmanNode(int weight) {
        this.weight = weight;
        flag = 0;
        parent = lChild = rChild = null;
    }

}
