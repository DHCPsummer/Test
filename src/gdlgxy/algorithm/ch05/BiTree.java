package gdlgxy.algorithm.ch05;

import gdlgxy.algorithm.ch03.LinkQueue;
import gdlgxy.algorithm.ch03.LinkStack;

/**
 * 二叉链式存储结构下的二叉树
 *
 * @author gdlgxy
 */
public class BiTree {
    /**
     * 用于记录preStr的索引值
     */
    private static int index = 0;
    /**
     * 树的根结点
     */
    private BiTreeNode root;

    /**
     * 构造一棵空树
     */
    public BiTree() {
        this.root = null;
    }

    /**
     * 用根结点root构造一棵树
     *
     * @param root 根结点
     */
    public BiTree(BiTreeNode root) {
        this.root = root;
    }

    /**
     * 由先根遍历的数组和中根遍历的数组建立一棵二叉树
     *
     * @param preOrder 整棵树的先根遍历
     * @param inOrder  整棵树的中根遍历
     * @param preIndex 先根遍历从preOrder字符串中的开始位置
     * @param inIndex  中根遍历从字符串inOrder中的开始位置
     * @param count    树结点的个数
     */
    public BiTree(String preOrder, String inOrder, int preIndex, int inIndex, int count) {
        // 先根和中根非空
        if (count > 0) {
            // 取先根字符串中的第一个元素作为根结点
            char r = preOrder.charAt(preIndex);
            int i = 0;
            // 寻找根结点在中根遍历字符串中的索引
            for (; i < count; i++) {
                if (r == inOrder.charAt(i + inIndex)) {
                    break;
                }
            }
            // 建立树的根结点
            root = new BiTreeNode(r);
            // 建立树的左子树
            root.leftChild = new BiTree(preOrder, inOrder, preIndex + 1, inIndex, i).root;
            // 建立树的右子树
            root.rightChild = new BiTree(preOrder, inOrder, preIndex + i + 1, inIndex + i + 1, count - i - 1).root;
        }
    }

    public BiTree(String preStr) {
        // 取出字符串索引为index的字符，且index增1
        char c = preStr.charAt(index++);
        // 字符不为#
        char blankNodes = '#';
        if (c != blankNodes) {
            // 建立树的根结点
            root = new BiTreeNode(c);
            // 建立树的左子树
            root.leftChild = new BiTree(preStr).root;
            // 建立树的右子树
            root.rightChild = new BiTree(preStr).root;
        } else {
            root = null;
        }
    }

    /**
     * 先根遍历二叉树基本操作的递归算法
     *
     * @param root 根节点
     */
    public void preRootTraverse(BiTreeNode root) {
        if (root != null) {
            // 访问根结点
            System.out.print(root.data);
            // 访问左子树
            preRootTraverse(root.leftChild);
            // 访问右子树
            preRootTraverse(root.rightChild);
        }
    }

    /**
     * 先根遍历二叉树基本操作的非递归算法
     */
    public void preRootTraverse() {
        BiTreeNode root = this.root;
        if (root != null) {
            // 构造栈
            LinkStack stack = new LinkStack();
            // 根结点入栈
            stack.push(root);
            while (!stack.isEmpty()) {
                // 移除栈顶结点，并返回其值
                root = (BiTreeNode) stack.pop();
                // 访问结点
                System.out.print(root.data);
                while (root != null) {
                    // 访问左孩子
                    if (root.leftChild != null) {
                        System.out.print(root.leftChild.data);
                    }
                    // 右孩子非空入栈
                    if (root.rightChild != null) {
                        stack.push(root.rightChild);
                    }
                    root = root.leftChild;
                }
            }
        }
    }

    /**
     * 中根遍历二叉树基本操作的递归算法
     *
     * @param root 根结点
     */
    public void inRootTraverse(BiTreeNode root) {
        if (root != null) {
            // 访问左子树
            inRootTraverse(root.leftChild);
            // 访问根结点
            System.out.print(root.data);
            // 访问右子树
            inRootTraverse(root.rightChild);
        }
    }

    /**
     * 中根遍历二叉树基本操作的非递归算法
     */
    public void inRootTraverse() {
        BiTreeNode root = this.root;
        if (root != null) {
            // 构造链栈
            LinkStack stack = new LinkStack();
            // 根结点入栈
            stack.push(root);
            while (!stack.isEmpty()) {
                while (stack.peek() != null) {
                    // 将栈顶结点的所有左孩子结点入栈
                    stack.push(((BiTreeNode) stack.peek()).leftChild);
                }
                // 空结点退栈
                stack.pop();
                if (!stack.isEmpty()) {
                    // 移除栈顶结点，并返回其值
                    root = (BiTreeNode) stack.pop();
                    // 访问结点
                    System.out.print(root.data);
                    // 结点的右孩子入栈
                    stack.push(root.rightChild);
                }
            }
        }
    }

    /**
     * 后根遍历二叉树基本操作的递归算法
     *
     * @param root 根结点
     */
    public void postRootTraverse(BiTreeNode root) {
        if (root != null) {
            // 访问左子树
            postRootTraverse(root.leftChild);
            // 访问右子树
            postRootTraverse(root.rightChild);
            // 访问根结点
            System.out.print(root.data);
        }
    }

    /**
     * 后根遍历二叉树基本操作的非递归算法
     */
    public void postRootTraverse() {
        BiTreeNode root = this.root;
        if (root != null) {
            // 构造链栈
            LinkStack stack = new LinkStack();
            // 根结点进栈
            stack.push(root);
            // 访问标记
            boolean flag;
            // p指向刚被访问的结点
            BiTreeNode p = null;
            while (!stack.isEmpty()) {
                while (stack.peek() != null) {
                    // 将栈顶结点的所有左孩子结点入栈
                    stack.push(((BiTreeNode) stack.peek()).leftChild);
                }
                // 空结点退栈
                stack.pop();
                while (!stack.isEmpty()) {
                    // 查看栈顶元素
                    root = (BiTreeNode) stack.peek();
                    if (root.rightChild == null || root.rightChild == p) {
                        // 访问结点
                        System.out.print(root.data);
                        // 移除栈顶元素
                        stack.pop();
                        // p指向刚被访问的结点
                        p = root;
                        // 设置访问标记
                        flag = true;
                    } else {
                        // 右孩子结点入栈
                        stack.push(root.rightChild);
                        flag = false;
                    }
                    if (!flag) {
                        break;
                    }
                }
            }
        }
    }

    /**
     * 层次遍历二叉树基本操作的算法(自左向右)
     */
    public void levelTraverse() {
        BiTreeNode root = this.root;
        if (root != null) {
            // 构造队列
            LinkQueue queue = new LinkQueue();
            // 根结点入队列
            queue.offer(root);
            while (!queue.isEmpty()) {
                root = (BiTreeNode) queue.poll();
                // 访问结点
                System.out.print(root.data);
                // 左孩子非空，入队列
                if (root.leftChild != null) {
                    queue.offer(root.leftChild);
                }
                // 右孩子非空，入队列
                if (root.rightChild != null) {
                    queue.offer(root.rightChild);
                }
            }
        }
    }

    /**
     * 获得根节点
     *
     * @return 返回根结点
     */
    public BiTreeNode getRoot() {
        return root;
    }

    /**
     * 设置根结点root
     *
     * @param root 根结点
     */
    public void setRoot(BiTreeNode root) {
        this.root = root;
    }

    /**
     * 统计以根结点root为树的叶结点数目
     *
     * @param root 根结点
     * @return 叶结点数目
     */
    public int countLeafNode(BiTreeNode root) {
        int count = 0;
        if (root != null) {
            if (root.leftChild == null && root.rightChild == null) {
                ++count;// 叶结点个数加1
            } else {
                // 加上左子树上叶结点的个数
                count += countLeafNode(root.leftChild);
                // 加上右子树上叶结点的个数
                count += countLeafNode(root.rightChild);
            }
        }
        return count;
    }

    /**
     * 统计以根结点root为树的结点的数目
     *
     * @param root 根结点
     * @return 结点的数目
     */
    public int countNode(BiTreeNode root) {
        int count = 0;
        if (root != null) {
            // 结点的个数加1
            ++count;
            // 加上左子树上结点的个数
            count += countNode(root.leftChild);
            // 加上右子树上结点的个数
            count += countNode(root.rightChild);
        }
        return count;
    }

    /**
     * 返回以根节点root为树的深度
     *
     * @param root 根结点
     * @return 以根节点root为树的深度
     */
    public int getDepth(BiTreeNode root) {
        if (root != null) {
            int leftDepth = getDepth(root.leftChild);
            int rightDepth = getDepth(root.rightChild);
            return 1 + (Math.max(leftDepth, rightDepth));
        }
        return 0;
    }
}