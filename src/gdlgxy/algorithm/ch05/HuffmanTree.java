package gdlgxy.algorithm.ch05;

/**
 * 赫夫曼树及其操作
 *
 * @author gdlgxy
 */
public class HuffmanTree {
    public static void main(String[] args) {
        // 初始化权值
        int[] w = {23, 11, 5, 3, 29, 14, 7, 8};
        // 构造赫夫曼树
        HuffmanTree t = new HuffmanTree();
        // 求赫夫曼编码
        int[][] hn = t.huffmanCoding(w);
        System.out.println("赫夫曼编码为：");
        // 输出赫夫曼编码
        for (int i = 0; i < hn.length; i++) {
            System.out.print(w[i] + " ");
            for (int j = 0; j < hn[i].length; j++) {
                // 开始标志符读到数组结尾
                if (hn[i][j] == -1) {
                    for (int k = j + 1; k < hn[i].length; k++) {
                        // 输出
                        System.out.print(hn[i][k]);
                    }
                    break;
                }
            }
            // 输出换行
            System.out.println();
        }
    }
    /*
    运行结果：
    赫夫曼编码为：
    23 01
    11 001
    5 11111
    3 11110
    29 10
    14 110
    7 1110
    8 000
     */

    /**
     * 求赫夫曼编码的算法，数组w存放n个字符的权值(均>0)
     *
     * @param w 存放字符的数组
     * @return 数组每个字符的赫夫曼编码
     */
    public int[][] huffmanCoding(int[] w) {
        // 字符个数
        int n = w.length;
        // 赫夫曼树的结点数
        int m = 2 * n - 1;
        HuffmanNode[] hn = new HuffmanNode[m];
        int i;
        // 构造n个具有权值的结点
        for (i = 0; i < n; i++) {
            hn[i] = new HuffmanNode(w[i]);
        }
        // 建赫夫曼树
        for (i = n; i < m; i++) {
            // 在HN[0..i - 1]选择不在赫夫曼树中且weight最小的两个结点min1和min2
            HuffmanNode min1 = selectMin(hn, i - 1);
            min1.flag = 1;
            HuffmanNode min2 = selectMin(hn, i - 1);
            min2.flag = 1;
            // 构造min1和min2的父结点，并修改且父结点的权值
            hn[i] = new HuffmanNode();
            min1.parent = hn[i];
            min2.parent = hn[i];
            hn[i].lChild = min1;
            hn[i].rChild = min2;
            hn[i].weight = min1.weight + min2.weight;
        }

        // 从叶子到根逆向求每个字符的赫夫曼编码
        // 分配n个字符编码存储空间
        int[][] huffCode = new int[n][n];
        for (int j = 0; j < n; j++) {
            // 编码的开始位置，初始化为数组的结尾
            int start = n - 1;
            // 从叶子到根逆向求编码
            for (HuffmanNode c = hn[j], p = c.parent; p != null; c = p, p = p.parent) {
                if (p.lChild.equals(c)) {
                    // 左孩子编码为0
                    huffCode[j][start--] = 0;
                } else {
                    // 右孩子编码为1
                    huffCode[j][start--] = 1;
                }
            }
            // 编码的开始标志为 -1，编码是-1之后的0、1的序列
            huffCode[j][start] = -1;
        }
        return huffCode;
    }

    /**
     * 在HN[0..i - 1]选择不在赫夫曼树中且weight最小的结点
     *
     * @param hn  包含树节点的数组
     * @param end 数组末尾长度
     * @return 不在赫夫曼树中且weight最小的结点
     */
    private HuffmanNode selectMin(HuffmanNode[] hn, int end) {
        HuffmanNode min = hn[end];
        for (int i = 0; i <= end; i++) {
            HuffmanNode h = hn[i];
            // 不在赫夫曼树中且weight最小的结点
            if (h.flag == 0 && h.weight < min.weight) {
                min = h;
            }
        }
        return min;
    }
}