package gdlgxy.algorithm.ch07;

/**
 * 顺序表记录结点类
 *
 * @author gdlgxy
 */
public class RecordNode {
    /**
     * 关键字
     */
    public Comparable key;
    /**
     * 数据元素
     */
    public Object element;

    public RecordNode() {
    }

	/**
	 * 构造方法1
	 * @param key 节点的关键字
	 */
    public RecordNode(Comparable key) {
        this.key = key;
    }

	/**
	 *  构造方法2
	 * @param key 节点关键字
	 * @param element 节点数据元素
	 */
    public RecordNode(Comparable key, Object element) {
        this.key = key;
        this.element = element;
    }

    @Override
	public String toString() { // 覆盖toString()方法
        return "[" + key + "," + element + "]";
    }
}
