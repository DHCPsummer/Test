package gdlgxy.algorithm.ch07;

/**
 * 第7章 顺序表类
 *
 * @author gdlgxy
 */
public class SeqList {
    /**
     * 顺序表记录结点数组
     */
    public RecordNode[] r;
    /**
     * 顺序表长度,即记录个数
     */
    public int curLen;

    public SeqList() {
    }

    /**
     * 顺序表的构造方法，构造一个存储空间容量为maxSize的顺序表
     *
     * @param maxSize 容量
     */
    public SeqList(int maxSize) {
        // 为顺序表分配maxSize个存储单元
        this.r = new RecordNode[maxSize];
        // 置顺序表的当前长度为0
        this.curLen = 0;
    }

    public RecordNode[] getRecord() {
        return r;
    }

    public void setRecord(RecordNode[] r) {
        this.r = r;
    }

    /**
     * 求顺序表中的数据元素个数并由函数返回其值
     *
     * @return 顺序表中的数据元素个数
     */
    public int length() {
        return curLen;
    }

    /**
     * 在当前顺序表的第i个结点之前插入一个RecordNode类型的结点x<br/>
     * 其中i取值范围为：0 ≤ i ≤ length()<br/>
     * 如果i值不在此范围则抛出异常,当i=0时表示在表头插入一个数据元素x<br/>
     * 当i=length()时表示在表尾插入一个数据元素x
     *
     * @param i 要插入结点的索引
     * @param x 结点
     * @throws Exception 如果 i < 0 || i > length()
     */
    public void insert(int i, RecordNode x) throws Exception {
        // 判断顺序表是否已满
        if (curLen == r.length) {
            throw new Exception("顺序表已满");
        }
        // i小于0或者大于表长
        if (i < 0 || i > curLen) {
            throw new Exception("插入位置不合理");
        }
        // 插入位置及之后的元素后移
        System.arraycopy(r, i, r, i + 1, curLen - i);
        // 插入x
        r[i] = x;
        // 表长度增1
        this.curLen++;
    }

    /**
     * 输出数组元素
     */
    public void display() {
        for (int i = 0; i < this.curLen; i++) {
            System.out.print(" " + r[i].key.toString());
        }
        System.out.println();
    }

    /**
     * 输出数组元素
     *
     * @param sortMode
     */
    public void display(int sortMode) {
        int i;
        //带监视哨的直接插入排序方法，0号单元用于存放监视哨
        if (sortMode == 9) {
            i = 1;
        } else {
            i = 0;
        }
        for (; i < this.curLen; i++) {
            System.out.print(" " + r[i].key.toString());
        }
        System.out.println();
    }


    /**
     * 【算法7.1】 不带监视哨的直接插入排序算法
     */
    public void insertSort() {
        RecordNode temp;
        int i;
        int j;
        // n-1趟扫描
        System.out.println("不带监视哨的直接插入排序算法");
        for (i = 1; i < this.curLen; i++) {
            // 将待插入的第i条记录暂存在temp中
            temp = r[i];
            // 将前面比r[i]大的记录向后移动
            for (j = i - 1; j >= 0 && temp.key.compareTo(r[j].key) < 0; j--) {
                r[j + 1] = r[j];
            }
            // r[i]插入到第j+1个位置
            r[j + 1] = temp;
            //        System.out.println("第" + i + "趟: ");
            //      display();
        }
    }

    /**
     * 【算法7.2】带监视哨的直接插入排序算法
     */
    public void insertSortWithGuard() {
        int i;
        int j;
        System.out.println("带监视哨的直接插入排序");
        // n-1趟扫描
        for (i = 2; i < this.curLen; i++) {
            // 将待插入的第i条记录暂存在r[0]中，同时r[0]为监视哨
            r[0] = r[i];
            // 将前面较大元素向后移动
            for (j = i - 1; r[0].key.compareTo(r[j].key) < 0; j--) {
                r[j + 1] = r[j];
            }
            // r[i]插入到第j+1个位置
            r[j + 1] = r[0];
            //    System.out.println("第" + i + "趟: ");
            //    display();
        }
    }

    /**
     * 【算法7.3】希尔排序算法
     *
     * @param d 为增量数组
     */
    public void shellSort(int[] d) {
        RecordNode temp;
        int i;
        int j;
        System.out.println("希尔排序");
        //控制增量，增量减半，若干趟扫描
        for (int dk : d) {
            //一趟中若干子表，每个记录在自己所属子表内进行直接插入排序
            for (i = dk; i < this.curLen; i++) {
                temp = r[i];
                for (j = i - dk; j >= 0 && temp.key.compareTo(r[j].key) < 0; j -= dk) {
                    r[j + dk] = r[j];
                }
                r[j + dk] = temp;
            }
            System.out.print("增量dk=" + dk + "  ");
            display();
        }
    }

    /**
     * 【算法7.4】 冒泡排序算法
     */
    public void bubbleSort() {
        //    System.out.println("冒泡排序");
        // 辅助结点
        RecordNode temp;
        // 是否交换的标记
        boolean flag = true;
        // 有交换时再进行下一趟，最多n-1趟
        for (int i = 1; i < this.curLen && flag; i++) {
            // 假定元素未交换
            flag = false;
            // 一次比较、交换
            for (int j = 0; j < this.curLen - i; j++) {
                // 逆序时，交换
                if (r[j].key.compareTo(r[j + 1].key) > 0) {
                    temp = r[j];
                    r[j] = r[j + 1];
                    r[j + 1] = temp;
                    flag = true;
                }
            }
            //   System.out.print("第" + i + "趟: ");
            //   display();
        }
    }

    /**
     * 【算法7.5】一趟快速排序
     * 交换排序表r[i..j]的记录，使支点记录到位，并返回其所在位置
     * 此时，在支点之前(后)的记录关键字均不大于(小于)它
     *
     * @param i 要排序的数组最左端索引
     * @param j 要排序的数组最右端索引
     * @return 支点其所在位置
     */
    private int Partition(int i, int j) {
        // 第一个记录作为支点记录
        RecordNode pivot = r[i];
        //    System.out.print(i + ".." + j + ",  pivot=" + pivot.key + "  ");
        // 从表的两端交替地向中间扫描
        while (i < j) {
            while (i < j && pivot.key.compareTo(r[j].key) <= 0) {
                j--;
            }
            // 将比支点记录关键字小的记录向前移动
            if (i < j) {
                r[i] = r[j];
                i++;
            }
            while (i < j && pivot.key.compareTo(r[i].key) > 0) {
                i++;
            }
            // 将比支点记录关键字大的记录向后移动
            if (i < j) {
                r[j] = r[i];
                j--;
            }
        }
        // 支点记录到位
        r[i] = pivot;
        //    display();
        // 返回支点位置
        return i;
    }

    /**
     * 【算法7.6】 递归形式的快速排序算法
     * 对子表r[low..high]快速排序
     *
     * @param low  要排序的数组最左端索引
     * @param high 要排序的数组最右端索引
     */
    private void qSort(int low, int high) {
        if (low < high) {
            // 一趟排序，将排序表分为两部分
            int pivotloc = Partition(low, high);
            // 低子表递归排序
            qSort(low, pivotloc - 1);
            // 高子表递归排序
            qSort(pivotloc + 1, high);
        }
    }

    /**
     * 【算法7.7】顺序表快速排序算法
     */
    public void quickSort() {
        qSort(0, this.curLen - 1);
    }

    /**
     * 【算法7.8】直接选择排序
     */
    public void selectSort() {
        //   System.out.println("直接选择排序");
        // 辅助结点
        RecordNode temp;
        // n-1趟排序
        for (int i = 0; i < this.curLen - 1; i++) {
            // 每趟在从r[i]开始的子序列中寻找最小元素
            // 设第i条记录的关键字最小
            int min = i;
            //在子序列中选择关键字最小的记录
            for (int j = i + 1; j < this.curLen; j++) {
                if (r[j].key.compareTo(r[min].key) < 0) {
                    //记住关键字最小记录的下标
                    min = j;
                }
            }
            //将本趟关键字最小的记录与第i条记录交换
            if (min != i) {
                temp = r[i];
                r[i] = r[min];
                r[min] = temp;
            }
            //    System.out.print("第" + (i + 1) + "趟: ");
            //    display();
        }
    }

    /**
     * 【算法7.9】 树形选择排序(锦标赛排序)
     * 建立树的顺序存储数组tree,并对其排序，并将结果返回到r中
     */
    void tournamentSort() {
        // 胜者树结点数组
        TreeNode[] tree;
        // 胜者树的叶子结点数
        int leafSize = 1;
        // 得到胜者树叶子结点(外结点)的个数，该个数必须是2的幂
        while (leafSize < this.curLen) {
            leafSize *= 2;
        }
        // 胜者树的所有节点数
        int TreeSize = 2 * leafSize - 1;
        // 叶子结点(外结点)存放的起始位置
        int loadindex = leafSize - 1;
        tree = new TreeNode[TreeSize];
        int j = 0;
        // 把待排序结点复制到胜者树的叶子结点中
        for (int i = loadindex; i < TreeSize; i++) {
            tree[i] = new TreeNode();
            tree[i].index = i;
            // 复制结点
            if (j < this.curLen) {
                tree[i].active = 1;
                tree[i].data = r[j++];
            } else {
                // 空的外结点
                tree[i].active = 0;
            }
        }
        // 进行初始比较查找关键码最小结点
        int i = loadindex;
        // 产生胜者树
        while (i > 0) {
            j = i;
            // 处理各对比赛者
            while (j < 2 * i) {
                if (tree[j + 1].active == 0 || ((tree[j].data).key.compareTo((tree[j + 1].data).key)) <= 0) {
                    // 左孩子(胜者)赋值给父结点
                    tree[(j - 1) / 2] = tree[j];
                } else {
                    // 右孩子(胜者)赋值给父结点
                    tree[(j - 1) / 2] = tree[j + 1];
                }
                // 下一对比赛者
                j += 2;
            }
            // 处理上层结点
            i = (i - 1) / 2;
        }
        // 处理剩余的n-1个记录
        for (i = 0; i < this.curLen - 1; i++) {
            // 将胜者树的根(最小者)存入数组r
            r[i] = tree[0].data;
            // 该元素相应外结点不再比赛
            tree[tree[0].index].active = 0;
            // 调整胜者树
            updateTree(tree, tree[0].index);
        }
        r[this.curLen - 1] = tree[0].data;
    }

    /**
     * 【算法7.10】树形选择排序的调整算法
     *
     * @param tree 树结点数组
     * @param i    当前最小关键字记录的下标
     */
    private void updateTree(TreeNode[] tree, int i) {
        int j;
        // i为偶数，对手为左结点
        if (i % 2 == 0) {
            tree[(i - 1) / 2] = tree[i - 1];
        } else {
            // i为奇数，对手为右结点
            tree[(i - 1) / 2] = tree[i + 1];
        }
        // 最小元素输出后，其对手上升到父结点
        i = (i - 1) / 2;
        // 直到i==0
        while (i > 0) {
            // i为偶数，对手为左结点
            if (i % 2 == 0) {
                j = i - 1;
            } else {
                // i为奇数，对手为右结点
                j = i + 1;
            }
            // 比赛对手中有一个为空
            if (tree[i].active == 0 || tree[j].active == 0) {
                if (tree[i].active == 1) {
                    // i可参选，i上升到父结点
                    tree[(i - 1) / 2] = tree[i];
                } else {
                    // 否则，j上升到父结点
                    tree[(i - 1) / 2] = tree[j];
                }
            } else {
                //双方都可参选
                //关键码小者上升到父结点
                if ((tree[i].data).key.compareTo((tree[j].data).key) <= 0) {
                    tree[(i - 1) / 2] = tree[i];
                } else {
                    tree[(i - 1) / 2] = tree[j];
                }
            }
            // i上升到父结点
            i = (i - 1) / 2;
        }
    }

    /**
     * 【算法7.11】将以筛选法调整堆算法
     * 将以low为根的子树调整成小顶堆，low、high是序列下界和
     *
     * @param low  序列下界
     * @param high 序列上界
     */
    public void sift(int low, int high) {
        // 子树的根
        int i = low;
        // j为i结点的左孩子
        int j = 2 * i + 1;
        RecordNode temp = r[i];
        // 沿较小值孩子结点向下筛选
        while (j < high) {
            if (j < high - 1 && r[j].key.compareTo(r[j + 1].key) > 0) {
                // 数组元素比较,j为左右孩子的较小者
                j++;
            }
            // 若父母结点值较大
            if (temp.key.compareTo(r[j].key) > 0) {
                // 孩子结点中的较小值上移
                r[i] = r[j];
                i = j;
                j = 2 * i + 1;
            } else {
                // 退出循环
                j = high + 1;
            }
        }
        // 当前子树的原根值调整后的位置
        r[i] = temp;
        //    System.out.print("sift  " + low + ".." + high + "  ");
        //    display();
    }

    /**
     * 【算法7.12】 堆排序算法
     */
    public void heapSort() {
        // System.out.println("堆排序");
        int n = this.curLen;
        RecordNode temp;
        // 创建堆
        for (int i = n / 2 - 1; i >= 0; i--) {
            sift(i, n);
        }
        // 每趟将最小值交换到后面，再调整成堆
        for (int i = n - 1; i > 0; i--) {
            temp = r[0];
            r[0] = r[i];
            r[i] = temp;
            sift(0, i);
        }
    }

    /**
     * 【算法7.13】两个有序序列的归并算法
     * 把r数组中两个相邻的有序表r[h]-r[m]和r[m+1]-r[t]归并为一个有序表order[h]-order[t]
     *
     * @param r     要归并的数组
     * @param order 归并后的辅助数组
     * @param h     待合并表的第一个元素的下标
     * @param m
     * @param t
     */
    public void merge(RecordNode[] r, RecordNode[] order, int h, int m, int t) {
        int i = h;
        int j = m + 1;
        int k = h;
        // 将r中两个相邻子序列归并到order中
        while (i <= m && j <= t) {
            // 较小值复制到order中
            if (r[i].key.compareTo(r[j].key) <= 0) {
                order[k++] = r[i++];
            } else {
                order[k++] = r[j++];
            }
        }
        // 将前一个子序列剩余元素复制到order中
        while (i <= m) {
            order[k++] = r[i++];
        }
        // 将后一个子序列剩余元素复制到order中
        while (j <= t) {
            order[k++] = r[j++];
        }
    }

    /**
     * 【算法7.14】一趟归并算法
     * 把数组r[n]中每个长度为s的有序表两两归并到数组order[n]中
     *
     * @param r     要归并的数组
     * @param order 归并后的辅助数组
     * @param s     为子序列的长度
     * @param n     为排序序列的长度
     */
    public void mergePass(RecordNode[] r, RecordNode[] order, int s, int n) {
        System.out.print("子序列长度s=" + s + "  ");
        // p为每一对待合并表的第一个元素的下标，初值为0
        int p = 0;
        // 两两归并长度均为s的有序表
        while (p + 2 * s - 1 <= n - 1) {
            merge(r, order, p, p + s - 1, p + 2 * s - 1);
            p += 2 * s;
        }
        // 归并最后两个长度不等的有序表
        if (p + s - 1 < n - 1) {
            merge(r, order, p, p + s - 1, n - 1);
        } else {
            // 将剩余的有序表复制到order中
            if (n - p >= 0) {
                System.arraycopy(r, p, order, p, n - p);
            }
        }
    }

    /**
     * 【算法7.15】2-路归并排序算法
     */
    public void mergeSort() {
        System.out.println("归并排序");
        // s为已排序的子序列长度，初值为1
        int s = 1;
        int n = this.curLen;
        // 定义长度为n的辅助数组temp
        RecordNode[] temp = new RecordNode[n];
        while (s < n) {
            // 一趟归并，将r数组中各子序列归并到temp中
            mergePass(r, temp, s, n);
            display();
            // 子序列长度加倍
            s *= 2;
            // 将temp数组中各子序列再归并到r中
            mergePass(temp, r, s, n);
            display();
            s *= 2;
        }
    }

    /**
     * 【算法8.1】顺序查找算法
     * 从顺序表r[0]到r[n-1]的n个元素中顺序查找出关键字为key的记录
     *
     * @param key 关键字
     * @return 若查找成功返回其下标，否则返回-1
     */
    public int seqSearch(Comparable key) {
        int i = 0, n = length();
        while (i < n && r[i].key.compareTo(key) != 0) {
            i++;
        }
        // 查找成功则返回该元素的下标i，否则返回-1
        if (i < n) {
            return i;
        } else {
            return -1;
        }
    }

    /**
     * 【算法8.2】带监视哨的顺序查找算法
     * 从顺序表r[1]到r[n]的n个元素中顺序查找出关键字为key的元素
     *
     * @param key 关键字
     * @return 若查找成功返回其下标，否则返回-1
     */
    public int seqSearchWithGuard(Comparable key) {
        int i = length() - 1;
        // 哨兵
        r[0].key = key;
        while ((r[i].key).compareTo(key) != 0) {
            i--;
        }
        if (i > 0) {
            return i;
        } else {
            return -1;
        }
    }

    /**
     * 【算法8.3】二分查找算法<br/>
     * 数组元素已按升序排列
     *
     * @param key 关键字
     * @return 若查找成功返回元素下标，否则返回-1
     */
    public int binarySearch(Comparable key) {
        if (length() > 0) {
            // 查找范围下界和上界
            int low = 0;
            int high = length() - 1;
            while (low <= high) {
                // 中间位置，当前比较元素位置
                int mid = (low + high) / 2;
                //    System.out.print(r[mid].key + "? ");
                if (r[mid].key.compareTo(key) == 0) {
                    // 查找成功
                    return mid;
                } else if (r[mid].key.compareTo(key) > 0) {
                    // 给定值更小,查找范围缩小到前半段
                    high = mid - 1;
                } else {
                    // 查找范围缩小到后半段
                    low = mid + 1;
                }
            }
        }
        // 查找不成功
        return -1;
    }

}

/**
 * 胜者树的结点类
 */
class TreeNode {
    /**
     * 排序记录结点数据值
     */
    public RecordNode data;
    /**
     * 结点在满二叉树中的序号
     */
    public int index;
    /**
     * 参加选择标志，1表示参选，0表示不参选
     */
    public int active;
/*
    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public RecordNode getData() {
        return data;
    }

    public void setData(RecordNode data) {
        this.data = data;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
*/
}
