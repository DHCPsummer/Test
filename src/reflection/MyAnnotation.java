package reflection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * 自定义注解
 * @author HHH
 */
@Target({TYPE, FIELD, METHOD, PARAMETER, CONSTRUCTOR, LOCAL_VARIABLE,})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    // 看似方法，实际是属性
    // default指定默认值
    String value() default "hello";

//    String[] value();允许使用多个值定义注解
}
