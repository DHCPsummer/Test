package reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author HHH
 */
public class Test {
    public static void main(String[] args) throws Exception {
        getAppointConstructor();
    }

    /**
     * 获取指定属性
     */
    public static void getAppointField() throws Exception {
        Class<?> clazz = Class.forName("reflection.Person");

        // 如果获取的字段不是静态的，要先创建运行时类对象
        Person p = (Person) clazz.getDeclaredConstructor().newInstance();

        // 获取指定属性,要求属性声明为public,该方法不常用
        Field id = clazz.getField("id");

        // 设置当前属性 set(对象名,属性值)
        id.set(p, 101);

        // 获取属性 get(对象名)
        int pId = (int) id.get(p);

        // 获取指定属性,属性可以是非public修饰的
        Field name = clazz.getDeclaredField("name");

        // 保证当前属性允许被访问
        name.setAccessible(true);
        name.set(p, "张三");
        String pName = (String) name.get(p);
        System.out.println(pName);
    }

    /**
     * 指定方法
     */
    public static void getAppointMethod() throws Exception {
        Class<?> clazz = Class.forName("reflection.Person");
        Person p = (Person) clazz.newInstance();
        Method show = clazz.getDeclaredMethod("show", String.class);
        show.setAccessible(true);
        show.invoke(p, "CHN");
        // 如果有返回值 String s = (String) show.invoke(p, "CHN");

        // 调用静态方法
        Method showCute = clazz.getDeclaredMethod("showCute");
        showCute.setAccessible(true);
        showCute.invoke(Person.class);
    }

    /**
     * 指定构造器
     * 一般使用newInstance()调用无参构造器就可以了,除非具体问题需要才调用有参构造器
     * 但是newInstance()在Java9中被弃用了建议使用clazz.getDeclaredConstructor().newInstance()构造空对象
     */
    public static void getAppointConstructor() throws Exception {
        Class<?> clazz = Class.forName("reflection.Person");
        Constructor<?> declaredConstructor = clazz.getDeclaredConstructor(String.class);
        declaredConstructor.setAccessible(true);
        Person p = (Person) declaredConstructor.newInstance("张三");
        System.out.println(p);
    }
}
