package reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 代理的接口
 */
interface UserDao {
    int add(int a, int b);
}

/**
 * 被代理类
 */
class UserDaoImpl implements UserDao {
    @Override
    public int add(int a, int b) {
        return a + b;
    }
}

class UserDaoProxy implements InvocationHandler {
    /**
     * 把创建的是谁的代理对象，把谁传递过来
     * 有参数构造传递
     */
    private Object obj;

    public UserDaoProxy(Object obj) {
        this.obj = obj;
    }

    /**
     * 增强的逻辑
     *
     * @param method 被代理类的同名方法
     * @param args   被代理同名方法的参数
     * @return 被代理类的同名方法的返回值
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // 方法之前
        System.out.println("方法之前执行....");
        // 被增强的方法执行
        Object res = method.invoke(obj, args);
        // 方法之后
        System.out.println("方法之后执行....");
        return res;
    }
}

/**
 * 动态代理工厂
 */
class ProxyFactory {
    /**
     * @param obj 被代理对象
     */
    public static Object getProxyInstance(Object obj) {
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), new UserDaoProxy(obj));
    }
}


/**
 * @author HHH
 */
public class ProxyTest {
    public static void main(String[] args) {
        UserDao dao = (UserDao) ProxyFactory.getProxyInstance(new UserDaoImpl());
        int result = dao.add(1, 2);
        System.out.println("result:" + result);
    }
}
