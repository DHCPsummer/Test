package reflection;

/**
 * 代理接口
 */
interface ClothFactory {
    void productCloth();
}

class NikeFactory implements ClothFactory {
    @Override
    public void productCloth() {
        System.out.println("生产Nike衣服");
    }
}

/**
 * 代理方法
 */
class ProxyFactoryCloth implements ClothFactory {
    private ClothFactory clothFactory;

    public ProxyFactoryCloth(ClothFactory clothFactory) {
        this.clothFactory = clothFactory;
    }

    @Override
    public void productCloth() {
        System.out.println("代理方法前...");
        clothFactory.productCloth();
        System.out.println("代理方法后...");
    }
}

/**
 * 静态代理
 *
 * @author HHH
 */
public class StaticProxy {
    public static void main(String[] args) {
        // 创建被代理对象
        NikeFactory nikeFactory = new NikeFactory();
        // 创建代理对象
        ProxyFactoryCloth proxyFactoryCloth = new ProxyFactoryCloth(nikeFactory);
        proxyFactoryCloth.productCloth();
    }
}
