package test;

import java.util.Scanner;

class Utility1 {
    private static Scanner scanner = new Scanner(System.in);

    private static String readKeyBoard(int limit) {
        label:for (; ; ) {
            String str = scanner.next();
            if (str.length() > limit) {
                System.out.println("输入过长，请重新输入：");
                continue label;
            } else {
                return str;
            }
        }
    }

    //  读取用户的选择功能号
    public static char readMenuSelection() {
        char c;
        for (; ; ) {
            String str = readKeyBoard(1);
            c = str.charAt(0);
            if (c != '1' && c != '2' && c != '3' && c != '4' && c != '5') {
                System.out.println("选择无效，请重新输入：");
            } else {
                break;
            }
        }
        return c;
    }

    //  读取用户输入的金额
    public static int readNumber() {
        int n;
        for (; ; ) {
            String str = readKeyBoard(4);

            try {
                n = Integer.parseInt(str);
                break;
            } catch (NumberFormatException e) {
                System.out.print("数字输入错误，请重新输入：");
            }

        }
        return n;
    }

    //  读取用户输入的收支说明
    public static String readString() {
        String str = readKeyBoard(8);
        return str;
    }

    //  读取用户是否退出系统
    public static char readConfimSelection() {
        char c;
        for (; ; ) {
            String str = readKeyBoard(1).toUpperCase();
            c = str.charAt(0);
            if (c == 'Y' || c == 'N') {
                break;
            } else {
                System.out.print("选择错误，请重新输入：");
            }
        }
        return c;
    }
}
