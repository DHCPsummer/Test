package test;

public class person {
    private String sid;//学号
    private String name;//姓名
    private String age;//年龄
    private int boardfee;//住宿
    private int shoppingfee;//网购
    private int partyfee;//聚会
    private int searchfee;//考证
    private int lovebill;//恋爱账单

    public void test(String sid, String name, String age, int boardfee, int shoppingfee, int partyfee, int searchfee, int lovebill) {
        this.sid = sid;
        this.name = name;
        this.age = age;
        this.boardfee = boardfee;
        this.shoppingfee = shoppingfee;
        this.partyfee = partyfee;
        this.searchfee = searchfee;
        this.lovebill = lovebill;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getBoardfee(int boardfee) {
        return boardfee;
    }

    public void setBoardfee(int boardfee) {
        this.boardfee = boardfee;
    }

    public int getShoppingfee() {
        return shoppingfee;
    }

    public void setShoppingfee(int shopping) {
        this.shoppingfee = shoppingfee;
    }

    public int getPartyfee() {
        return partyfee;
    }

    public void setPartyfee(int partyfee) {
        this.partyfee = partyfee;
    }

    public int getSearchfee() {
        return searchfee;
    }

    public void setSearchfee(int searchfee) {
        this.searchfee = searchfee;
    }

    public int getLovebill() {
        return lovebill;
    }

    public void setLovefee(int lovebill) {
        this.lovebill = lovebill;
    }
}