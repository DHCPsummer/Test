package test;

import java.util.Random;
import java.util.Scanner;

public class EdManagement {
    public static void main(String[] args) {
        // 定义isFlag来判断用户是否退出系统
        boolean isFlag = true;
        //  用于记录收入和支出的细节
        String details = "类型    \t    账户金额    \t    支出金额    \t     时间\n";
        //  初始化金额
        int balance = 0;
        //  设置显示页面，并在使用时循环显示
        Random insRandom = new Random();
        Scanner sc = new Scanner(System.in);
        while (isFlag) {
            System.out.println("-----------我 的 支 出 记 录-----------");
            System.out.println("1.收支明细");
            System.out.println("2.登记支出");
            System.out.println("3.插入支出");
            System.out.println("4.删除支出");
            System.out.println("5.退   出");
            System.out.println();
            System.out.println("请选择(1-5):");
            //  从readMenuSelection中获取选择的功能号
            char selection = Utility1.readMenuSelection();
            switch (selection) {
                //  当用户执行支出明细操作
                case '1':
                    System.out.println("------------当前支出明细记录------------");
                    //  显示收支的明细，明细记录存放在details字符串中
                    System.out.println(details);
                    System.out.println("---------------------------------------");
                    break;
                //  当用户执行登记支出操作
                case '2':
                    System.out.println("1.伙食费用");
                    System.out.println("2.网购费用");
                    System.out.println("3.聚会费用");
                    System.out.println("4.考证费用");
                    System.out.println("5.恋爱费用");
                    System.out.println();
                    System.out.print("本次支出时间：");
                    //  定义一个reduceMoney来存放消费的金额
                    int reduceMoney = Utility1.readNumber();
                    int b = sc.nextInt();
                    switch (b) {
                        case 1:
                            String ins1 = "伙食费用";
                            int s1 = insRandom.nextInt(6000 - 3600 + 1) + 3600;
                            reduceMoney = s1 - 1;
                            System.out.println("本次支出金额：" + ins1 + ":" + reduceMoney);
                            break;
                        case 2:
                            String ins2 = "网购费用";
                            int s2 = insRandom.nextInt(2000);
                            reduceMoney = s2 - 2;
                            System.out.println("本次支出金额：" + ins2 + ":" + reduceMoney);

                            break;
                        case 3:
                            String ins3 = "聚会费用";
                            int s3 = insRandom.nextInt(2000);
                            reduceMoney = s3 - 3;
                            System.out.println("本次支出金额：" + ins3 + ":" + reduceMoney);
                            break;
                        case 4:
                            String ins4 = "考证费用";
                            int s4 = insRandom.nextInt(2000);
                            reduceMoney = s4 - 4;
                            System.out.println("本次支出金额：" + ins4 + ":" + reduceMoney);
                            break;
                        case 5:
                            String ins5 = "恋爱费用";
                            int s5 = insRandom.nextInt(2000);
                            reduceMoney = s5 - 5;
                            System.out.println("本次支出金额：" + ins5 + ":" + reduceMoney);
                            break;
                        default:
                            break;
                    }
                    String reduceInfo = Utility1.readString();
                    //  更新账户金额
                    balance -= reduceMoney;
                    //  更新显示收入信息
                    details += "支出 \t" + balance + "\t\t\t" + reduceMoney + "\t\t\t" + reduceInfo + "\n";
                    System.out.println("---------------登记完成-----------------");
                    break;

                case '3':
                    System.out.println("需要修改什么：");
                    System.out.println("1.时间");
                    System.out.println("2.金额");
                    int c = sc.nextInt();
                    switch (c) {
                        case 1:
                            System.out.println("具体时间：");

                            break;

                        default:
                            break;
                    }
                    break;
                    //当用户执行删除支出操作
                case '4':
                    System.out.println(" 删除操作  5");
                    break;
                    //  当用户执行退出系统操作
                case '5':
                    System.out.println("是否退出系统?(y/n)");
                    char isQuit = Utility1.readConfimSelection();
                    if (isQuit == 'Y') {
                        System.out.println("正在退出系统...");
                        System.out.println("已退出系统");
                        isFlag = false;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private static void readMenuSelection() {
    }
}
