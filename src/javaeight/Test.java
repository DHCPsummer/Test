package javaeight;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * java8包下的测试类
 *
 * @author HHH
 */
public class Test {
    public static void main(String[] args) {
        LocalDate data = LocalDate.now();
        LocalDateTime time = LocalDateTime.now();
        LocalDateTime time1 = time.withDayOfMonth(30);
        System.out.println(data);
        System.out.println(time);
        System.out.println(time1);
    }
}
