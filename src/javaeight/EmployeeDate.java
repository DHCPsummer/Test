package javaeight;

import java.util.ArrayList;
import java.util.List;

/**
 * 员工数据
 *
 * @author HHH
 */
public class EmployeeDate {
    public static List<Employee> getEmployeesData() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("张三",18,1800,1));
        employees.add(new Employee("李四",28,5000,2));
        employees.add(new Employee("王五",15,500,3));
        employees.add(new Employee("小六",19,8000,4));
        return employees;
    }
}
