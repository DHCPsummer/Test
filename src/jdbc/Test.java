package jdbc;

import java.sql.*;

/**
 * 测试jdbc
 *
 * @author HHH
 */
public class Test {
    /**
     * 数据库IP地址
     */
    private static final String SQL_IP = "localhost";
    /**
     * 数据库端口
     */
    private static final String SQL_PORT = "3306";
    /**
     * 数据库名
     */
    private static final String SQL_NAME = "test";
    /**
     * 连接数据库的URL
     */
    private static final String URL = "jdbc:mysql://" + SQL_IP + ":" + SQL_PORT + "/" + SQL_NAME;
    /**
     * 数据库用户名
     */
    private static final String SUER_NAME = "root";
    /**
     * 数据库密码
     */
    private static final String PASSWORD = "liang020901";

    public static void main(String[] args) {
        // 加载驱动
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("未能成功加载驱动程序,请检查是否导入驱动程序!");
            e.printStackTrace();
        }
        try (Connection connection = DriverManager.getConnection(URL, SUER_NAME, PASSWORD);
             Statement statement = connection.createStatement()) {
            // MySQL的select语句
            String select = "select * from test.t_students";
            // 执行SQL查询语句，得到查询结果
            ResultSet rs = statement.executeQuery(select);
            // 输出
            StringBuilder sb = new StringBuilder();
            sb.append("id\tname\n");
            while (rs.next()) {
                sb.append(rs.getInt(1)).append("\t").append(rs.getString(2)).append("\n");
            }
            System.out.println(sb);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
